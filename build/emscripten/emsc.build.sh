BUILD_DIR=$(pwd)
CMAKE_PROJECT_DIR=$BUILD_DIR
for i in `seq 1 2`;
do
        CMAKE_PROJECT_DIR="$(dirname "$CMAKE_PROJECT_DIR")"
done
VIRTUAL_SERVER_PATH=<virtual-server-address>

#------------------------------------------------------------------------------------------------

mkdir project

rsync -av --progress ../../resources project --exclude .DS_Store

cd project

cmake ${CMAKE_PROJECT_DIR} \
        -DCMAKE_TOOLCHAIN_FILE=${EMSCRIPTEN}/cmake/Modules/Platform/Emscripten.cmake \
        -DCMAKE_BUILD_TYPE=Debug \
        -DTARGET_PLATFORM=web && \
        echo Done && \
        make -j4
cd ..

mkdir deploy
# rsync -av --progress ../../target_resources/emscripten/scripts deploy --exclude .DS_Store
cp -f project/chart.js deploy
cp -f project/chart.js.mem deploy
cp -f project/chart.data deploy
cp -f ../../target_resources/emscripten/scripts/api.js deploy
cp -f ../../target_resources/emscripten/index.html deploy

while [ -n "$1" ]
do
case "$1" in
        -d)
                scp $BUILD_DIR/deploy/chart.js $VIRTUAL_SERVER_PATH
                scp $BUILD_DIR/deploy/chart.js.mem $VIRTUAL_SERVER_PATH
                scp $BUILD_DIR/deploy/chart.data $VIRTUAL_SERVER_PATH
                echo " - upload to the $VIRTUAL_SERVER_PATH server"
                ;;
        *)
                echo "There is no arguments"
                ;;
        esac
        shift
done
exit 0