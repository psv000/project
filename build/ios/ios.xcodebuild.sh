UNIVERSAL_OUTPUTFOLDER=${BUILD_DIR}/${CONFIGURATION}-universal
INCLUDE_FOLDER=${UNIVERSAL_OUTPUTFOLDER}/include

LIB_FOLDERS=(engine freetype-build libpng-build zlib-build jansson-build)
SLIM_LIBS=( )
FAT_LIBS=(libengine libfreetype libpng libz libjansson)

JANSSON_OUTPUT_FOLDER=${BUILD_DIR}/jansson-build/${CONFIGURATION}

rm -r -f ${UNIVERSAL_OUTPUTFOLDER}

# Step 1. Build Device and Simulator versions
xcodebuild -target $1 -parallelizeTargets ONLY_ACTIVE_ARCH=NO -configuration ${CONFIGURATION} -sdk iphoneos  BUILD_DIR='${BUILD_DIR}' BUILD_ROOT='${BUILD_ROOT}'

mkdir -p ${JANSSON_OUTPUT_FOLDER}-iphoneos
cp -R ${BUILD_DIR}/jansson-build/lib/${CONFIGURATION}/libjansson.a ${JANSSON_OUTPUT_FOLDER}-iphoneos
rm -f ${BUILD_DIR}/jansson-build/lib/${CONFIGURATION}/libjansson.a

xcodebuild -target $1 -parallelizeTargets -configuration ${CONFIGURATION} -sdk iphonesimulator -arch x86_64 BUILD_DIR='${BUILD_DIR}' BUILD_ROOT='${BUILD_ROOT}'

mkdir -p ${JANSSON_OUTPUT_FOLDER}-iphonesimulator
cp -R ${BUILD_DIR}/jansson-build/lib/${CONFIGURATION}/libjansson.a ${JANSSON_OUTPUT_FOLDER}-iphonesimulator
rm -f ${BUILD_DIR}/jansson-build/lib/${CONFIGURATION}/libjansson.a

# make sure the output directory exists
mkdir -p ${UNIVERSAL_OUTPUTFOLDER}
mkdir -p ${INCLUDE_FOLDER}
# Step 2. Create universal binary file using lipo
lipo -create -output ${UNIVERSAL_OUTPUTFOLDER}/$1.a ${BUILD_DIR}/${CONFIGURATION}-iphoneos/$1.a ${BUILD_DIR}/${CONFIGURATION}-iphonesimulator/$1.a
rsync --progress -av --exclude "*.cpp" --exclude "*.mm" --exclude "IOSCallbacksReciever.h" ${BUILD_DIR}/../../../sources/platform/iOS/ ${INCLUDE_FOLDER}

if [ Debug = ${CONFIGURATION} ]
then
SLIM_LIBS=(libengine libfreetyped libpng16d libz libjansson)
else
SLIM_LIBS=(libengine libfreetype libpng16 libz libjansson)
fi

for i in "${!SLIM_LIBS[@]}"
do
    lipo -create -output ${UNIVERSAL_OUTPUTFOLDER}/${FAT_LIBS[$i]}.a ${BUILD_DIR}/${LIB_FOLDERS[$i]}/${CONFIGURATION}-iphoneos/${SLIM_LIBS[$i]}.a ${BUILD_DIR}/${LIB_FOLDERS[$i]}/${CONFIGURATION}-iphonesimulator/${SLIM_LIBS[$i]}.a
done

rsync --progress -av ${UNIVERSAL_OUTPUTFOLDER}/ ${BUILD_DIR}/../output-${CONFIGURATION}