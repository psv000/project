export CMAKE_PROJECT_DIR=../../../

BUILD_TYPE=$1
if [ -z "$BUILD_TYPE" ] || ( [ $1 != "Debug" ] && [ $1 != "Release" ] );
  then
    BUILD_TYPE=Debug
    echo "default build type is Debug"
else
    echo "build type is $BUILD_TYPE"
fi

mkdir project
cd project
cmake   -G Xcode ${CMAKE_PROJECT_DIR} \
        -DCMAKE_TOOLCHAIN_FILE=../ios.toolchain.cmake \
        -DCMAKE_BUILD_TYPE=$BUILD_TYPE \
        -DCMAKE_DEF_SHOW_DEBUG_STATS=1 \
        -DTARGET_PLATFORM=iOS && \
        open Project.xcodeproj && \
        echo ---------- Done ----------