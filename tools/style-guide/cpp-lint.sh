SOURCE_DIRECTORY=./../..
FILES_LIST=$(find ${SOURCE_DIRECTORY} -name "*.h" ! -path '*3rdparty*' ! -path '*build*')
python cpplint.py ${FILES_LIST}