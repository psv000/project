//
//  log.h
//  engine
//
//  Created by s.popov on 14/09/2017.
//
//

#pragma once

#if !defined(LOG_DISABLE)

#if defined IOS_PLATFORM or defined UNIX_PLATFORM

#include <core/logger.h>

#define LOGD(format, args...) logger::logd(format, ##args)
#define LOGE(format, args...) logger::loge(format, ##args)
#define LOGW(format, args...) logger::logw(format, ##args)
#define LOGI(format, args...) logger::logi(format, ##args)
#define LOGC(format, args...) logger::logc(format, ##args)

#elif defined ANDROID_PLATFORM // \IOS_PLATFORM

#include <android/log.h>

#define DEBUG_TAG PROJECT_NAME_TAG
#define LOGC(...) ((void) __android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, __VA_ARGS__))
#define LOGI(...) ((void) __android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, __VA_ARGS__))
#define LOGD(...) ((void) __android_log_print(ANDROID_LOG_DEBUG, DEBUG_TAG, __VA_ARGS__))
#define LOGE(...) ((void) __android_log_print(ANDROID_LOG_ERROR, DEBUG_TAG, __VA_ARGS__))
#define LOGW(...) ((void) __android_log_print(ANDROID_LOG_WARN, DEBUG_TAG, __VA_ARGS__))

#elif defined WEB_PLATFORM // \ANDROID_PLATFORM

#include "core/logger.h"

#define LOGD(format, args...) logger::logd(format, ##args)
#define LOGE(format, args...) logger::loge(format, ##args)
#define LOGW(format, args...) logger::logw(format, ##args)
#define LOGI(format, args...) logger::logi(format, ##args)
#define LOGC(format, args...) logger::logc(format, ##args)

#endif

#else // \WEB_PLATFORM

#define LOGD(format, args...)
#define LOGE(format, args...)
#define LOGW(format, args...)
#define LOGI(format, args...)
#define LOGC(format, args...)
#define LOGS(format, args...)

#endif //!defined(LOG_DISABLE)
