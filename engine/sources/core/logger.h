//
//  logger.h
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#pragma once

#include <stdio.h>
#include "inttypes.h"
#include "core/timesource.h"

namespace logger
{
void logd(const char *format, ...);
void loge(const char *format, ...);
void logw(const char *format, ...);
void logi(const char *format, ...);
void logc(const char *format, ...);
    
}//namespace logger
