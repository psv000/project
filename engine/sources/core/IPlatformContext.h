//
//  IPlatformContext.h
//  engine
//
//  Created by s.popov on 20/10/2017.
//
//

#pragma once


class IPlatformContext
{
public:
    virtual ~IPlatformContext() {}
    virtual float ppi() const = 0;
    virtual float density() const = 0;
};
