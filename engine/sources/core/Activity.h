//
//  Activity.h
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#pragma once

class IActivityDelegate;
class IPlatformContext;
namespace graphics
{
class Render;
class IRenderSurface;
}//namespace graphics

namespace input
{
class GestureNotifier;
}//namespace input

namespace Activity
{

void init();
void setupManagers(IPlatformContext* context, graphics::IRenderSurface*);
void deinit();

void start();
void stop();

void appWillEnterBackground();
void appDidEnterBackground();

void appWillEnterForeground();
void appDidEnterForeground();

void didRecieveMemoryWarning();

void update(float dt);
void draw();

void setDelegate(IActivityDelegate*);
void setContextSize(int width, int height);
    
float designScaleFactor();
float logicScaleFactor();
float deviceScaleFactor();

void setDesignScaleFactor(float);
void setLogicScaleFactor(float);
void setDeviceScaleFactor(float);

graphics::Render* render();
input::GestureNotifier* gestureNotifier();

IPlatformContext* context();

double activityUpdateDuration();
void recalculateDensity();

}//namespace Activity
