//
//  Activity.cpp
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#include "Activity.h"
#include "IActivityDelegate.h"
#include "IPlatformContext.h"

#include <core/memory/MemoryManager.h>
#include <core/file_system/FileSystem.h>

#include <input/GestureNotifier.h>

#include <graphics/Render.h>
#include <graphics/shaders/ShaderProgramLoader.h>
#include <graphics/textures/texture_loader/TextureLoader.h>

#include <graphics/scene/SceneContainerFactory.h>
#include <graphics/scene/MeshFactory.h>
#include <graphics/scene/SpriteFactory.h>
#include <graphics/scene/LabelFactory.h>
#include <graphics/scene/LineFactory.h>

#include <graphics/MaterialManager.h>
#include <graphics/fonts/TextRender.h>

#include <core/timesource.h>

#include <asserts.h>

namespace Activity
{
    IActivityDelegate* gActivityDelegate { nullptr };
    IPlatformContext* gContext { nullptr };
    graphics::Render* gRender {nullptr};
    input::GestureNotifier* gGestureNotifier {nullptr};

    double gActivityUpdateDuration {0.};
    float gDesignScaleFactor {1.f};
    float gViewSurfaceDensity {1.f};
    float gLogicScaleFactor {1.f};
    
}//namespace Activity

//------------------------------------------------------------------------------
void Activity::init()
{
    memory::MemoryManager::init();
    {
        graphics::SceneContainerFactory::init();
        graphics::SpriteFactory::init();
        graphics::MeshFactory::init();
        graphics::LabelFactory::init();
        graphics::LineFactory::init();
    }
    
    gRender = new graphics::Render();
    gGestureNotifier = new input::GestureNotifier();
}
//------------------------------------------------------------------------------
void Activity::setupManagers(IPlatformContext* context, graphics::IRenderSurface* surface)
{
    gContext = context;
    Activity::recalculateDensity();
    
    FileSystem::init();
    
    graphics::RenderOptions options;
    options.surface = surface;
    options.clearColor = vec4f(1.f, 1.f, 1.f, 1.f);
    
    graphics::ShaderProgramLoader::init();
    gRender->setup(options);
    
    textures::TextureLoader::init();
    graphics::MaterialManager::init();
    fonts::TextRender::init();
    
    abort_with_msg(gActivityDelegate != nullptr, "gActivityDelegate is null");
    gActivityDelegate->init();
}
//------------------------------------------------------------------------------
void Activity::deinit()
{
    gActivityDelegate->deinit();
    gRender->deinit();
    
    graphics::MaterialManager::deinit();
    fonts::TextRender::deinit();
    textures::TextureLoader::deinit();
    FileSystem::deinit();
    graphics::ShaderProgramLoader::deinit();
    
    {
        graphics::LineFactory::deinit();
        graphics::LabelFactory::deinit();
        graphics::MeshFactory::deinit();
        graphics::SpriteFactory::deinit();
        graphics::SceneContainerFactory::deinit();
    }
    
    delete gRender;
    delete gGestureNotifier;
    
    memory::MemoryManager::deinit();
    gActivityUpdateDuration = 0.;
}
//------------------------------------------------------------------------------
void Activity::start()
{
    gActivityDelegate->start();
}
//------------------------------------------------------------------------------
void Activity::stop()
{
    gActivityDelegate->stop();
}
//------------------------------------------------------------------------------
void Activity::appWillEnterBackground()
{
    gActivityDelegate->appWillEnterBackground();
}
//------------------------------------------------------------------------------
void Activity::appDidEnterBackground()
{
    gActivityDelegate->appDidEnterBackground();
}
//------------------------------------------------------------------------------
void Activity::appWillEnterForeground()
{
    gActivityDelegate->appWillEnterForeground();
}
//------------------------------------------------------------------------------
void Activity::appDidEnterForeground()
{
    gActivityDelegate->appDidEnterForeground();
}
//------------------------------------------------------------------------------
void Activity::didRecieveMemoryWarning()
{
    gActivityDelegate->didRecieveMemoryWarning();
}
//------------------------------------------------------------------------------
void Activity::update(float dt)
{
    gRender->beforeUpdate();
    gRender->update(dt);
    double time = posix_time();
    gActivityDelegate->update(dt);
    gActivityUpdateDuration = posix_time() - time;
    gRender->afterUpdate();
}
//------------------------------------------------------------------------------
void Activity::draw()
{
    gRender->draw();
    gActivityDelegate->draw();
}
//------------------------------------------------------------------------------
void Activity::setDelegate(IActivityDelegate* clientApp)
{
    abort_with_msg(clientApp != nullptr, "clientApp is null");
    gActivityDelegate = clientApp;
}
//------------------------------------------------------------------------------
void Activity::setContextSize(int width, int height)
{
    gRender->setContextSize(width, height);
    
    float designScaleFactor = Activity::designScaleFactor();
    width /= designScaleFactor;
    height /= designScaleFactor;
    
    gActivityDelegate->onResize(width, height);
}
//------------------------------------------------------------------------------
float Activity::designScaleFactor()
{
    return gDesignScaleFactor;
}
//------------------------------------------------------------------------------
float Activity::logicScaleFactor()
{
    return gLogicScaleFactor;
}
//------------------------------------------------------------------------------
float Activity::deviceScaleFactor()
{
    return gViewSurfaceDensity;
}
//------------------------------------------------------------------------------
graphics::Render* Activity::render()
{
    return gRender;
}
//------------------------------------------------------------------------------
input::GestureNotifier* Activity::gestureNotifier()
{
    return gGestureNotifier;
}
//------------------------------------------------------------------------------
double Activity::activityUpdateDuration()
{
    return gActivityUpdateDuration * 1000.;
}
//---------------------------------------------------------------------------------
void Activity::recalculateDensity()
{
    gViewSurfaceDensity = gContext->density();
    gDesignScaleFactor = gViewSurfaceDensity;
    if ( gViewSurfaceDensity > 1.f )
    {
        gLogicScaleFactor = 2.f;
    }
    else
    {
        gLogicScaleFactor = 1.f;
    }
}
//---------------------------------------------------------------------------------
void Activity::setDesignScaleFactor(float value)
{
    gDesignScaleFactor = value;
}
//---------------------------------------------------------------------------------
void Activity::setLogicScaleFactor(float value)
{
    gLogicScaleFactor = value;
}
//---------------------------------------------------------------------------------
void Activity::setDeviceScaleFactor(float value)
{
    gViewSurfaceDensity = value;
}
//---------------------------------------------------------------------------------
IPlatformContext* Activity::context()
{
    return gContext;
}
//---------------------------------------------------------------------------------