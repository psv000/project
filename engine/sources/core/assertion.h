//
//  assertions.h
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#pragma once

#include <stdbool.h>

//------------------------------------------------------------------------------
typedef struct
{
    int         line;
    const char* function;
    const char* file;
}
__debug_info_t;
//------------------------------------------------------------------------------
void __void_assert(bool condition, __debug_info_t info);
void __msg_assert(bool condition, __debug_info_t info, const char* message);
void __abort_with_msg(bool condition, __debug_info_t info, const char* message);
//------------------------------------------------------------------------------
