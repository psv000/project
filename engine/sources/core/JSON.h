#pragma once

#include <common/stringtypes.h>

struct json_t;
class JSON final
{
public:
    JSON();
    ~JSON();
    
    bool load(const char*);
    bool load(const string&);
    bool isempty() const;
    void stringify(string&);
    
private:
    json_t* mValue;
};
