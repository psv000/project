//
//  FileSystem.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <common/inttypes.h>
#include <common/stringtypes.h>
#include <core/IPlatformContext.h>

#include "File.h"

namespace FileSystem
{
    
enum class LocationType : uint8
{
    Root = 0,
    Data,
    Docs,
    Cache,
    Local,
    Storage,
    Resources
};
    
struct Locations
{
    std::string appRootDir;
    std::string dataDir;
    std::string docsDir;
    std::string cacheDir;
    std::string localDir;
    std::string storageDir;
    std::string resourcesDir;
};

void init();
void deinit();
void setLocations(Locations&&);

FileSystem::File loadFile(const char* name, LocationType type);
FileSystem::File saveFile(const char* name, LocationType type);
    
string locationPath(FileSystem::LocationType);
    
}//namespace FileSystem
