//
//  FileTypes.h
//  engine
//
//  Created by s.popov on 18/10/2017.
//
//

#pragma once

#include <common/inttypes.h>
#include <common/stringtypes.h>

namespace FileSystem
{

enum class AccessMod : uint8
{
    ReadOnly = 0,
    WriteNew,
    ReadWrite
};

enum class Extension : uint8
{
    FShader,
    VShader,
    PNG,
    TTF,
    Unknown
};

//------------------------------------------------------------------------------
inline string accessModToString(AccessMod mod)
{
    switch (mod) {
        case AccessMod::ReadOnly:
            return "r";
        case AccessMod::WriteNew:
            return "w";
        case AccessMod::ReadWrite:
            return "rw";
        default:
            break;
    }
    return string();
}
//------------------------------------------------------------------------------
inline Extension parseExtension(const char* filename)
{
    if (strstr(filename, "frag") != nullptr)
    {
        return Extension::FShader;
    }
    else if (strstr(filename, "vert") != nullptr)
    {
        return Extension::VShader;
    }
    else if (strstr(filename, "png") != nullptr)
    {
        return Extension::PNG;
    }
    else if (strstr(filename, "ttf") != nullptr)
    {
        return Extension::TTF;
    }
    return Extension::Unknown;
}
//
//class IFile
//{
//public:
//    virtual ~File() {}
//
//    virtual const char* name() const  = 0;
//    virtual Extension extension() const = 0;
//
//    virtual const void* data() const = 0;
//
//    virtual void release() = 0;
//
//    virtual uint64 read(char *buffer, uint64 size) = 0;
//
//    virtual uint64 seek(uint64 offset) = 0;
//    virtual uint64 rseek(uint64 offset) = 0;
//    virtual uint64 seekd(uint64 offset) = 0;
//    virtual uint64 rseekd(uint64 offset) = 0;
//
//    virtual uint64 offset() const = 0;
//    virtual uint64 length() const = 0;
//
//    virtual bool empty() const = 0;
//    virtual bool end() const = 0;
//};
    
}//namespace FileSystem
