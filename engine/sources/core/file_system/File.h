//
//  File.h
//  engine
//
//  Created by s.popov on 18/10/2017.
//
//

#pragma once

#if defined( ANDROID_PLATFORM )
#include <platform/android/AndroidFile.h>
#else //\defined( ANDROID_PLATFORM )
#include <platform/unix/UnixFile.h>
#endif //\defined( ANDROID_PLATFORM )
