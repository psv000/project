//
//  FileSystem.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "FileSystem.h"
#include <fstream>

#include <common/formatstring.h>
#include <asserts.h>

namespace FileSystem
{

std::string locationPath(LocationType type);

Locations gLocations;
    
}//namespace FileSystem
//------------------------------------------------------------------------------
void FileSystem::init()
{

}
//------------------------------------------------------------------------------
void FileSystem::deinit()
{
    
}
//------------------------------------------------------------------------------
void FileSystem::setLocations(Locations&& locations)
{
    gLocations.appRootDir = std::move(locations.appRootDir);
    gLocations.cacheDir = std::move(locations.cacheDir);
    gLocations.dataDir = std::move(locations.dataDir);
    gLocations.docsDir = std::move(locations.docsDir);
    gLocations.localDir = std::move(locations.localDir);
    gLocations.storageDir = std::move(locations.storageDir);
    gLocations.resourcesDir = std::move(locations.resourcesDir);
}
//------------------------------------------------------------------------------
std::string FileSystem::locationPath(LocationType type)
{
    switch (type) {
        case LocationType::Root:
            return gLocations.appRootDir;
        case LocationType::Data:
            return gLocations.dataDir;
        case LocationType::Docs:
            return gLocations.docsDir;
        case LocationType::Cache:
            return gLocations.cacheDir;
        case LocationType::Local:
            return gLocations.localDir;
        case LocationType::Storage:
            return gLocations.storageDir;
        case LocationType::Resources:
            return gLocations.resourcesDir;
    }
    
    msg_assert(false, "location not found");
    return std::string();
}
//------------------------------------------------------------------------------
FileSystem::File FileSystem::loadFile(const char* filename, LocationType type)
{
    auto fullpath = format_string("%s%s", locationPath(type).c_str(), filename);
    
    File file(fullpath.c_str(), AccessMod::ReadOnly);
    
    msg_assert(!file.empty(), format_string("file <%s> not found [fullpath: %s]",
                                            filename, fullpath.c_str()).c_str());
    return file;
}
//------------------------------------------------------------------------------
FileSystem::File FileSystem::saveFile(const char* filename, LocationType type)
{
    auto fullpath = format_string("%s%s", locationPath(type).c_str(), filename);
    
    File file(fullpath.c_str(), AccessMod::WriteNew);
    return file;
}
//------------------------------------------------------------------------------
