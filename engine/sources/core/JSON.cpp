#include "JSON.h"
#include <core/file_system/FileSystem.h>
#include <core/memory/memory.h>
#include <jansson.h>
#include <logs.h>
//------------------------------------------------------------------------------
JSON::JSON():
mValue(nullptr)
{
    
}
//------------------------------------------------------------------------------
JSON::~JSON()
{
    if ( mValue )
        json_delete(mValue);
}
//------------------------------------------------------------------------------
bool JSON::load(const string& filename)
{
    return JSON::load( filename.c_str() );
}
//------------------------------------------------------------------------------
bool JSON::load(const char* filename)
{
    FileSystem::File file = FileSystem::loadFile(filename, FileSystem::LocationType::Resources);
    if ( !file.empty() )
    {
        json_error_t error;
        char* buffer = nullptr;
        STACK_ALLOC(file.length(), buffer);
        file.read(buffer, file.length());
        mValue = json_loadb(buffer, file.length(), 0, &error);
        if (!mValue)
        {
            LOGE("error while json loading [line: %d | column: %d | position: %d] with message: %s. <source: %s>",
                 error.line,
                 error.column,
                 error.position,
                 error.text,
                 error.source);
            return false;
        }
    }
    return true;
}
//------------------------------------------------------------------------------
bool JSON::isempty() const
{
    return mValue == nullptr;
}
//------------------------------------------------------------------------------
void JSON::stringify(string& stringValue)
{
    void_assert( !JSON::isempty() );
    size_t flags = 0;
    stringValue = json_dumps(mValue, flags);
}
//------------------------------------------------------------------------------
