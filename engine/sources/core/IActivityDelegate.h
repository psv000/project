//
//  IActivityDelegate.h
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#pragma once

#include <common/inttypes.h>

class IActivityDelegate
{
public:
    virtual ~IActivityDelegate() {}
    
    virtual void init() = 0;
    virtual void deinit() = 0;
    
    virtual void start() = 0;
    virtual void stop() = 0;
    
    virtual void appWillEnterBackground() = 0;
    virtual void appDidEnterBackground() = 0;
    
    virtual void appWillEnterForeground() = 0;
    virtual void appDidEnterForeground() = 0;
    
    virtual void didRecieveMemoryWarning() = 0;

    virtual void onResize(uint16, uint16) = 0;
    
    virtual void update(float dt) = 0;
    virtual void draw() = 0;
};
