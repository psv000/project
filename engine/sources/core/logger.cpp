//
//  logger.cpp
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#include "logger.h"
#include "logger_format.h"
#include <stdarg.h>

namespace logger
{
void logd(const char *format, ...);
void loge(const char *format, ...);
void logw(const char *format, ...);
void logi(const char *format, ...);
void logc(const char *format, ...);
const char* output_format(Logs type);
}//namespace logger
//------------------------------------------------------------------------------
#ifdef DEBUG_LOG_HIGHLIGHTING
const char* logger::output_format(Logs type)
{
    switch(type) {
        case Logs::LOG_ERROR:
            return "[%.3f]\033[1;31m[E] >>>\033[0m \033[48;5;196m%s\033[0m\n";
        case Logs::LOG_WARNING:
            return "[%.3f]\033[1;32m[W] >>\033[0m %s\n";
        case Logs::LOG_INFO:
            return "[%.3f]\033[1;34m[I] >\033[0m %s\n";
        case Logs::LOG_DEBUG:
            return "[%.3f]\033[1;33m[D] >\033[0m %s\n";
        case Logs::LOG_COMMON:
        default:
            return "[%.3f] %s\n";
    }
}
#else
const char* logger::output_format(Logs type)
{
    switch(type) {
        case Logs::LOG_ERROR:
            return "[%.3f][E] >>> %s\n";
        case Logs::LOG_WARNING:
            return "[%.3f][W] >> %s\n";
        case Logs::LOG_INFO:
            return "[%.3f][I] > %s\n";
        case Logs::LOG_DEBUG:
            return "[%.3f][D] > %s\n";
        case Logs::LOG_COMMON:
        default:
            return "[%.3f] %s\n";
    }
}
#endif
//------------------------------------------------------------------------------
#ifdef DEBUG_LOG_SINCE_START
    static const double sInitTime { posix_time() };
    #define TIME_EXPR (posix_time() - sInitTime)
#else
    #define TIME_EXPR (posix_time())
#endif

#define log_body(type)\
    do\
    {\
        const int line_limit (1024);\
        char messageBuffer[line_limit + 1];\
        {\
            va_list args;\
            va_start(args, format);\
            int length = vsnprintf (messageBuffer, line_limit - 1, format, args);\
            (void) length;\
            va_end(args);\
        }\
        messageBuffer[line_limit] = '\0';\
        const char *logBuffer = output_format(type);\
        printf(logBuffer, TIME_EXPR, messageBuffer);\
        fflush(stdout);\
    }\
    while(false)
//------------------------------------------------------------------------------
void logger::logd(const char *format, ...)
{
    log_body(Logs::LOG_DEBUG);
}
//------------------------------------------------------------------------------
void logger::loge(const char *format, ...)
{
    log_body(Logs::LOG_ERROR);
}
//------------------------------------------------------------------------------
void logger::logw(const char *format, ...)
{
    log_body(Logs::LOG_WARNING);
}
//------------------------------------------------------------------------------
void logger::logi(const char *format, ...)
{
    log_body(Logs::LOG_INFO);
}
//------------------------------------------------------------------------------
void logger::logc(const char *format, ...)
{
    log_body(Logs::LOG_COMMON);
}
//------------------------------------------------------------------------------
