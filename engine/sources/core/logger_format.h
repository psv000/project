//
//  logger_format.h
//  chart
//
//  Created by s.popov on 27/12/2017.
//
//

#pragma once

#include <common/inttypes.h>

namespace logger
{
enum class Logs : uint8
{
    LOG_COMMON,
    LOG_INFO,
    LOG_DEBUG,
    LOG_ERROR,
    LOG_WARNING
};
}//namespace logger
