//
//  timesource.h
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#pragma once

#include <common/inttypes.h>

typedef struct
{
    uint8   second;
    uint8   minute;
    uint8   hour;
    uint8   m_day;
    uint8   w_day;
    uint16  y_day;
    uint16  mounth;
    uint16  year;
}
date_t;

using tick_t = int64;

date_t date(double);
double posix_time();

tick_t cpu_time();
double milliseconds_from(tick_t);

void format_date(double, char*, uint32, const char*);
