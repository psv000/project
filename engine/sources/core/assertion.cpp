//
//  assertions.cpp
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#include "assertion.h"
#include <cstdlib>
#include <cassert>
#include <logs.h>

#ifdef DEBUG_LOG_HIGHLIGHTING
#define PRINT_SERVICE_ONLY(file, line, function) LOGE("\033[32;1m%s [%d:%s] >>\033[0m unawailable runtime condition", file, line, function);
#define PRINT(file, line, function, msg) LOGE("\033[32;1m%s [%d:%s] >>\033[0m %s", file, line, function, msg);
#else
#define PRINT_SERVICE_ONLY(file, line, function) LOGE("%s [%d:%s] >> unawailable runtime condition", file, line, function);
#define PRINT(file, line, function, msg) LOGE("%s [%d:%s] >> %s", file, line, function, msg);
#endif//DEBUG_LOG_HIGHLIGHTING

//------------------------------------------------------------------------------
void __msg_assert(bool condition, __debug_info_t info, const char* message)
{
    if (!condition)
    {
        PRINT(info.file, info.line, info.function, message);
        abort();
    }
}
//------------------------------------------------------------------------------
void __abort_with_msg(bool condition, __debug_info_t info, const char* message)
{
    if (!condition)
    {
        PRINT(info.file, info.line, info.function, message);
        abort();
    }
}
//------------------------------------------------------------------------------
void __void_assert(bool condition, __debug_info_t info)
{
    if (!condition)
    {
        PRINT_SERVICE_ONLY(info.file, info.line, info.function);
        abort();
    }
}
//------------------------------------------------------------------------------
