//
//  header.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <common/inttypes.h>

namespace memory
{

typedef struct
{
    int64 length;
}
header_t;

}//namespace memory
