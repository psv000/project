//
//  MemoryManager.cpp
//  engine
//
//  Created by s.popov on 28/10/2017.
//
//

#include "MemoryManager.h"
#include "memory.h"
#include "header.h"
#include <core/timesource.h>
#include <utils/memory/helpers.h>

#include <core/memory/allocations.h>
#include <core/memory/allocators/MallocAllocator.h>
#include <core/memory/allocators/StackAllocator.h>

#include <asserts.h>

#include "cstdlib"

#define STACK_MEM_SIZE (1 * 1024 * 1024) //1mB

namespace memory
{
namespace MemoryManager
{

static const uint16 kAllocatorsCount { 64 };

uint64 gRawMemAllocated {0};
uint64 gRawMemAllocationsCount {0};
uint64 gRawMemAllocationsCountPerTime {0};
uint64 gStackPeak {0};
BaseAllocator* gAllocators[kAllocatorsCount];
uint16 gAllocatorsCount;

BaseAllocator* gMallocAllocator;
Allocator* gStackAllocator;

double gLastTime;
    
bool gInit {false};

}//namespace MemoryManager
//------------------------------------------------------------------------------
void MemoryManager::init()
{
    {
        gMallocAllocator = (MallocAllocator*)malloc( sizeof(MallocAllocator) );
        void_assert(gMallocAllocator);
        new (gMallocAllocator) MallocAllocator();
#ifdef DEBUG_ENABLED
        gMallocAllocator->setName("System Malloc Allocator");
#endif
    }
    {
        void* mem = malloc(STACK_MEM_SIZE);
        
        gStackAllocator = (StackAllocator*)malloc( sizeof(StackAllocator) );
        void_assert(gStackAllocator);
        new (gStackAllocator) StackAllocator(STACK_MEM_SIZE, mem);
#ifdef DEBUG_ENABLED
        gStackAllocator->setName("System Stack Allocator");
#endif
    }
    
    gRawMemAllocated = 0;
    gRawMemAllocationsCount = 0;
    gRawMemAllocationsCountPerTime = 0;
    gStackPeak = 0;
    memset(gAllocators, 0, kAllocatorsCount*sizeof(BaseAllocator*));
    gAllocatorsCount = 0;
    
    gLastTime = posix_time();
    
    gInit = true;
}
//------------------------------------------------------------------------------
void MemoryManager::deinit()
{
    void_assert( ( gRawMemAllocated == 0 &&
                  gAllocatorsCount == 0 ) ||
                  !"memory leak");
    
    gRawMemAllocated = 0;
    gRawMemAllocationsCount = 0;
    gRawMemAllocationsCountPerTime = 0;
    memset(gAllocators, 0, kAllocatorsCount*sizeof(BaseAllocator*));
    gAllocatorsCount = 0;
    gStackPeak = 0;
    
    gInit = false;

    {
        free ( gStackAllocator->start() );
        gStackAllocator->~Allocator();
        free( gStackAllocator );
    }
    {
        gMallocAllocator->~BaseAllocator();
        free( gMallocAllocator );
    }
}
//------------------------------------------------------------------------------
BaseAllocator* MemoryManager::systemHeapAllocator()
{
    return gMallocAllocator;
}
//------------------------------------------------------------------------------
void MemoryManager::registerAllocator(memory::BaseAllocator* allocator)
{
    void_assert(gInit);
    gAllocators[gAllocatorsCount++] = allocator;
}
//------------------------------------------------------------------------------
void MemoryManager::unregisterAllocator(memory::BaseAllocator* allocator)
{
    void_assert(gInit);
    int16 r = -1;
    for(int16 i = 0; i < gAllocatorsCount; ++i)
    {
        if (allocator == gAllocators[i])
        {
            r = i;
            break;
        }
    }
    
    void_assert(r > -1 || !"allocator not found");
    
    if (r >= 0)
    {
        for (int16 i = r + 1; i < gAllocatorsCount; ++i)
        {
            gAllocators[i - 1] = gAllocators[i];
        }
        
        gAllocators[--gAllocatorsCount] = nullptr;
    }
}
//------------------------------------------------------------------------------
uint64 MemoryManager::memoryUsed()
{
    uint64 used = 0;
    for ( uint16 i = 0; i < gAllocatorsCount; ++i )
    {
        used += gAllocators[i]->getMemoryUsed();
    }
    return used;
}
//------------------------------------------------------------------------------
uint64 MemoryManager::memoryAllocated()
{
    uint64 allocated = 0;
    for ( uint16 i = 0; i < gAllocatorsCount; ++i )
    {
        allocated += gAllocators[i]->getMemoryAllocated();
    }
    return allocated;
}
//------------------------------------------------------------------------------
uint64 MemoryManager::rawMemoryAllocated()
{
    return gMallocAllocator->getMemoryUsed();
}
//------------------------------------------------------------------------------
uint64 MemoryManager::rawMemoryAllocationsPerTime()
{
    uint64 res = gRawMemAllocationsCountPerTime;
    gRawMemAllocationsCountPerTime = 0;
    return res;
}
//------------------------------------------------------------------------------
uint64 MemoryManager::stackPeak()
{
    return gStackPeak;
}
//------------------------------------------------------------------------------
uint64 MemoryManager::stackSize()
{
    return gStackAllocator->getMemoryAllocated();
}
//------------------------------------------------------------------------------
void MemoryManager::destructAllocator(memory::BaseAllocator* allocator)
{
    void_assert(gInit);
    MemoryManager::unregisterAllocator(allocator);
    allocator->~BaseAllocator();
    memory::dealloc(allocator);
}
//------------------------------------------------------------------------------
void MemoryManager::destructPool(memory::PoolAllocator* allocator)
{
    void_assert(gInit);
    MemoryManager::unregisterAllocator(allocator);
    memory::dealloc(allocator->start());
    allocator->~PoolAllocator();
    memory::dealloc(allocator);
}
//------------------------------------------------------------------------------
void* MemoryManager::heapAlloc(uint64 bytes, uint8 align)
{
    ++gRawMemAllocationsCountPerTime;
    return gMallocAllocator->alloc(bytes, align);
}
//------------------------------------------------------------------------------
void MemoryManager::dealloc(void* ptr)
{
    if ( gStackAllocator->owns(ptr) )
    {
        gStackAllocator->dealloc(ptr);
    }
    else
    {
        --gRawMemAllocationsCount;
        gMallocAllocator->dealloc(ptr);
    }
}
//------------------------------------------------------------------------------
void* MemoryManager::stackAlloc(uint64 bytes, uint8 align)
{
    void* ptr = gStackAllocator->alloc(bytes, align);
    void_assert(ptr);
    gStackPeak = GET_MAX(gStackPeak, gStackAllocator->getMemoryUsed());
    return ptr;
}
//------------------------------------------------------------------------------
}//namespace memory
