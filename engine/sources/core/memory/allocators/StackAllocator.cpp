//
//  StackAllocator.cpp
//  engine
//
//  Created by s.popov on 07/11/2017.
//
//

#include "StackAllocator.h"
#include <core/memory/memory.h>
#include <core/memory/header.h>
#include <utils/memory/helpers.h>

#include <cstdlib>

namespace memory
{
//------------------------------------------------------------------------------
StackAllocator::StackAllocator(int64 size, void* mem):
Allocator(mem, size),
mOffset(0)
{

}
//------------------------------------------------------------------------------
StackAllocator::~StackAllocator()
{

}
//------------------------------------------------------------------------------
void* StackAllocator::alloc(uint64 bytes, uint8 alignment)
{
    using utils::memory::align_forward;
    using utils::memory::size_with_padding;
    using utils::memory::data_pointer;
    using utils::memory::fill;
    using memory::header_t;
    
    const char* currentAddress = (char*)_memStart + mOffset;
    const int64 size = size_with_padding(bytes, alignment);
    
    if (size + mOffset > _memCapacity) {
        void_assert(false);
        return nullptr;
    }
    header_t *header_ptr = (header_t *)align_forward(currentAddress, alignof(header_t));
    void *ptr = data_pointer(header_ptr, alignment);
    fill(header_ptr, ptr, size);
    
    mOffset += size;
    
    _memoryUsed += size;
    ++_allocationsCount;
    
    return ptr;
}
//------------------------------------------------------------------------------
void StackAllocator::dealloc(void* ptr)
{
    if(ptr == nullptr)
    {
        return;
    }
    
    using utils::memory::header;
    using memory::header_t;
    
    header_t *header_ptr = header(ptr);
    _memoryUsed -= header_ptr->length;
    --_allocationsCount;
    
    mOffset -= header_ptr->length;
    void_assert( mOffset >= 0 && mOffset < _memCapacity );
}
//------------------------------------------------------------------------------
bool StackAllocator::owns(void* ptr) const
{
    return ptr >= _memStart && ptr < (void*)((char*)_memStart + _memCapacity);
}
//------------------------------------------------------------------------------
}//namespace memory
