//
//  PoolAllocator.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include "Allocator.h"

namespace memory
{

class PoolAllocator final : public Allocator
{
public:
    PoolAllocator(int64, uint8, int64, void*);
    virtual ~PoolAllocator();

    PoolAllocator(const PoolAllocator&) = delete;
    PoolAllocator& operator=(const PoolAllocator&) = delete;

    virtual void*   alloc   (uint64, uint8) override;
    virtual void    dealloc (void*)         override;
    virtual bool    owns    (void*)         const override;
    
    void setBackAllocator(BaseAllocator* allocator) { _backAllocator = allocator; }

private:
    uint64  _objectSize;
    uint8   _objectAlignment;
    void**  _freeList;
    
    BaseAllocator* _backAllocator;
};

}//namespace memory
