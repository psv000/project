//
//  Allocator.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "Allocator.h"

#include <asserts.h>
#include <cstdlib>
#include <common/formatstring.h>

namespace memory
{
//------------------------------------------------------------------------------
BaseAllocator::BaseAllocator():
_allocationsCount(0),
_memoryUsed(0)
{

}
//------------------------------------------------------------------------------
BaseAllocator::~BaseAllocator()
{
#ifdef DEBUG_ENABLED
    msg_assert(_memoryUsed == 0 && _allocationsCount == 0, format_string("<%s> is not empty", _name.c_str()).c_str());
#endif
}
//------------------------------------------------------------------------------
Allocator::Allocator(void* start, uint64 size):
BaseAllocator(),
_memStart(start),
_memCapacity(size)
{
    msg_assert(start != nullptr && size > 0, "invalid memory pool");
}
//------------------------------------------------------------------------------
Allocator::~Allocator()
{

}
//------------------------------------------------------------------------------
}//namespace memory

