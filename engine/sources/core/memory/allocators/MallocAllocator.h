//
//  MallocAllocator.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include "Allocator.h"

namespace memory
{

class MallocAllocator final : public BaseAllocator
{
public:
    MallocAllocator();
    virtual ~MallocAllocator();

    MallocAllocator(const MallocAllocator&) = delete;
    MallocAllocator& operator=(const MallocAllocator&) = delete;
    bool owns(void* ptr) const override { return false; }

    virtual void*   alloc   (uint64, uint8) override;
    virtual void    dealloc (void*)         override;
};

}//namespace memory
