//
//  MallocAllocator.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "MallocAllocator.h"

#include <asserts.h>
#include <core/memory/memory.h>
#include <core/memory/header.h>
#include <utils/memory/helpers.h>
#include <cstdlib>

namespace memory
{
//------------------------------------------------------------------------------
MallocAllocator::MallocAllocator():
BaseAllocator()
{

}
//------------------------------------------------------------------------------
MallocAllocator::~MallocAllocator()
{
}
//------------------------------------------------------------------------------
void* MallocAllocator::alloc (uint64 bytes, uint8 alignment)
{
    using utils::memory::size_with_padding;
    using utils::memory::data_pointer;
    using utils::memory::fill;
    using memory::header_t;

    const int64 size = size_with_padding(bytes, alignment);
    header_t *header_ptr = (header_t *)malloc(size);
    void *ptr = data_pointer(header_ptr, alignment);
    fill(header_ptr, ptr, size);

    _memoryUsed += size;
    ++_allocationsCount;

    return ptr;
}
//------------------------------------------------------------------------------
void MallocAllocator::dealloc (void* ptr)
{
    if(ptr == nullptr)
    {
        return;
    }

    using utils::memory::header;
    using memory::header_t;

    header_t *header_ptr = header(ptr);
    _memoryUsed -= header_ptr->length;
    --_allocationsCount;
    free((void *)header_ptr);
}
//------------------------------------------------------------------------------
}//namespace memory
