//
//  StackAllocator.h
//  engine
//
//  Created by s.popov on 07/11/2017.
//
//

#pragma once

#include "Allocator.h"

namespace memory
{

class StackAllocator final : public Allocator
{
public:
    StackAllocator(int64, void*);
    virtual ~StackAllocator();
    
    StackAllocator(const StackAllocator&) = delete;
    StackAllocator& operator=(const StackAllocator&) = delete;
    
    virtual void*   alloc   (uint64, uint8)  override;
    virtual void    dealloc (void*)         override;
    virtual bool    owns    (void*)         const override;
    
private:
    uint64 mOffset;
};

}//namespace memory
