//
//  PoolAllocator.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "PoolAllocator.h"

#include <asserts.h>
#include <utils/memory/helpers.h>
#include <utils/memory/pointer_math.h>

namespace memory
{
//------------------------------------------------------------------------------
PoolAllocator::PoolAllocator(int64 objectSize, uint8 objectAlignment, int64 size, void* memStart):
    Allocator       (memStart, size),
    _objectSize     (objectSize),
    _objectAlignment(objectAlignment),
    _freeList       (nullptr),
    _backAllocator  (nullptr)
{
    msg_assert(objectSize >= sizeof(void*),
            "objectSize must be larger than void* because when blocks are\
                dealloc they store a pointer to the next free block in the list");

    uint8 adjustment = utils::memory::align_forward_adjustment(memStart, objectAlignment);
    _freeList = (void**)utils::memory::pointer_add(memStart, adjustment);
    int64 numObjects = (size - adjustment) / objectSize;

    void** ptr = _freeList;
    
    for(int64 i = 0; i < numObjects - 1; i++)
    {
        *ptr = utils::memory::pointer_add(ptr, objectSize);
        ptr = (void**) *ptr;
    }
    *ptr = nullptr;
}
//------------------------------------------------------------------------------
PoolAllocator::~PoolAllocator()
{

}
//------------------------------------------------------------------------------
void* PoolAllocator::alloc(uint64 size, uint8 alignment)
{
    msg_assert(size == _objectSize && alignment == _objectAlignment,
               "invalid arguments. alignments are not equal");

    if(!_freeList)
    {
        if (_backAllocator)
        {
            return _backAllocator->alloc(size, alignment);
        }
    }

    void* ptr = _freeList;
    
    _freeList = (void**)(*_freeList);
    
    _memoryUsed += size;
    _allocationsCount++;

    return ptr;
}
//------------------------------------------------------------------------------
void PoolAllocator::dealloc(void* ptr)
{
    if(ptr == nullptr)
    {
        return;
    }
    
    if ( PoolAllocator::owns(ptr) )
    {
        *((void**)ptr) = _freeList;

        _freeList = (void**)ptr;
        void_assert(_freeList);
        
        _memoryUsed -= _objectSize;
        _allocationsCount--;
    }
    else if ( _backAllocator )
    {
        _backAllocator->dealloc(ptr);
    }
}
//------------------------------------------------------------------------------
bool PoolAllocator::owns(void* ptr) const
{
    return ptr != nullptr && _memStart <= ptr && ptr <= utils::memory::pointer_add(ptr, _memCapacity);
}
//------------------------------------------------------------------------------
}//namespace memory
