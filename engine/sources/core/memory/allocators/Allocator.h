//
//  Allocator.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//


#pragma once

#include <common/inttypes.h>
#include <common/stringtypes.h>
#include <memory>

namespace memory
{

class BaseAllocator
{
public:
    BaseAllocator();
    virtual ~BaseAllocator();

    BaseAllocator(const BaseAllocator&) = delete;
    BaseAllocator& operator=(const BaseAllocator&) = delete;

    virtual void*   alloc   (uint64, uint8)  = 0;
    virtual void    dealloc (void*)         = 0;
    virtual bool    owns    (void*) const   = 0;

    virtual uint64 getAllocationsCount   () const { return _allocationsCount; }
    virtual uint64 getMemoryUsed         () const { return _memoryUsed; }
    virtual uint64 getMemoryAllocated    () const { return 0; }

    void setName(const string& name)
    {
#ifdef DEBUG_ENABLED
        _name = name;
#endif
    }

protected:
    uint64  _allocationsCount;
    uint64  _memoryUsed;
#ifdef DEBUG_ENABLED
    string    _name;
#endif
};

class Allocator : public BaseAllocator
{
public:
    Allocator(void* start, uint64 size);
    virtual ~Allocator();

    Allocator(const Allocator&) = delete;
    Allocator& operator=(const Allocator&) = delete;

    virtual void*   alloc   (uint64, uint8) override    = 0;
    virtual void    dealloc (void*) override            = 0;
    virtual bool    owns    (void*) const override      = 0;

    void* start() const { return _memStart; }
    uint64 getMemoryAllocated() const override { return _memCapacity; }

protected:
    void*   _memStart;
    uint64  _memCapacity;
};

}//namespace memory
