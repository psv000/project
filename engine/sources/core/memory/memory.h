//
//  memory.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <common/inttypes.h>
#include <new>
#include <asserts.h>

#define offset_of(type, member) ((void*) &(((type*)0)->member))

namespace memory
{

void* alloc(uint64 bytes);
void dealloc(void* p);
    
void* stack_alloc(uint64 bytes);
    
template<typename __type, typename ... Args>
__type* construct(Args... args)
{
    __type* obj = (__type*)memory::alloc(sizeof(__type));
    void_assert(obj);
    new (obj) __type(args...);
    return obj;
}
    
template<typename __type, typename ... Args>
void destruct(__type* object, Args... args)
{
    object->~__type();
    dealloc(object);
}

class StackAllocWrapper
{
void* ptr;
public:
template<typename __type>
StackAllocWrapper(uint64 size, __type*& external_ptr)
{
    ptr = stack_alloc(size * sizeof(__type));
    external_ptr = (__type*)ptr;
}

~StackAllocWrapper()
{
    dealloc(ptr);
}

void* operator*() const { return ptr; }

};
    
}//namespace memory

#define CONCATENATE_DETAIL(x, y) x##y
#define CONCATENATE(x, y) CONCATENATE_DETAIL(x, y)
#define MAKE_UNIQUE(x) CONCATENATE(x, __COUNTER__)

#define UNIQ_NAME MAKE_UNIQUE(stack_wrapper_)
#define STACK_ALLOC(size, ptr) memory::StackAllocWrapper UNIQ_NAME (size, ptr)
