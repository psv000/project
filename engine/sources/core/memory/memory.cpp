//
//  memory.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "memory.h"
#include "cstdlib"

#include <common/inttypes.h>
#include <asserts.h>

#include "MemoryManager.h"

//-------------------------------------------------------------
void* memory::alloc(uint64 bytes)
{
    return MemoryManager::heapAlloc(bytes);
}
//-------------------------------------------------------------
void memory::dealloc(void* p)
{
    MemoryManager::dealloc(p);
}
//-------------------------------------------------------------
void* memory::stack_alloc(uint64 bytes)
{
    return MemoryManager::stackAlloc(bytes);
}
//-------------------------------------------------------------
