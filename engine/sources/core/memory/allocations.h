//
//  allocations.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <asserts.h>
#include <common/inttypes.h>

#include "allocators/Allocator.h"

#define __alignof(__type) alignof(__type)

namespace memory
{
    
template <class __type, typename... Args>
__type* allocate(BaseAllocator& allocator, Args... args)
{
    auto size = GET_MAX(sizeof(__type), sizeof(void*));
    auto align = GET_MAX(__alignof(__type), __alignof(void*));
    auto mem = allocator.alloc(size, align);
    msg_assert(mem, "can\'t allocate memory");
    auto object = new (mem) __type(args...);
    return object;
}

template <class __type>
__type* allocate_copy(BaseAllocator& allocator, const __type& t)
{
    auto size = GET_MAX(sizeof(__type), sizeof(void*));
    auto align = GET_MAX(__alignof(__type), __alignof(void*));
    return new (allocator.alloc(size, align)) __type(t);
}

template<class __type>
void deallocate(BaseAllocator& allocator, __type* object)
{
    object->~__type();
    allocator.dealloc(object);
}

template<class __type>
__type* allocate_array(BaseAllocator& allocator, int64 length)
{
    msg_assert(length != 0, "invalid array length");

    auto size = GET_MAX(sizeof(__type), sizeof(void*));
    auto align = GET_MAX(__alignof(__type), __alignof(void*));

    uint8 header = sizeof(int64)/size;
    if(sizeof(int64) % size > 0)
    {
        header += 1;
    }

    //Allocate extra space to store array length in the bytes before the array
    __type* pointer = ( (__type*) allocator.alloc( size*(length + header), align ) ) + header;
    *( ((int64*)pointer) - 1 ) = length;

    for(int64 i = 0; i < length; i++)
    {
        new (&pointer[i]) __type;
    }

    return pointer;
}

template<class __type>
void deallocate_array(BaseAllocator& allocator, __type* array)
{
    if (array == nullptr)
    {
        return;
    }

    int64 length = *( ((int64*)array) - 1 );

    for(int64 i = 0; i < length; i++)
    {
        array[i].~__type();
    }

    //Calculate how much extra memory was allocated to store the length before the array
    uint8 header = sizeof(int64)/sizeof(__type);

    if(sizeof(int64) % sizeof(__type) > 0)
    {
        header += 1;
    }

    allocator.dealloc(array - header);
}

}//namespace memory
