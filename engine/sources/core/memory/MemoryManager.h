//
//  MemoryManager.h
//  engine
//
//  Created by s.popov on 28/10/2017.
//
//

#pragma once

#include "allocators/Allocator.h"
#include "allocators/PoolAllocator.h"
#include <asserts.h>

namespace memory
{
namespace MemoryManager
{
    
void init();
void deinit();
    
void registerAllocator(memory::BaseAllocator*);
void unregisterAllocator(memory::BaseAllocator*);
    
uint64 memoryUsed();
uint64 memoryAllocated();
uint64 rawMemoryAllocated();
uint64 rawMemoryAllocationsPerTime();
uint64 stackPeak();
uint64 stackSize();
    
void* heapAlloc(uint64, uint8 = 16);
void dealloc(void*);
void* stackAlloc(uint64, uint8 = 16);
    
BaseAllocator* systemHeapAllocator();
    
template<typename T, typename... Args>
T* createAllocator(const char* name, Args... args)
{
    T* allocator = (T*)MemoryManager::heapAlloc( sizeof(T), alignof(T) );
    void_assert(allocator);
    auto compileCheck = static_cast<memory::BaseAllocator*>(allocator);
    (void) compileCheck;
    new (allocator) T(args...);
#ifdef DEBUG_ENABLED
    allocator->setName(name);
#endif//DEBUG_ENABLED
    MemoryManager::registerAllocator(allocator);
    return allocator;
}
    
template<typename T>
PoolAllocator* createPool(const char* name, int objectsCount)
{
    PoolAllocator* allocator = (PoolAllocator*)MemoryManager::heapAlloc( sizeof(PoolAllocator),
                                                                         alignof(PoolAllocator) );
    void_assert(allocator);
    
    size_t objectSize = GET_MAX(sizeof(T), sizeof(void*));
    size_t objectAlign = GET_MAX(__alignof(T), __alignof(void*));
    
    void* mem = MemoryManager::heapAlloc(objectsCount * objectSize);
    void_assert(mem);
    new (allocator) PoolAllocator(objectSize, objectAlign, objectsCount * objectSize, mem);
#ifdef DEBUG_ENABLED
    allocator->setName(name);
#endif//DEBUG_ENABLED
    
    allocator->setBackAllocator( MemoryManager::systemHeapAllocator() );
    
    MemoryManager::registerAllocator(allocator);
    return allocator;
}
    
void destructAllocator(memory::BaseAllocator*);
void destructPool(memory::PoolAllocator*);


}//namespace MemoryManager
}//namespace memory
