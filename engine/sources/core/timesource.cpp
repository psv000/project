//
//  timesource.cpp
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#include "timesource.h"
#include <ctime>
#include <cmath>
#if defined IOS_PLATFORM or defined WEB_PLATFORM or defined UNIX_PLATFORM
#include <sys/time.h>
#endif

#define INITIAL_YEAR 1900
#define MILLISECONDS_IN_CLOCKS ( (float) CLOCKS_PER_SEC / 1000 )
//------------------------------------------------------------------------------
date_t date(double posix_time)
{
    struct tm* timeinfo;

    time_t rawtime = static_cast<time_t>(posix_time);
    timeinfo = localtime ( &rawtime );

    date_t date;
    date.second = timeinfo->tm_sec;
    date.minute = timeinfo->tm_min;
    date.hour   = timeinfo->tm_hour;
    date.m_day  = timeinfo->tm_mday;
    date.w_day  = timeinfo->tm_wday;
    date.y_day  = timeinfo->tm_yday;
    date.mounth = timeinfo->tm_mon;
    date.year   = INITIAL_YEAR + timeinfo->tm_year;

    return date;
}
//------------------------------------------------------------------------------
double posix_time()
{
    timeval date;
    gettimeofday(&date, nullptr);
    static const double NANOSECONDS_IN_SECOND (1000000.0);
    return date.tv_sec + (date.tv_usec / NANOSECONDS_IN_SECOND);
}
//------------------------------------------------------------------------------
tick_t cpu_time()
{
    return clock();
}
//------------------------------------------------------------------------------
double milliseconds_from(tick_t start)
{
    return (double)(cpu_time() - start) / (double)CLOCKS_PER_SEC * 100;
}
//------------------------------------------------------------------------------
void format_date(double date, char* buffer, uint32 length, const char* format)
{
    const time_t temp = date;
    tm* timeStruct = localtime(&temp);
    strftime(buffer, length, format, timeStruct);
}
//------------------------------------------------------------------------------
