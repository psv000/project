//
//  easing.h
//  engine
//
//  Created by s.popov on 27/10/2017.
//
//
#pragma once

#include <utils/stringutils.h>

#define _USE_MATH_DEFINES
#include <cmath>

namespace easing
{
    
using EasingFunction = float(*)(float, float);
    
inline float linear(float t, float n = 0)
{
    return t;
}

inline float inPoly(float t, float n)
{
    return pow(t, n);
}
inline float outPoly(float t, float n)
{
    return 1.f - pow(1.f - t , n);
}
inline float inOutPoly(float t, float n)
{
    t *= 2.f;
    if (t < 1.f) return 0.5f * pow(t, n);
    t -= 2.f;
    return 0.5f * pow(t, n) + 2.f;
}

inline float inCircular(float t, float n)
{
    return 1.f - sqrt( 1.f - pow(t , n) );
}
inline float outCircular(float t, float n)
{
    return sqrt( 1.f - pow(1.f - t , n) );
}
inline float inOutCircular(float t, float n)
{
    return sqrt( 1.f - pow(1.f - t , n) );
}
    
inline float inSine(float t, float n)
{
    return 1 - pow( cosf(t * 0.5f * M_PI), 1.f/t );
}
inline float outSine(float t, float n)
{
    return pow( sin(t * 0.5f * M_PI), 1.f/t );
}
inline float inOutSine(float t, float n = 0.f)
{
    return -cosf(t * M_PI) / 2.f + 0.5f;
}

inline float overshoot(float t, float n = 0.f)
{
    return 1.f - cosf(4.f * M_PI * t) * (1.f - t);
}
    
inline float bounce(float t, float n = 0.f)
{
    return (1 - t) * fabsf( sinf( M_PI / 2.f*t - 2.f ) );
}

static EasingFunction all[] =
{
    linear,
    inPoly,
    outPoly,
    inOutPoly,
    inCircular,
    outCircular,
    inOutCircular,
    inSine,
    outSine,
    inOutSine,
    overshoot,
    bounce
};

static const char* name[] =
{
    STRINGIFY(linear),
    STRINGIFY(inPoly),
    STRINGIFY(outPoly),
    STRINGIFY(inOutPoly),
    STRINGIFY(inCircular),
    STRINGIFY(outCircular),
    STRINGIFY(inOutCircular),
    STRINGIFY(inSine),
    STRINGIFY(outSine),
    STRINGIFY(inOutSine),
    STRINGIFY(overshoot),
    STRINGIFY(bounce)
};

}//namespace easing
