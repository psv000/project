//
//  RenderPrimitives.h
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#pragma once

#include <common/stringtypes.h>
#include <common/inttypes.h>
#include <utils/stringutils.h>
#include <utils/arrayutils.h>
#include <asserts.h>

namespace graphics
{

using VertexBufferID = uint32;
using VertexArrayObjectID = uint32;
using IndexBufferID = uint32;

enum class BufferUsage : uint8
{
    StaticDraw = 0,
    Unknown
};

using FramebufferID = uint32;
using ProgramID = uint32;
using UniformLocation = int32;
    
using MaterialID = int64;
using TextureID = uint32;

const FramebufferID         INVALID_FBO         {0};
const VertexBufferID        INVALID_VBO         {0};
const VertexArrayObjectID   INVALID_VAO         {0};
const IndexBufferID         INVALID_IBO         {0};
const ProgramID             INVALID_PROGRAM_ID  {0};
    
const MaterialID            INVALID_MATERIAL_ID {-1};
const TextureID             INVALID_TEXTURE_ID  {0};
    
struct RenderUnit
{
    VertexBufferID vao;
    VertexArrayObjectID vbo;
    IndexBufferID ibo;
};
    
enum class VertexAttribute : uint8
{
    Position = 0,
    Color = 1,
    Texture = 2,
    Shift = 3,
    Gamma = 4,
    Unknown
};
//------------------------------------------------------------------------------
enum class VertexDataType : uint8
{
    Float = 0,
    Byte,
    UnsignedShort
};

inline uint8 attribute_size(VertexDataType type)
{
    switch (type) {
        case VertexDataType::Float:
            return sizeof(float);
        case VertexDataType::UnsignedShort:
            return sizeof(unsigned short);
        case VertexDataType::Byte:
            return sizeof(unsigned char);
        default:
            void_assert(false);
            break;
    }
}
//------------------------------------------------------------------------------
struct MeshVertexComponent
{
    VertexAttribute attribute;
    uint8 size;
    VertexDataType type;
};
    
struct MVCArray
{
    MeshVertexComponent* components;
    uint8 size;
};

enum class RenderCommandType : uint8
{
    Empty = 0,
    ApplyProgram,
    ApplyUniforms,
    DrawMesh,
    DrawLine,
    ClearContext,
    CreateTexture2D,
    SetTexture2DAlignment,
    SetTexture2DSubImage,
    UpdateTexture2D,
    SetState,
    Count
};
    
#ifdef DEBUG_ENABLED

static const char* RenderCommandName[] =
{
    STRINGIFY(RenderCommandType::Empty),
    STRINGIFY(RenderCommandType::ApplyProgram),
    STRINGIFY(RenderCommandType::ApplyUniforms),
    STRINGIFY(RenderCommandType::DrawMesh),
    STRINGIFY(RenderCommandType::DrawLine),
    STRINGIFY(RenderCommandType::ClearContext),
    STRINGIFY(RenderCommandType::CreateTexture2D),
    STRINGIFY(RenderCommandType::SetTexture2DAlignment),
    STRINGIFY(RenderCommandType::SetTexture2DSubImage),
    STRINGIFY(RenderCommandType::UpdateTexture2D),
    STRINGIFY(RenderCommandType::SetState)
};

inline const char* renderCommandName(RenderCommandType type)
{
    void_assert((uint16)RenderCommandType::Count == lengthof(RenderCommandName));
    return RenderCommandName[(uint16)type];
}

#else

inline const char* renderCommandName(RenderCommandType type)
{
    return "";
}

#endif //DEBUG_ENABLED
    
enum class UniformType : uint8
{
    Float = 0,
    Int,
    Vec4,
    Vec3,
    Vec2,
    Mat4,
    Texture2D,
    Unknown
};
    
struct UniformElement
{
    string name;
    UniformType type;
};
    
struct UEArray
{
    UniformElement* elements;
    uint64 size;
};
    
struct UniformContainer
{
    UniformLocation location;
    UniformType type;
};
    
enum class TextureLevel : uint8
{
    L0 = 0,
    Count
};
    
struct TexturePack
{
    TextureLevel level;
    TextureID id;
};
    
enum class DrawMode : uint8
{
    Lines = 0,
    LineStrip,
    Triangles,
    TriangleStrip,
    Unknown
};
    
}//namespace graphics
