//
//  Render.cpp
//  engine
//
//  Created by s.popov on 16/09/2017.
//
//

#include "Render.h"
#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include "GraphicsDeviceFactory.h"
#include "scene/SceneContainerFactory.h"
#include "scene/LabelFactory.h"

#include <core/memory/MemoryManager.h>
#include <common/formatstring.h>

#include <core/timesource.h>

namespace graphics
{
//------------------------------------------------------------------------------
Render::Render():
mGraphicsDevice(nullptr),
mRenderSurface(nullptr),
mCommandQueue(),
mOrtoScreenCamera(Camera::Type::Orthographic),
mSystemScene(nullptr),
mSysInfo(nullptr),
mFPS(0),
mUpdatesPerSecond(5),
mDrawablesUpdateDuration(0.f),
mDrawDuration(0.f),
mContexSize(0)
{
    memset(&mOptions, 0, sizeof(RenderOptions));
}
//------------------------------------------------------------------------------
Render::~Render()
{
    GraphicsDeviceFactory::destructGraphicsDevice(mGraphicsDevice);
}
//------------------------------------------------------------------------------
void Render::setup(RenderOptions options)
{
    mOptions = options;
    mRenderSurface = options.surface;

    mGraphicsDevice = GraphicsDeviceFactory::createGraphicsDevice();
    msg_assert(mGraphicsDevice, "graphics device is undefined");
    mGraphicsDevice->initDevice(mOptions);
    mGraphicsDevice->onResize(mContexSize.x, mContexSize.y);
    mCommandQueue.init(mGraphicsDevice);

    vec3f sceneSize(mContexSize.x, mContexSize.y, 0.f);
    mSystemScene = SceneContainerFactory::createSceneContainer(&mOrtoScreenCamera, sceneSize);
    
    mSystemScene->setZOrder(SceneContainer::MAX_SCENE_ORDER);
    mSysInfo = LabelFactory::createLabel();
    mSystemScene->addNode(mSysInfo);
    
    fonts::FontProperties &prop = mSysInfo->font();
    prop.size = 12;
    prop.color = Color::Green;
    prop.backgroundColor = Color::Black;
    prop.backgroundColor.a = 200;

    mSysInfo->setAnchor(vec2f(0.f, 1.f));
    mSysInfo->setPivot(vec2f(0.f, 1.f));
    
    registerDrawable(mSystemScene);
}
//------------------------------------------------------------------------------
void Render::deinit()
{
    LabelFactory::destroyLabel(mSysInfo);
    mSysInfo = nullptr;
    
    unregisterDrawable(mSystemScene);
    SceneContainerFactory::destroySceneContainer(mSystemScene);
    mSystemScene = nullptr;
    
    mCommandQueue.deinit();
    mGraphicsDevice->deinitDevice();
}
//------------------------------------------------------------------------------
void Render::setClearColor(const Color4F& color)
{
    mOptions.clearColor = color;
    mGraphicsDevice->setClearColor(color.r, color.g, color.b, color.a);
}
//------------------------------------------------------------------------------
void Render::setContextSize(uint16 width, uint16 height)
{
    mContexSize = { width, height };

    {
        float designScaleFactor = Activity::designScaleFactor();
        mSystemScene->resize(width / designScaleFactor,
                             height / designScaleFactor);
    }

    mOrtoScreenCamera.updateProjections(width, height);
    if ( mGraphicsDevice )
    {
        mGraphicsDevice->onResize(width, height);
    }
}
//------------------------------------------------------------------------------
vec2u Render::contextSize() const
{
    float designScaleFactor = Activity::designScaleFactor();
    return vec2u( mContexSize.x / designScaleFactor, mContexSize.y / designScaleFactor );
}
//------------------------------------------------------------------------------
void Render::beforeUpdate()
{
    mOrtoScreenCamera.update();
    mCommandQueue.beginCommand(RenderCommandType::ClearContext);
    mCommandQueue.endCommand();
}
//------------------------------------------------------------------------------
#ifdef SHOW_DEBUG_STATS
static string memStr(uint64 bytes)
{
    if (bytes < 1024)
    {
        return format_string("%db", bytes);
    }
    else if (bytes < 1024 * 1024)
    {
        return format_string("%.2fkB", (float)bytes/1024.f);
    }
    return format_string("%.2fmB", (float)bytes/(1024.f * 1024.f));
}
//------------------------------------------------------------------------------
#define app_stats_string format_string("FPS: %d "\
    "[au %.3fms | du %.3fms | d %.3fms][%.3f]\n"\
    "MEM\n  allocators: %s / %s\n  heap: %s\n  alloc/sec: %lu\n  "\
    "stack: %s / %s\n" \
    "frame mem: %.3f\n" \
    "res: %d x %d x %.2f" \
    ,mFPS \
    ,Activity::activityUpdateDuration() \
    ,mDrawablesUpdateDuration, mDrawDuration \
    ,Activity::activityUpdateDuration() + mDrawablesUpdateDuration + mDrawDuration\
    ,memStr( memory::MemoryManager::memoryUsed() ).c_str() \
    ,memStr( memory::MemoryManager::memoryAllocated() ).c_str() \
    ,memStr( memory::MemoryManager::rawMemoryAllocated() ).c_str() \
    ,memory::MemoryManager::rawMemoryAllocationsPerTime() * mUpdatesPerSecond \
    ,memStr( memory::MemoryManager::stackPeak() ).c_str() \
    ,memStr( memory::MemoryManager::stackSize() ).c_str() \
    ,mCommandQueue.queueFullness()\
    ,mContexSize.x, mContexSize.y, Activity::designScaleFactor())
#elif defined SHOW_DEBUG_STATS_SHORT
#define app_stats_string format_string("FPS: %d [au %.1fms | du %.1fms | d %.1fms][%.3f]" \
    ,mFPS \
    ,Activity::activityUpdateDuration() \
    ,mDrawablesUpdateDuration, mDrawDuration,\
    Activity::activityUpdateDuration() + mDrawablesUpdateDuration + mDrawDuration)
#else
#define app_stats_string ""
#endif
void Render::update(float dt)
{
#if defined ( SHOW_DEBUG_STATS ) || defined ( SHOW_DEBUG_STATS_SHORT )
    static uint16 frames = 0;
    static float accumTime = 0;

    accumTime += dt;

    ++frames;
    if (accumTime > 1.f / mUpdatesPerSecond)
    {
        mFPS = (double)frames / accumTime;
        mSysInfo->setText(app_stats_string);
        
        accumTime = 0.f;
        frames = 0;
    }
#endif//DEBUG_ENABLED
}
//------------------------------------------------------------------------------
void Render::afterUpdate()
{
    double time = posix_time();
    for (IDrawable* drawable : mDrawables)
    {
        drawable->update();
    }
    mDrawablesUpdateDuration = ( posix_time() - time ) * 1000.;
}
//------------------------------------------------------------------------------
void Render::draw()
{
    double time = posix_time();
    mCommandQueue.proccessQueue();
    mCommandQueue.clear();
    mDrawDuration = ( posix_time() - time ) * 1000.;
}
//------------------------------------------------------------------------------
void Render::registerDrawable(IDrawable* drawable)
{
    auto it = std::find(mDrawables.begin(), mDrawables.end(), drawable);
    if (it != mDrawables.end())
    {
        msg_assert(false, "drawable already registered");
        return;
    }
    
    mDrawables.push_back(drawable);
    
    std::sort(mDrawables.begin(), mDrawables.end(), [](const IDrawable* l, const IDrawable* r)
    {
        return l->zOrder() < r->zOrder();
    });
}
//------------------------------------------------------------------------------
void Render::unregisterDrawable(IDrawable* drawable)
{
    auto it = std::find(mDrawables.begin(), mDrawables.end(), drawable);
    if (it == mDrawables.end())
    {
        return;
    }
    mDrawables.erase(it);
}
//------------------------------------------------------------------------------
}//namespace graphics
