//
//  DrawHelper.cpp
//  engine
//
//  Created by s.popov on 24/10/2017.
//
//

#include "DrawHelper.h"

#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include <graphics/Render.h>
#include <graphics/RenderCommandQueue.h>

#include <graphics/shaders/Program.h>

#include <graphics/scene/Mesh.h>

#define MAX_PROGRAM_ARGS_BYTES (1024 * 10)

namespace graphics
{
//------------------------------------------------------------------------------
ColoredLinePoints DrawHelper::coloredLine(vec2f* vertices, uint32 count, Color4B color)
{
    float designScaleFactor = Activity::designScaleFactor();
    
    ColoredLinePoints v;
    v.reserve(count);
    for (uint32 i = 0; i < count; ++i)
    {
        v.push_back( { vec3f(vertices[i], 0.f) * designScaleFactor, color } );
    }
    return v;
}
//------------------------------------------------------------------------------
void DrawHelper::queueMeshToRender(VertexArrayObjectID vao,
                                   uint64 indicesCount,
                                   Program* program,
                                   const ProgramArgs& args)
{
    auto& commandQueue = Activity::render()->renderCommandQueue();
    {
        commandQueue.beginCommand(RenderCommandType::ApplyProgram);
        commandQueue.addValue(program->id());
        commandQueue.endCommand();
    }
    {
        commandQueue.beginCommand(RenderCommandType::ApplyUniforms);
        
        commandQueue.addValue(program->id());
        commandQueue.addValue(args.count());
        byte* offsetReserved = commandQueue.reserve<uint64>();
        
        byte* start = nullptr;
        byte* end = nullptr;
        
        for (uint8 i = 0, size = args.count(); i < size; ++i)
        {
            auto& uniform = args.at(i);
            commandQueue.addValue( (UniformContainer){ uniform.location, uniform.type }, ( i == 0 ) ? &start : nullptr );
            
            byte** endPtr = ( i == size - 1 ) ? &end : nullptr;
            switch (uniform.type) {
                case UniformType::Float:
                    commandQueue.addValue( uniform.floatVal(), nullptr, endPtr );
                    break;
                case UniformType::Int:
                    commandQueue.addValue( uniform.intVal(), nullptr, endPtr );
                    break;
                case UniformType::Vec2:
                    commandQueue.addValue( uniform.vec2fVal(), nullptr, endPtr );
                    break;
                case UniformType::Vec3:
                    commandQueue.addValue( uniform.vec3fVal(), nullptr, endPtr );
                    break;
                case UniformType::Vec4:
                    commandQueue.addValue( uniform.vec4fVal(), nullptr, endPtr );
                    break;
                case UniformType::Mat4:
                    commandQueue.addValue( uniform.mat4fVal(), nullptr, endPtr );
                    break;
                case UniformType::Texture2D:
                    commandQueue.addValue( uniform.textureVal(), nullptr, endPtr );
                    break;
                case UniformType::Unknown:
                    break;
            }
        }
        
        void_assert(end >= start);
        *(uint64*)offsetReserved = (uint64)(end - start);
        
        commandQueue.endCommand();
    }
    {
        commandQueue.beginCommand(RenderCommandType::DrawMesh);
        commandQueue.addValue(vao);
        commandQueue.addValue(indicesCount);
        commandQueue.endCommand();
    }
}
//------------------------------------------------------------------------------
void DrawHelper::queueLineToRender(VertexArrayObjectID vao,
                                   VertexBufferID vbo,
                                   uint32 verticesCount,
                                   uint8 width,
                                   DrawMode mode,
                                   Program* program,
                                   const ProgramArgs& args)
{
    auto& commandQueue = Activity::render()->renderCommandQueue();
    {
        commandQueue.beginCommand(RenderCommandType::ApplyProgram);
        commandQueue.addValue(program->id());
        commandQueue.endCommand();
    }
    {
        commandQueue.beginCommand(RenderCommandType::ApplyUniforms);
        
        commandQueue.addValue(program->id());
        commandQueue.addValue(args.count());
        byte* offsetReserved = commandQueue.reserve<uint64>();
        
        byte* start = nullptr;
        byte* end = nullptr;
        
        for (uint8 i = 0, size = args.count(); i < size; ++i)
        {
            auto& uniform = args.at(i);
            commandQueue.addValue( (UniformContainer){ uniform.location, uniform.type }, ( i == 0 ) ? &start : nullptr );
            
            byte** endPtr = ( i == size - 1 ) ? &end : nullptr;
            switch (uniform.type) {
                case UniformType::Float:
                    commandQueue.addValue( uniform.floatVal(), nullptr, endPtr );
                    break;
                case UniformType::Int:
                    commandQueue.addValue( uniform.intVal(), nullptr, endPtr );
                    break;
                case UniformType::Vec2:
                    commandQueue.addValue( uniform.vec2fVal(), nullptr, endPtr );
                    break;
                case UniformType::Vec3:
                    commandQueue.addValue( uniform.vec3fVal(), nullptr, endPtr );
                    break;
                case UniformType::Vec4:
                    commandQueue.addValue( uniform.vec4fVal(), nullptr, endPtr );
                    break;
                case UniformType::Mat4:
                    commandQueue.addValue( uniform.mat4fVal(), nullptr, endPtr );
                    break;
                case UniformType::Texture2D:
                    commandQueue.addValue( uniform.textureVal(), nullptr, endPtr );
                    break;
                case UniformType::Unknown:
                    break;
            }
        }
        
        void_assert(end >= start);
        *(uint64*)offsetReserved = (uint64)(end - start);
        void_assert(*(uint64*)offsetReserved < MAX_PROGRAM_ARGS_BYTES);
        
        commandQueue.endCommand();
    }
    {
        commandQueue.beginCommand(RenderCommandType::DrawLine);
        commandQueue.addValue(vao);
        commandQueue.addValue(verticesCount);
        commandQueue.addValue(width);
        commandQueue.addValue(mode);
        commandQueue.endCommand();
    }
}
//------------------------------------------------------------------------------
void DrawHelper::createCircle(Mesh* circle, uint32* indices, uint32 indicesCount,
                              void* vertices, uint32 verticesCount,
                              float radius)
{
    auto programID = circle->program();
    auto& programArgs = circle->programArgs();
    
    circle->setSize(vec2f(2.f * radius));
    circle->setContent(VerticesType::V3f_C4u_U2us, vertices, verticesCount, indices, indicesCount);
    programArgs.registerArgument(programID, "ucolor", UniformType::Vec4);
    programArgs.registerArgument(programID, "uborder", UniformType::Float);
    
    programArgs.applyArgument( "ucolor", vec4f(1.f));
    programArgs.applyArgument( "uborder", 1.f / ( 2 * radius ) );
}
//------------------------------------------------------------------------------
}//namespace graphics
