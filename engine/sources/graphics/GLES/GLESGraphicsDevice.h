//
//  GLESGraphicsDevice.h
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#pragma once

#include <graphics/IGraphicsDevice.h>
#include "GLESProgramFactory.h"

namespace graphics
{
 
class GLESGraphicsDevice final : public IGraphicsDevice
{
public:
    GLESGraphicsDevice();
    
    void initDevice(const RenderOptions&) override;
    void deinitDevice() override;
    
    void setClearColor(float, float, float, float) override;
    
    void onResize(uint16, uint16) override;
    
    FramebufferID createFrameBuffer() override;
    void destroyFrameBuffer(FramebufferID) override;
    
    VertexBufferID createVertexBuffer() override;
    void destroyVertexBuffer(VertexBufferID) override;
    void updateVertexBuffer(VertexBufferID, const void*, uint64) override;
    
    IndexBufferID createIndexBuffer() override;
    void updateIndexBuffer(IndexBufferID, BufferUsage, void*, uint64) override;
    void destroyIndexBuffer(IndexBufferID) override;
    
    VertexArrayObjectID createVertexArrayObject() override;
    void updateVertexArrayObject(const MVCArray&,
                                 VertexArrayObjectID,
                                 VertexBufferID,
                                 IndexBufferID) override;
    
    void destroyVertexArrayObject(VertexArrayObjectID) override;
    
    Program* createProgram(const char*, const char*) override;
    void destroyShader(Program*) override;
    
    void createUniformContainer(ProgramID, const UEArray&, UniformContainer*) override;
    
    void applyShader(ProgramID) override;
    void applyUniforms(ProgramID, void*, uint8) override;
    
    void drawMesh(VertexArrayObjectID, uint64) override;
    void drawLine(VertexArrayObjectID, uint32, uint8, DrawMode) override;
    
    void clearContext() override;
    
    TextureID createTexture2D() override;
    void setTexture2DAlignment(TextureID, uint8) override;
    
    void setTexture2DSubImage(TextureID,
                              TextureLevel,
                              int16, int16,
                              uint16, uint16,
                              textures::Format,
                              textures::DataType,
                              const void*) override;
    
    void updateTexture2D(TextureID,
                         TextureLevel,
                         textures::InternalFormat,
                         uint16, uint16, int16,
                         textures::Format,
                         textures::DataType,
                         const void*) override;
    
    void destroyTexture2D(TextureID) override;
    
    void setState(const RenderState&) override;
    
    int checkError() const;
    void printError(GLenum) const;
    
private:
    GLESProgramFactory mProgramFactory;
};
    
}//namespace graphics
