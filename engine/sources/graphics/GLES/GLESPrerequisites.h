//
//  GLESPrerequisites.h
//  engine
//
//  Created by s.popov on 17/09/2017.
//
//

#pragma once

#if defined IOS_PLATFORM

#include <OpenGLES/ES3/gl.h>
#include <OpenGLES/ES3/glext.h>

#elif defined ANDROID_PLATFORM

#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>

#elif defined WEB_PLATFORM

#include <GLES3/gl3.h>
#include <GLES3/gl2ext.h>

#elif defined UNIX_PLATFORM

#define GLFW_INCLUDE_GLCOREARB
#include <GLFW/glfw3.h>

#endif
