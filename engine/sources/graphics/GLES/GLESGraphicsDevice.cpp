
//  GLESGraphicsDevice.cpp
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#include "GLESGraphicsDevice.h"
#include "GLESPrerequisites.h"
#include "GLESTypesTranslator.h"
#include <graphics/shaders/ShaderProgramLoader.h>
#include <graphics/textures/TextureTypesHelpers.h>

#include <glm/gtc/type_ptr.hpp>
#include <math/vec3.h>

#include <common/formatstring.h>
#include <logs.h>

#define MAX_GL_ERRORS 10

namespace graphics
{
//------------------------------------------------------------------------------
GLESGraphicsDevice::GLESGraphicsDevice():
mProgramFactory()
{
    
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::initDevice(const RenderOptions& options)
{
    LOGI("OpenGl version: %s", glGetString(GL_VERSION));
    LOGI("Vendor: %s", glGetString(GL_VENDOR));
    LOGI("Render: %s", glGetString(GL_RENDERER));
    
    const auto& color = options.clearColor;
    glClearColor(color.r, color.g, color.b, color.a);
//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_BACK);
    
    checkError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::deinitDevice()
{
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::setClearColor(float r, float g, float b, float a)
{
    glClearColor(r, g, b, a);
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::printError(GLenum err) const
{
    switch (err) {
        case GL_NO_ERROR:
            LOGE("*** OpenGL error: GL_NO_ERROR");
            break;
        case GL_INVALID_ENUM:
            LOGE("*** OpenGL error: GL_INVALID_ENUM");
            break;
        case GL_INVALID_VALUE:
            LOGE("*** OpenGL error: GL_INVALID_VALUE");
            break;
        case GL_INVALID_OPERATION:
            LOGE("*** OpenGL error: GL_INVALID_OPERATION");
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            LOGE("*** OpenGL error: GL_INVALID_FRAMEBUFFER_OPERATION");
            break;
        case GL_OUT_OF_MEMORY:
            LOGE("*** OpenGL error: GL_OUT_OF_MEMORY");
            break;
        default:
            LOGE("*** OpenGL error: error %d", err);
            break;
    }
}
//------------------------------------------------------------------------------
int GLESGraphicsDevice::checkError() const
{
    GLenum e;
    int errorsPrinted = 0;
    while ((e = glGetError()) != GL_NO_ERROR) {
        if (errorsPrinted < MAX_GL_ERRORS) {
            printError(e);
            ++errorsPrinted;
            if (errorsPrinted >= MAX_GL_ERRORS) {
                LOGE("*** TOO MANY OPENGL ERRORS. NO LONGER PRINTING.");
            }
        }
    }

    return errorsPrinted;
}
#if defined GLES_RENDER_ENABLED && defined DEBUG_ENABLED
#define checkGLError() msg_assert(checkError() == 0, "gapi error")
#else
#define checkGLError(message)
#endif //GLES_RENDER_ENABLED
//------------------------------------------------------------------------------
void GLESGraphicsDevice::onResize(uint16 width, uint16 height)
{
    glViewport(0, 0, width, height);
}
//------------------------------------------------------------------------------
Program* GLESGraphicsDevice::createProgram(const char* vertex, const char* fragment)
{
    return ShaderProgramLoader::createProgram(&mProgramFactory, vertex, fragment);
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::destroyShader(Program* shader)
{
    if ( shader )
        ShaderProgramLoader::destroyProgram(&mProgramFactory, shader);
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::createUniformContainer(ProgramID id, const UEArray& array, UniformContainer* container)
{
    mProgramFactory.createUniformContainer(id, array, container);
}
//------------------------------------------------------------------------------
FramebufferID GLESGraphicsDevice::createFrameBuffer()
{
    GLuint fbo;
    glGenFramebuffers(1, &fbo);
    return fbo;
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::destroyFrameBuffer(FramebufferID fbo)
{
    glDeleteFramebuffers(1, &fbo);
}
//------------------------------------------------------------------------------
VertexBufferID GLESGraphicsDevice::createVertexBuffer()
{
    GLuint vbo;
    glGenBuffers(1, &vbo);
    checkGLError();
    return vbo;
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::destroyVertexBuffer(VertexBufferID vbo)
{
    glDeleteBuffers(1, &vbo);
    checkGLError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::updateVertexBuffer(VertexBufferID vbo, const void* data, uint64 size)
{
    glBindBuffer(GL_ARRAY_BUFFER, (GLuint)vbo);
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
    checkGLError();
}
//------------------------------------------------------------------------------
VertexArrayObjectID GLESGraphicsDevice::createVertexArrayObject()
{
    GLuint vao;
    glGenVertexArrays(1, &vao);
    checkGLError();
    return vao;
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::updateVertexArrayObject(const MVCArray& mvc,
                                                 VertexArrayObjectID vao,
                                                 VertexBufferID vbo,
                                                 IndexBufferID ibo)
{
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, (GLuint)vbo);
    
    const auto components = mvc.components;
    
    GLchar offset = 0;
    uint32 stride = 0;
    
    for (uint8 i = 0; i < mvc.size; ++i)
    {
        const auto& vertex = components[i];
        switch(vertex.type)
        {
            case VertexDataType::Float:
                stride += sizeof(float) * vertex.size;
                break;
            case VertexDataType::Byte:
                stride += sizeof(byte) * vertex.size;
                break;
            case VertexDataType::UnsignedShort:
                stride += sizeof(uint16) * vertex.size;
                break;
            default:
                msg_assert(false, "unknown vertex component type");
        }
    }
    
    for (uint8 i = 0; i < mvc.size; ++i)
    {
        const auto& vertex = components[i];
        
        GLenum type;
        GLboolean normalized = GL_FALSE;
        uint32 currentOffset = offset;
        switch(vertex.type)
        {
            case VertexDataType::Float:
                type = GL_FLOAT;
                offset += sizeof(float) * vertex.size;
                normalized = GL_FALSE;
                break;
            case VertexDataType::Byte:
                type = GL_UNSIGNED_BYTE;
                offset += sizeof(byte) * vertex.size;
                normalized = GL_TRUE;
                break;
            case VertexDataType::UnsignedShort:
                type = GL_UNSIGNED_SHORT;
                offset += sizeof(uint16) * vertex.size;
                normalized = GL_TRUE;
                break;
            default:
            {
                type = GL_INVALID_ENUM;
                msg_assert(false, "unknown vertex component type");
                break;
            }
        }
        
        GLchar* pointer = nullptr;
        glVertexAttribPointer((GLuint)vertex.attribute, vertex.size, type, normalized, stride, pointer + currentOffset);
        glEnableVertexAttribArray((GLuint)vertex.attribute);
        
        checkGLError();
    }
    if (ibo != INVALID_IBO)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    }

    glBindVertexArray(0);
    checkGLError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::destroyVertexArrayObject(VertexArrayObjectID vao)
{
    glDeleteVertexArrays(1, &vao);
    checkGLError();
}
//------------------------------------------------------------------------------
IndexBufferID GLESGraphicsDevice::createIndexBuffer()
{
    GLuint ibo;
    glGenBuffers(1, &ibo);
    checkGLError();
    return ibo;
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::updateIndexBuffer(IndexBufferID ibo, BufferUsage usage, void* data, uint64 size)
{
    GLenum glUsage;
    switch(usage)
    {
        case BufferUsage::StaticDraw:
            glUsage = GL_STATIC_DRAW;
            break;
        default:
            glUsage = GL_STATIC_DRAW;
            break;
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, glUsage);
    checkGLError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::destroyIndexBuffer(IndexBufferID ibo)
{
    glDeleteBuffers(1, &ibo);
    checkGLError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::applyShader(ProgramID id)
{
    checkGLError();
    glUseProgram(id);
    checkGLError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::applyUniforms(ProgramID shaderID, void* data, uint8 size)
{
    byte* ptr = (byte*)data;
    for (int i = 0; i < size; ++i)
    {
        UniformContainer uniform = array::readValue<UniformContainer>(ptr);
        GLuint location = (GLuint)uniform.location;
        switch (uniform.type)
        {
            case UniformType::Float:
            {
                glUniform1f(location, array::readValue<float>(ptr));
                checkGLError();
                break;
            }
            case UniformType::Int:
            {
                glUniform1i(location, array::readValue<int>(ptr));
                checkGLError();
                break;
            }
            case UniformType::Vec2:
            {
                vec2f& value = array::readValue<vec2f>(ptr);
                glUniform2f(location, value[0], value[1]);
                checkGLError();
                break;
            }
            case UniformType::Vec3:
            {
                vec3f& value = array::readValue<vec3f>(ptr);
                glUniform3f(location, value[0], value[1], value[2]);
                checkGLError();
                break;
            }
            case UniformType::Vec4:
            {
                vec4f& value = array::readValue<vec4f>(ptr);
                glUniform4f(location, value[0], value[1], value[2], value[3]);
                checkGLError();
                break;
            }
            case UniformType::Mat4:
            {
                mat4f& val = array::readValue<mat4f>(ptr);
                auto val_ptr = glm::value_ptr(val);
                glUniformMatrix4fv(location, 1, GL_FALSE, val_ptr);
                checkGLError();
                break;
            }
            case UniformType::Texture2D:
            {
                
                TexturePack pack = array::readValue<TexturePack>(ptr);
                glActiveTexture(GL_TEXTURE0 + (int)pack.level);
                glBindTexture(GL_TEXTURE_2D, (GLuint)pack.id);
                glUniform1i(location, (GLint)pack.level);
                checkGLError();
                break;
            }
            default:
                msg_assert(false, "bad uniform container");
                break;
        }
    }
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::drawMesh(VertexArrayObjectID vao, uint64 indicesCount)
{
    glBindVertexArray((GLuint)vao);
    glDrawElements(GL_TRIANGLES, (GLsizei)indicesCount, GL_UNSIGNED_INT, (void*)0);
    glBindVertexArray(0);
    checkGLError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::drawLine(VertexArrayObjectID vao, uint32 count, uint8 width, DrawMode mode)
{
    glLineWidth(width);
    glBindVertexArray((GLuint)vao);
    glDrawArrays(drawMode::NativeToGL(mode), 0, count);
    glBindVertexArray(0);
    glLineWidth(1.f);
    checkGLError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::clearContext()
{
    glClear(GL_COLOR_BUFFER_BIT);
    checkGLError();
}
//------------------------------------------------------------------------------
TextureID GLESGraphicsDevice::createTexture2D()
{
    GLuint id;
    glGenTextures(1, &id);
    
    glBindTexture(GL_TEXTURE_2D, id);
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glBindTexture(GL_TEXTURE_2D, 0);
    
    checkGLError();
    return id;
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::setTexture2DAlignment(TextureID id, uint8 alignment)
{
    glBindTexture(GL_TEXTURE_2D, id);
    glPixelStorei(GL_UNPACK_ALIGNMENT, alignment);
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::setTexture2DSubImage(TextureID id,
                                              TextureLevel level,
                                              int16 xoff, int16 yoff,
                                              uint16 width, uint16 height,
                                              textures::Format format,
                                              textures::DataType type,
                                              const void* data)
{
    glBindTexture(GL_TEXTURE_2D, id);
    GLenum glFormat = textures::format::NativeToGL(format);
    GLenum glDataType = textures::dataType::NativeToGL(type);
    
    glTexSubImage2D(GL_TEXTURE_2D,
                    (GLint) level,
                    xoff, yoff,
                    width, height,
                    glFormat,
                    glDataType,
                    (const GLvoid*)data);
    
    checkError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::updateTexture2D(TextureID id,
                                         TextureLevel level,
                                         textures::InternalFormat internal,
                                         uint16 w, uint16 h, int16 b,
                                         textures::Format format,
                                         textures::DataType type,
                                         const void* data)
{
    glBindTexture(GL_TEXTURE_2D, id);
    GLenum glInteral = textures::internalFormat::NativeToGL(internal);
    GLenum glFormat = textures::format::NativeToGL(format);
    GLenum glDataType = textures::dataType::NativeToGL(type);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glTexImage2D(GL_TEXTURE_2D,
                 (GLint) level,
                 glInteral,
                 w, h, b,
                 glFormat,
                 glDataType,
                 (const GLvoid*)data);
    
    checkError();
    
    glBindTexture(GL_TEXTURE_2D, 0);
    
    checkError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::destroyTexture2D(TextureID id)
{
    glDeleteTextures(1, &id);
    checkError();
}
//------------------------------------------------------------------------------
void GLESGraphicsDevice::setState(const RenderState& state)
{
    if (state.isBlendEnabled())
    {
        glEnable(GL_BLEND);
        
        GLenum src = blendFunc::NativeToGL(state.srcBlendFunc());
        GLenum dst = blendFunc::NativeToGL(state.dstBlendFunc());
        
        glBlendFunc(src, dst);
    }
    else
    {
        glDisable(GL_BLEND);
    }
    
    checkGLError();
    
    if ( state.isScissorEnabled() )
    {
        glEnable(GL_SCISSOR_TEST);
        glScissor(state.scissorx(), state.scissory(), state.scissorw(), state.scissorh());
    }
    else
    {
        glDisable(GL_SCISSOR_TEST);
    }
    
    checkGLError();

}
//------------------------------------------------------------------------------
}//namespace graphics
