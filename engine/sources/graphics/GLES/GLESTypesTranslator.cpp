//
//  GLESTypesTranslator.cpp
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#include "GLESTypesTranslator.h"

textures::InternalFormat textures::internalFormat::GLtoNative(GLenum glFormat)
{
    switch (glFormat) {
        case GL_RGB:
            return textures::InternalFormat::RGB;
        case GL_RGBA:
            return textures::InternalFormat::RGBA;
        case GL_ALPHA:
            return textures::InternalFormat::Alpha;
        case GL_R8:
            return textures::InternalFormat::Red8;
        case GL_INVALID_ENUM:
            return textures::InternalFormat::Unknown;
    }
    return textures::InternalFormat::Unknown;
}

GLenum textures::internalFormat::NativeToGL(textures::InternalFormat format)
{
    switch (format) {
        case textures::InternalFormat::RGB:
            return GL_RGB;
        case textures::InternalFormat::RGBA:
            return GL_RGBA;
        case textures::InternalFormat::Alpha:
            return GL_ALPHA;
        case textures::InternalFormat::Red8:
            return GL_R8;
        case textures::InternalFormat::Unknown:
            return GL_INVALID_ENUM;
    }
    return GL_INVALID_ENUM;
}

textures::Format textures::format::GLtoNative(GLenum glFormat)
{
    switch (glFormat) {
        case GL_RGB:
            return textures::Format::RGB;
        case GL_RGBA:
            return textures::Format::RGBA;
        case GL_ALPHA:
            return textures::Format::Alpha;
        case GL_RED:
            return textures::Format::Red;
        case GL_INVALID_ENUM:
            return textures::Format::Unknown;
    }
    return textures::Format::Unknown;
}

GLenum textures::format::NativeToGL(textures::Format format)
{
    switch (format) {
        case textures::Format::RGB:
            return GL_RGB;
        case textures::Format::RGBA:
            return GL_RGBA;
        case textures::Format::Alpha:
            return GL_ALPHA;
        case textures::Format::Red:
            return GL_RED;
        case textures::Format::Unknown:
            return GL_INVALID_ENUM;
    }
    return GL_INVALID_ENUM;
}

textures::Filtering textures::filtering::GLtoNative(GLenum)
{
    return textures::Filtering::Unknown;
}

GLenum textures::filtering::NativeToGL(textures::Filtering)
{
    return GL_INVALID_ENUM;
}

textures::DataType textures::dataType::GLtoNative(GLenum glType)
{
    switch (glType) {
        case GL_UNSIGNED_BYTE:
            return textures::DataType::UnsignedByte;
        case GL_UNSIGNED_INT:
            return textures::DataType::UnsignedInt;
        case GL_UNSIGNED_SHORT:
            return textures::DataType::UnsignedShort;
        case GL_INVALID_ENUM:
            return textures::DataType::Unknown;
    }
    return textures::DataType::Unknown;
}

GLenum textures::dataType::NativeToGL(textures::DataType type)
{
    switch (type) {
        case textures::DataType::UnsignedByte:
            return GL_UNSIGNED_BYTE;
        case textures::DataType::UnsignedInt:
            return GL_UNSIGNED_INT;
        case textures::DataType::UnsignedShort:
            return GL_UNSIGNED_SHORT;
        case textures::DataType::Unknown:
            return GL_INVALID_ENUM;
    }
    return GL_INVALID_ENUM;

}

graphics::RenderState::BlendFunc graphics::blendFunc::GLtoNative(GLenum glFunc)
{
    switch (glFunc) {
        case GL_ONE:
            return graphics::RenderState::BlendFunc::One;
        case GL_SRC_ALPHA:
            return graphics::RenderState::BlendFunc::SrcAlpha;
        case GL_ONE_MINUS_SRC_ALPHA:
            return graphics::RenderState::BlendFunc::OneMinusSrcAlpha;
        case GL_INVALID_ENUM:
            return graphics::RenderState::BlendFunc::Unknown;
    }
    return graphics::RenderState::BlendFunc::Unknown;
}

GLenum graphics::blendFunc::NativeToGL(graphics::RenderState::BlendFunc func)
{
    switch (func) {
        case graphics::RenderState::BlendFunc::One:
            return GL_ONE;
        case graphics::RenderState::BlendFunc::SrcAlpha:
            return GL_SRC_ALPHA;
        case graphics::RenderState::BlendFunc::OneMinusSrcAlpha:
            return GL_ONE_MINUS_SRC_ALPHA;
        case graphics::RenderState::BlendFunc::Unknown:
            return GL_INVALID_ENUM;
    }
    return GL_INVALID_ENUM;
}

graphics::DrawMode graphics::drawMode::GLtoNative(GLenum glMode)
{
    switch (glMode) {
        case GL_LINES:
            return graphics::DrawMode::Lines;
        case GL_LINE_STRIP:
            return graphics::DrawMode::LineStrip;
        case GL_TRIANGLES:
            return graphics::DrawMode::Triangles;
        case GL_TRIANGLE_STRIP:
            return graphics::DrawMode::TriangleStrip;
    }
    return graphics::DrawMode::Unknown;
}

GLenum graphics::drawMode::NativeToGL(graphics::DrawMode mode)
{
    switch (mode) {
        case graphics::DrawMode::Lines:
            return GL_LINES;
        case graphics::DrawMode::LineStrip:
            return GL_LINE_STRIP;
        case graphics::DrawMode::Triangles:
            return GL_TRIANGLES;
        case graphics::DrawMode::TriangleStrip:
            return GL_TRIANGLE_STRIP;
        case graphics::DrawMode::Unknown:
            return GL_INVALID_ENUM;
    }
    return GL_INVALID_ENUM;
}
