//
//  GLESProgramFactory.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "GLESProgramFactory.h"
#include "GLESPrerequisites.h"

#include <graphics/RenderPrimitives.h>

#include <core/memory/memory.h>
#include <common/formatstring.h>
#include <asserts.h>
#include <logs.h>

#include <vector>

#include <core/file_system/FileSystem.h>

namespace graphics
{
//------------------------------------------------------------------------------
bool GLESProgramFactory::loadFile(std::string& source, const char* filepath)
{
    FileSystem::File file = FileSystem::loadFile(filepath, FileSystem::LocationType::Resources);
    bool result = !file.empty();
    if (result)
    {
        auto length = file.length();
        char* buffer = nullptr;
        STACK_ALLOC(length + 1, buffer);
        file.read(buffer, length);
        buffer[length] = '\0';
        source = static_cast<const char*>( buffer );
    }
    return result;
}
//------------------------------------------------------------------------------
void GLESProgramFactory::checkShader(GLuint id, GLint *result, std::vector<char> *errorMessage)
{
    glGetShaderiv(id, GL_COMPILE_STATUS, result);
    GLint logLength(0);
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &logLength);
    errorMessage->resize(logLength);
    glGetShaderInfoLog(id, logLength, NULL, &(*errorMessage)[0]);
}
//------------------------------------------------------------------------------
void GLESProgramFactory::compileShader(ProgramID id, const char* source)
{
    glShaderSource(id, 1, &source , NULL);
    glCompileShader(id);
}
//------------------------------------------------------------------------------
ProgramID GLESProgramFactory::createProgram(const char* vertex, const char* fragment)
{
    std::string vertexSource;
    std::string fragmentSource;
    
    bool fragloaded = loadFile(fragmentSource, fragment);
    bool vertloaded = loadFile(vertexSource, vertex);
    
    msg_assert(fragloaded && vertloaded, "shader doesn\'t loads");
    if (!fragloaded || !vertloaded)
    {
        return INVALID_PROGRAM_ID;
    }
    
    GLuint vertShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint fragShaderID = glCreateShader(GL_FRAGMENT_SHADER);
    
    GLint result = GL_FALSE;
    
    std::vector<char> errorMessage;
    
    compileShader(vertShaderID, vertexSource.c_str());
    checkShader(vertShaderID, &result, &errorMessage);
    
    msg_assert(!errorMessage.size()
               || ( errorMessage.size() == 1 && errorMessage[0] == '\0' ),
               format_string("vertex error logs: %s [%s]", &errorMessage[0], vertex).c_str());
    
    if (errorMessage.size() && errorMessage[0] != '\0') {
        return INVALID_PROGRAM_ID;
    }
    
    compileShader(fragShaderID, fragmentSource.c_str());
    checkShader(fragShaderID, &result, &errorMessage);
    
    msg_assert(!errorMessage.size()
               || ( errorMessage.size() == 1 && errorMessage[0] == '\0' ),
               format_string("fragment error logs: %s [%s]", &errorMessage[0], fragment).c_str());
    
    if (errorMessage.size() && errorMessage[0] != '\0') {
        return INVALID_PROGRAM_ID;
    }
    
    GLuint programID = glCreateProgram();
    glAttachShader(programID, vertShaderID);
    glAttachShader(programID, fragShaderID);
    
    glBindAttribLocation(programID, (GLuint)VertexAttribute::Position, "position");
    glBindAttribLocation(programID, (GLuint)VertexAttribute::Color, "color");
    glBindAttribLocation(programID, (GLuint)VertexAttribute::Texture, "uvcoord");
    glBindAttribLocation(programID, (GLuint)VertexAttribute::Shift, "shift");
    glBindAttribLocation(programID, (GLuint)VertexAttribute::Gamma, "gamma");
    
    glLinkProgram(programID);
    
    int logLength(0);
    glGetProgramiv(programID, GL_LINK_STATUS, &result);
    glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &logLength);
    
    std::vector<char> programErrorMessage( (logLength < int(1)) ? (int(1)) : (logLength) );
    glGetProgramInfoLog(programID, logLength, NULL, &programErrorMessage[0]);
    
    if (programErrorMessage.size() && programErrorMessage[0] != '\0')
    {
        LOGE("program error logs: %s", &programErrorMessage[0]);
    }
    
    if (programErrorMessage.size() && programErrorMessage[0] != '\0' && programID == 0) {
        return INVALID_PROGRAM_ID;
    }
    
    glDeleteShader(vertShaderID);
    glDeleteShader(fragShaderID);
    
    return programID;
}
//------------------------------------------------------------------------------
void GLESProgramFactory::destroyShader(ProgramID id)
{
    glDeleteProgram(id);
}
//------------------------------------------------------------------------------
void GLESProgramFactory::createUniformContainer(ProgramID id, const UEArray& array, UniformContainer* containers)
{
    for (uint64 i = 0; i < array.size; ++i)
    {
        UniformElement& elem = array.elements[i];
        int location = glGetUniformLocation((GLuint)id, elem.name.c_str());
        
        msg_assert(location > -1, format_string("bad location for id %s", elem.name.c_str()).c_str());
        
        UniformContainer& container = containers[i];
        container.location = location;
        container.type = elem.type;
    }
}
//------------------------------------------------------------------------------
}//namespace graphics
