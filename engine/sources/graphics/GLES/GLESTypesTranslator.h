//
//  GLESTypesTranslator.h
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#pragma once

#include "GLESPrerequisites.h"
#include <graphics/textures/TextureTypes.h>
#include <graphics/RenderState.h>

namespace textures
{
namespace internalFormat
{

textures::InternalFormat GLtoNative(GLenum);
GLenum NativeToGL(textures::InternalFormat);

}//namespace internalFormat
namespace format
{

textures::Format GLtoNative(GLenum);
GLenum NativeToGL(textures::Format);
    
}//namespace format
    
namespace filtering
{
    
textures::Filtering GLtoNative(GLenum);
GLenum NativeToGL(textures::Filtering);
    
}//namespace filtering
    
namespace dataType
{
    
textures::DataType GLtoNative(GLenum);
GLenum NativeToGL(textures::DataType);
    
}//namespace dataType

}//namespace textures

namespace graphics
{

namespace blendFunc
{
    
RenderState::BlendFunc GLtoNative(GLenum);
GLenum NativeToGL(RenderState::BlendFunc);
    
}//namespace blendFunc
    
namespace drawMode
{
    
DrawMode GLtoNative(GLenum);
GLenum NativeToGL(DrawMode);
    
}//namespace blendFunc

}//namespace graphics

