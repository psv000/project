//
//  GLESProgramFactory.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include "GLESProgramFactory.h"
#include "GLESPrerequisites.h"

#include <graphics/shaders/IProgramFactory.h>

#include <math/vec2.h>
#include <math/vec4.h>
#include <math/mat4.h>

#include <vector>

namespace graphics
{

class Texture;
class GLESProgramFactory final : public IProgramFactory
{
public:
    ProgramID createProgram(const char*, const char*) override;
    void destroyShader(ProgramID) override;
    
    void createUniformContainer(ProgramID, const UEArray&, UniformContainer*) override;
    
private:
    bool loadFile(std::string& source, const char* file);
    void checkShader(GLuint id, GLint *result, std::vector<char> *errorMessage);
    void compileShader(ProgramID, const char*);
};

}//namespace graphics
