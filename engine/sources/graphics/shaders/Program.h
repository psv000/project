//
//  Program.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <graphics/RenderPrimitives.h>

namespace graphics
{

class Program
{
public:
    Program() = delete;
    ~Program();
    Program(const ProgramID id);
    Program(const Program&);
    Program& operator=(const Program&);
    
    ProgramID id() const { return mID; }
    
private:
    ProgramID mID;
};

}//namespace graphics
