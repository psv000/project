//
//  ProgramArgs.cpp
//  engine
//
//  Created by s.popov on 25/10/2017.
//
//

#include "ProgramArgs.h"

#include <core/Activity.h>
#include <graphics/Render.h>
#include <graphics/IGraphicsDevice.h>

#define GET_ARG(name) \
ProgramArg* ptr = argument(name); \
if ( !ptr ) \
{ \
    void_assert(false); \
    return; \
} \
ProgramArg& arg = *ptr;

namespace graphics
{
//------------------------------------------------------------------------------
void ProgramArgs::applyArgument(const char *name, float value)
{
    GET_ARG( name );
    arg.type = UniformType::Float;
    arg.v.f = value;
}
//------------------------------------------------------------------------------
void ProgramArgs::applyArgument(const char *name, int value)
{
    GET_ARG( name );
    arg.type = UniformType::Int;
    arg.v.i = value;
}
//------------------------------------------------------------------------------
void ProgramArgs::applyArgument(const char *name, const mat4f &mat)
{
    GET_ARG( name );
    arg.type = UniformType::Mat4;
    arg.v.mat4x4 = mat;
}
//------------------------------------------------------------------------------
void ProgramArgs::applyArgument(const char *name, const vec4f &vec)
{
    GET_ARG( name );
    arg.type = UniformType::Vec4;
    arg.v.vec4 = vec;
}
//------------------------------------------------------------------------------
void ProgramArgs::applyArgument(const char *name, const vec3f &vec)
{
    GET_ARG( name );
    arg.type = UniformType::Vec3;
    arg.v.vec3 = vec;
}
//------------------------------------------------------------------------------
void ProgramArgs::applyArgument(const char *name, const vec2f &vec)
{
    GET_ARG( name );
    arg.type = UniformType::Vec2;
    arg.v.vec2 = vec;
}
//------------------------------------------------------------------------------
void ProgramArgs::applyArgument(const char *name, const TexturePack &tex)
{
    GET_ARG( name );
    arg.type = UniformType::Texture2D;
    arg.v.texture = tex;
}
//------------------------------------------------------------------------------
void ProgramArgs::registerArgument(const char* name, UniformLocation location)
{
    ProgramArg* arg = argument(name);
    if ( !arg )
    {
        mArguments.emplace_back();
        arg = &mArguments.back();
        arg->name = name;
    }
    arg->location = location;
}
//------------------------------------------------------------------------------
void ProgramArgs::registerArgument(ProgramID id, const char* name, UniformType type)
{
    UEArray ueArray;
    
    UniformElement elements[] = { {name, type} };
    
    ueArray.elements = elements;
    ueArray.size = lengthof(elements);
    
    UniformContainer containers[ueArray.size];
    
    auto device = Activity::render()->graphicsDevice();
    device->applyShader(id);
    device->createUniformContainer(id, ueArray, containers);
    
    ProgramArgs::registerArgument(name, containers[0].location);
}
//------------------------------------------------------------------------------
void ProgramArgs::removeArgument(const char *name)
{
    const string key(name);
    int64 index = -1;
    for (uint64 i = 0; i < mArguments.size(); ++i)
    {
        if ( key == mArguments[i].name )
        {
            index = i;
        }
    }
    if ( index >= 0 )
    {
        mArguments.erase(mArguments.begin() + index);
    }
}
//------------------------------------------------------------------------------
void ProgramArgs::clear()
{
    mArguments.clear();
}
//------------------------------------------------------------------------------
}//namespace graphics
