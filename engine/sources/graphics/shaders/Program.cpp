//
//  Program.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "Program.h"

namespace graphics
{
//------------------------------------------------------------------------------
Program::Program(const ProgramID id):
mID(id)
{

}
//------------------------------------------------------------------------------
Program::Program(const Program& shader):
mID(shader.mID)
{
}
//------------------------------------------------------------------------------
Program& Program::operator=(const Program& shader)
{
    if (this != &shader)
    {
        mID = shader.mID;
    }
    return *this;
}
//------------------------------------------------------------------------------
Program::~Program()
{

}
}//namespace graphics
