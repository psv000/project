//
//  ShaderProgramLoader.cpp
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#include "ShaderProgramLoader.h"

#include <graphics/shaders/IProgramFactory.h>
#include "Program.h"

#include <core/memory/memory.h>
#include <core/memory/allocations.h>
#include <core/memory/MemoryManager.h>

#include <utils/cache.h>
#include <utils/hash.h>

#include <common/inttypes.h>
#include <common/formatstring.h>
#include <logs.h>

#define PROGRAM_SIZE ( GET_MAX(sizeof(Program), sizeof(void*)) )
#define PROGRAM_COUNT_IN_POOL (32)

namespace graphics
{
namespace ShaderProgramLoader
{

memory::PoolAllocator* gShaderPool = nullptr;

using ShaderCacheInfo = utils::CacheInfo<Program>;
using ShaderCache = utils::CacheCollection<Program>;
ShaderCache gCache;
    
}//namespace ShaderProgramLoader
//------------------------------------------------------------------------------
void ShaderProgramLoader::init()
{
    gShaderPool = memory::MemoryManager::createPool<Program>("shader_allocator",
                                                             PROGRAM_COUNT_IN_POOL);
    
    gCache.realloc(PROGRAM_COUNT_IN_POOL);
}
//------------------------------------------------------------------------------
void ShaderProgramLoader::deinit()
{
    msg_assert(gCache.size() == 0, "shaders cache isn\'t empty");
    memory::MemoryManager::destructPool(gShaderPool);
}
//------------------------------------------------------------------------------
Program* ShaderProgramLoader::createProgram(IProgramFactory* factory, const char* pathToVertex, const char* pathToFragment)
{
    auto hash = utils::hash::string(format_string("%s%s", pathToVertex, pathToFragment).c_str());
    ShaderCacheInfo info {hash, nullptr, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        ++(gCache[i].ref_count);
        return gCache[i].item;
    }
    
    ProgramID shaderID = factory->createProgram(pathToVertex, pathToFragment);
    msg_assert(shaderID != INVALID_PROGRAM_ID, "invalid shader id");
    
    auto program = memory::allocate<Program>(*gShaderPool, shaderID);
    
    gCache.push_back({hash, program, 1});
    return program;
}
//------------------------------------------------------------------------------
void ShaderProgramLoader::destroyProgram(IProgramFactory* factory, Program* shader)
{
    ShaderCacheInfo info {utils::hash::invalid_hash, shader, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        auto &info = gCache[i];
        --(info.ref_count);
        if (info.ref_count != 0)
            return;
        gCache.erase(info);
    }
    factory->destroyShader(shader->id());
    memory::deallocate(*gShaderPool, shader);
}
//------------------------------------------------------------------------------
}//namespace graphics
