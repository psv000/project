//
//  ProgramArgs.h
//  engine
//
//  Created by s.popov on 25/10/2017.
//
//

#pragma once

#include <graphics/RenderPrimitives.h>
#include <core/memory/memory.h>
#include <utils/arrayutils.h>
#include <vector>
#include <map>

#include <math/vec2.h>
#include <math/vec3.h>
#include <math/vec4.h>
#include <math/mat4.h>

namespace graphics
{

//------------------------------------------------------------------------------
struct ProgramArg {

    string name;
    
    UniformLocation location;
    UniformType type;
    
    union Value
    {
        Value(): mat4x4(0.f) {}
        
        float f;
        int i;
        vec4f vec4;
        vec2f vec3;
        vec2f vec2;
        mat4f mat4x4;
        TexturePack texture;
        
    } v;
    
    float floatVal()  const { return v.f; }
    int intVal() const { return v.i; }
    const vec4f& vec4fVal() const { return v.vec4; }
    const vec2f& vec3fVal() const { return v.vec3; }
    const vec2f& vec2fVal() const { return v.vec2; }
    const mat4f& mat4fVal() const { return v.mat4x4; }
    const TexturePack& textureVal() const { return v.texture; }
};
//------------------------------------------------------------------------------
class ProgramArgs
{
public:
    ProgramArgs() {}
    ProgramArgs(const ProgramArgs &) = delete;
    ProgramArgs &operator=(const ProgramArgs &) = delete;
    
    void applyArgument(const char*, float);
    void applyArgument(const char*, int);
    void applyArgument(const char*, const mat4f&);
    void applyArgument(const char*, const vec4f&);
    void applyArgument(const char*, const vec3f&);
    void applyArgument(const char*, const vec2f&);
    void applyArgument(const char*, const TexturePack&);
    
    void registerArgument(const char*, UniformLocation);
    void registerArgument(ProgramID, const char*, UniformType);
    void removeArgument(const char *);
    
    void clear();
    
    uint8 count() const { return mArguments.size(); }
    const ProgramArg &at(uint8 i) const { return mArguments.at(i); }
    
private:
    inline ProgramArg* argument(const char *name)
    {
        ProgramArg *argument = nullptr;
        const string key(name);
        
        for (auto& arg : mArguments)
        {
            if (arg.name == key)
            {
                argument = &arg;
                break;
            }
        }
        
        return argument;
    }
        
private:
    std::vector<ProgramArg> mArguments;
};
    
}//namespace graphics
