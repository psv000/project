//
//  IProgramFactory.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <graphics/RenderPrimitives.h>

namespace graphics
{

class IProgramFactory
{
public:
    virtual ~IProgramFactory() {}
    
    virtual ProgramID createProgram(const char*, const char*) = 0;
    virtual void destroyShader(ProgramID) = 0;
    
    virtual void createUniformContainer(ProgramID, const UEArray&, UniformContainer*) = 0;
};

}//namespace graphics
