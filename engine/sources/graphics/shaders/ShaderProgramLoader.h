//
//  ShaderProgramLoader.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

namespace graphics
{
    
class Program;
class IProgramFactory;
    
namespace ShaderProgramLoader
{

void init();
void deinit();

Program* createProgram(IProgramFactory*, const char*, const char*);
void destroyProgram(IProgramFactory*, Program*);
    
}//namespace ShaderProgramLoader
}//namespace graphics
