//
//  MaterialManager.h
//  engine
//
//  Created by s.popov on 04/10/2017.
//
//

#pragma once

#include <graphics/RenderPrimitives.h>

namespace graphics
{
    
struct Material;
namespace MaterialManager
{

void init();
void deinit();
    
Material* obtainMaterial(const char*);
Material* createMaterial();
void releaseMaterial(Material*);
    
bool loadTextureAtlas(const char*);
    
}//namespace MaterialManager
    
}//namespace graphics
