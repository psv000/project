//
//  Colors.h
//  engine
//
//  Created by s.popov on 12/10/2017.
//
//

#pragma once

#include <math/vec4.h>

#define __BYTE_MAX__ (255.f)

using Color4F = vec4f;
using Color4B = vec4b;

namespace Color
{
    
const Color4B None    { 255,  0,      0,  0   };
const Color4B Black   { 0,    0,      0,  255 };
const Color4B White   { 255,  255,  255,  255 };
const Color4B Red     { 255,    0,    0,  255 };
const Color4B Green   {   0,  255,    0,  255 };
const Color4B TransparentGreen {   0,  255,    0,  120 };
const Color4B DarkGreen { 0,  128,    0,  255 };
const Color4B Blue    {   0,    0,  255,  255 };
const Color4B Yellow  { 255,  255,    0,  255 };
    
const Color4F White_F  { 1.f,  1.f,  1.f,  1.f };
    
inline Color4F normalize(const Color4B& color)
{
    Color4F outColor;
    
    outColor.r = (float)color.r / __BYTE_MAX__;
    outColor.g = (float)color.g / __BYTE_MAX__;
    outColor.b = (float)color.b / __BYTE_MAX__;
    outColor.a = (float)color.a / __BYTE_MAX__;
    
    return outColor;
}
    
inline Color4B denormalize(const Color4F& color)
{
    Color4B outColor;
    
    outColor.r = color.r * __BYTE_MAX__;
    outColor.g = color.g * __BYTE_MAX__;
    outColor.b = color.b * __BYTE_MAX__;
    outColor.a = color.a * __BYTE_MAX__;
    
    return outColor;
}
    
}//namespace Color
