//
//  MaterialManager.cpp
//  engine
//
//  Created by s.popov on 04/10/2017.
//
//

#include "MaterialManager.h"

#include "Material.h"

#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include <core/memory/allocations.h>
#include <core/memory/MemoryManager.h>

#include <utils/hash.h>
#include <utils/cache.h>

#include <graphics/textures/Texture.h>
#include <graphics/textures/texture_loader/TextureLoader.h>

#include <core/file_system/FileSystem.h>
#include <common/formatstring.h>
#include <math/vec2.h>

#include <jansson.h>

#include <logs.h>

#define MATERIALS_COUNT_IN_POOL (16)
#define MATERIALS_CACHE_SIZE (512)

namespace graphics
{
    
namespace MaterialManager
{
    
Material* createMaterial(const char*);
void destroyMaterial(Material*);

memory::PoolAllocator* gMaterialPool = nullptr;

using MaterialCacheInfo = utils::CacheInfo<Material>;
using MaterialCache = utils::CacheCollection<Material>;
MaterialCache gCache;
    
uint32 mNewMaterialID;

}//namespace MaterialManager
//------------------------------------------------------------------------------
void MaterialManager::init()
{
    gMaterialPool = memory::MemoryManager::createPool<Material>("material_allocator",
                                                                MATERIALS_COUNT_IN_POOL);
    
    gCache.realloc(MATERIALS_CACHE_SIZE);
}
//------------------------------------------------------------------------------
void MaterialManager::deinit()
{
    
    while( gCache.size() )
    {
        auto info = gCache[0];
        msg_assert(info.ref_count == 1, format_string("Material with id <%d> has unreleased references",
                                                     info.item->id).c_str());
        MaterialManager::destroyMaterial(info.item);
    }
    
    msg_assert(gCache.size() == 0, "materials cache isn\'t empty");
    
    memory::MemoryManager::destructPool(gMaterialPool);
}
//------------------------------------------------------------------------------
Material* MaterialManager::createMaterial(const char* name)
{
    auto hash = utils::hash::string(name);
    MaterialCacheInfo info {hash, nullptr, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        ++(gCache[i].ref_count);
        return gCache[i].item;
    }
    
    Material* material = memory::allocate<Material>(*gMaterialPool, hash);
    gCache.push_back({hash, material, 1});
    return material;
}
//------------------------------------------------------------------------------
void MaterialManager::destroyMaterial(Material* material)
{
    MaterialCacheInfo info {utils::hash::invalid_hash, material, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        auto &info = gCache[i];
        --(info.ref_count);
        if (info.ref_count != 0)
            return;
        gCache.erase(info);
    }
    textures::TextureLoader::releaseTexture(material->texture);
    memory::deallocate(*gMaterialPool, material);
}
//------------------------------------------------------------------------------
Material* MaterialManager::obtainMaterial(const char* name)
{
    auto hash = utils::hash::string(name);
    MaterialCacheInfo info {hash, nullptr, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        ++(gCache[i].ref_count);
        return gCache[i].item;
    }
    
    return nullptr;
}
//------------------------------------------------------------------------------
Material* MaterialManager::createMaterial()
{
    string name = format_string("%ud", ++mNewMaterialID);
    auto hash = utils::hash::string(name.c_str());
    MaterialCacheInfo info {hash, nullptr, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        msg_assert(false, "material hash collision");
        return nullptr;
    }
    
    Material* material = memory::allocate<Material>(*gMaterialPool, hash);
    material->name = name;
    gCache.push_back({hash, material, 1});
    return material;
}
//------------------------------------------------------------------------------
void MaterialManager::releaseMaterial(Material* material)
{
    MaterialCacheInfo info {utils::hash::invalid_hash, material, 0};
    int64 i = gCache.find(info);
    msg_assert(i >= 0, format_string("material with id <%d> not found", material->id).c_str());
    if (i >= 0)
    {
        auto &info = gCache[i];
        --(info.ref_count);
        
        msg_assert(info.ref_count != 0, format_string("material with id <%d> has 0 reference", material->id).c_str());
    }
}
//------------------------------------------------------------------------------
bool MaterialManager::loadTextureAtlas(const char* filename)
{
    textures::Texture* texture = textures::TextureLoader::loadFile(format_string("%s.png", filename).c_str());
    FileSystem::File file = FileSystem::loadFile(format_string("%s.json", filename).c_str(),
                                                 FileSystem::LocationType::Resources);
    if ( !file.empty() )
    {
        json_error_t error;
        char* buffer = nullptr;
        STACK_ALLOC(file.length(), buffer);
        file.read(buffer, file.length());
        json_t* json = json_loadb(buffer, file.length(), 0, &error);
        if (!json)
        {
            LOGE("error while json loading [line: %d | column: %d | position: %d] with message: %s. <source: %s>",
                 error.line,
                 error.column,
                 error.position,
                 error.text,
                 error.source);
            return false;
        }
        
        json_t* frames = json_object_get(json, "frames");
        uint64 framesCount = json_array_size(frames);
        vec2f textureSize(texture->size());
        for (uint64 i = 0; i < framesCount; ++i)
        {
            json_t* item = json_array_get(frames, i);
            const char* name = json_string_value(json_object_get(item, "filename"));
            
            json_t* frame = json_object_get(item, "frame");
            vec2f origin(json_integer_value(json_object_get(frame, "x")),
                         json_integer_value(json_object_get(frame, "y")));
            vec2f size  (json_integer_value(json_object_get(frame, "w")),
                         json_integer_value(json_object_get(frame, "h")));
            
            Material* material = MaterialManager::createMaterial(name);
            {
                material->name = name;
                auto forigin = origin / textureSize;
                material->origin = forigin * vec2f(USHRT_MAX);
                material->rect = ( forigin + size / textureSize ) * vec2f(USHRT_MAX);
                
                material->origin.y = USHRT_MAX - material->origin.y;
                material->rect.y = USHRT_MAX - material->rect.y;
                
                auto height = material->origin.y;
                material->origin.y = material->rect.y;
                material->rect.y = height;
                
                material->size = size;
                material->texture = texture;

                textures::TextureLoader::obtainTexture(texture);
            }
        }
        
        json_delete(json);

        textures::TextureLoader::releaseTexture(texture);
        
        return true;
    }
    
    return false;
}
//------------------------------------------------------------------------------
}//namespace graphics
