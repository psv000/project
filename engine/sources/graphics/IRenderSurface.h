//
//  IRenderSurface.h
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#pragma once

#include <math/vec2.h>
#include <common/inttypes.h>
#include <graphics/RenderPrimitives.h>

namespace graphics
{
    
class IRenderSurface
{
public:
    virtual ~IRenderSurface() {}
    virtual void onResize(uint16, uint16) = 0;
    virtual void apply() = 0;
    virtual void present() = 0;
    
    virtual vec2u size() const = 0;
    
    virtual void MSAAon(uint8) = 0;
    virtual void MSAAoff() = 0;
};
    
}//namespace graphics
