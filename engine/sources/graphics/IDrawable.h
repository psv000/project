//
//  IDrawable.h
//  engine
//
//  Created by s.popov on 28/09/2017.
//
//

#pragma once

#include <common/inttypes.h>

namespace graphics
{
    
class IDrawable
{
public:
    virtual ~IDrawable() {};
    
    virtual void update() = 0;
    
    virtual void setZOrder(int order) = 0;
    virtual int zOrder() const = 0;
};
    
}//namespace graphics
