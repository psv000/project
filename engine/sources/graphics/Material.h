//
//  Material.h
//  engine
//
//  Created by s.popov on 04/10/2017.
//
//

#pragma once

#include <graphics/RenderPrimitives.h>

#include <common/inttypes.h>
#include <common/stringtypes.h>
#include <math/vec2.h>

namespace textures
{
    
class Texture;
    
}//namespace textures

namespace graphics
{

struct Material final
{
    Material(MaterialID identifier): id(identifier) {}
    
    MaterialID id;
    string name;
    textures::Texture* texture;
    vec2us origin;
    vec2us rect;
    vec2us size;
};
    
}//namespace graphics
