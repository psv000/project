//
//  Render.h
//  engine
//
//  Created by s.popov on 16/09/2017.
//
//

#pragma once

#include "RenderOptions.h"
#include "RenderPrimitives.h"
#include "RenderCommandQueue.h"

#include "IRenderSurface.h"

#include "scene/Camera.h"

#include "Colors.h"

#include <vector>
#include <math/vec2.h>

namespace graphics
{

class SceneContainer;
class Label;
    
class IDrawable;
class Program;
class IGraphicsDevice;
class Render
{
public:
    Render();
    Render(const Render&);
    Render& operator=(const Render&);
    
    virtual ~Render();
    
    void setup(RenderOptions);
    void deinit();
    
    void setClearColor(const Color4F&);
    void setContextSize(uint16 width, uint16 height);
    
    void beforeUpdate();
    void update(float);
    void afterUpdate();
    void draw();
    
    vec2u contextSize() const;
    
    IGraphicsDevice* graphicsDevice() { return mGraphicsDevice; }
    IRenderSurface* renderSurface() { return mRenderSurface; }
    RenderCommandQueue& renderCommandQueue() { return mCommandQueue; }
    
    void registerDrawable(IDrawable*);
    void unregisterDrawable(IDrawable*);
    
    Camera* ortographicScreenCamera() { return &mOrtoScreenCamera; }
        
protected:
    RenderOptions mOptions;
    IGraphicsDevice* mGraphicsDevice;
    IRenderSurface* mRenderSurface;
    RenderCommandQueue mCommandQueue;
    
    using Drawables = std::vector<IDrawable*>;
    Drawables mDrawables;
    
    Camera mOrtoScreenCamera;
    SceneContainer* mSystemScene;
    Label* mSysInfo;
    
    uint16 mFPS;
    uint16 mUpdatesPerSecond;
    
    float mDrawablesUpdateDuration;
    float mDrawDuration;
    
    vec2u mContexSize;
};

}//namespace graphics
