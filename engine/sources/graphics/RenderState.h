//
//  RenderState.h
//  engine
//
//  Created by s.popov on 05/10/2017.
//
//

#pragma once

#include "RenderPrimitives.h"
#include <common/inttypes.h>

namespace graphics
{

class RenderState final
{
public:
    static const uint32 SCISSOR_ENABLED { 1 << 2 };
    static const uint32 BLEND_ENABLED   { 1 << 1 };
    static const uint32 CLEAR_FLAGS     { 0 };
    
    enum class BlendFunc : uint8
    {
        One = 0,
        SrcAlpha,
        OneMinusSrcAlpha,
        Unknown
    };

public:
    
    RenderState();
    
    void setBlendFunc(BlendFunc src, BlendFunc dst);
    
    BlendFunc srcBlendFunc() const { return mSrcFunc; }
    BlendFunc dstBlendFunc() const { return mDstFunc; }
    
    void setFlags(uint32 flags) { mFlags |= flags; }
    void unsetFlags(uint32 flags) { mFlags &= ~flags; }
    
    void setScissor(uint16, uint16, uint16, uint16);
    
    bool isBlendEnabled() const { return mFlags & BLEND_ENABLED; }
    bool isScissorEnabled() const { return mFlags & SCISSOR_ENABLED; }
    
    uint16 scissorx() const { return mScissor.x; }
    uint16 scissory() const { return mScissor.y; }
    uint16 scissorw() const { return mScissor.w; }
    uint16 scissorh() const { return mScissor.h; }
    
private:
    uint32 mFlags;
    
    struct
    {
        uint16 x, y, w, h;
    }
    mScissor;
    
    BlendFunc mSrcFunc;
    BlendFunc mDstFunc;
    
};
    
}//namespace graphics
