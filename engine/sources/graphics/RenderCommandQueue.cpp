//
//  RendercommandQueue.cpp
//  engine
//
//  Created by s.popov on 21/09/2017.
//
//

#include "RenderCommandQueue.h"

#include <core/Activity.h>
#include "Render.h"
#include "IGraphicsDevice.h"

#include "textures/TextureTypesHelpers.h"

#include <core/memory/memory.h>
#include <utils/arrayutils.h>
#include <asserts.h>

#include <logs.h>

#define RENDER_QUEUE_SIZE (128 * 1024) // 128 kB

namespace graphics
{
//------------------------------------------------------------------------------
RenderCommandQueue::RenderCommandQueue():
mDevice(nullptr),
mCommands(nullptr),
mCommandsAllocated(0),
mCommandsBufferLength(0),
mDebugPointer(nullptr),
mQueueFullness(0.)
{
}
//------------------------------------------------------------------------------
void RenderCommandQueue::init(IGraphicsDevice* device)
{
    mDevice = device;
    mCommandsBufferLength = RENDER_QUEUE_SIZE;
    mCommands = (byte*) memory::alloc(sizeof(byte) * mCommandsBufferLength);
    memset(mCommands, 0, sizeof(byte) * mCommandsBufferLength);
}
//------------------------------------------------------------------------------
void RenderCommandQueue::deinit()
{
    memory::dealloc(mCommands);
    mCommands = nullptr;
}
//------------------------------------------------------------------------------
RenderCommandQueue::~RenderCommandQueue()
{
}
//------------------------------------------------------------------------------
void RenderCommandQueue::beginCommand(RenderCommandType type)
{
    RenderCommandQueue::addValue(type);
}
//------------------------------------------------------------------------------
void RenderCommandQueue::sort()
{
    
}
//------------------------------------------------------------------------------
void RenderCommandQueue::clear()
{
    mCommandsAllocated = 0;
}
//------------------------------------------------------------------------------
void RenderCommandQueue::proccessQueue()
{
    beginCommand(RenderCommandType::Empty);
    endCommand();
    
    mQueueFullness = (double)mCommandsAllocated / mCommandsBufferLength;
    
    byte* start = mCommands;
    uint64 length = mCommandsBufferLength;
    byte* ptr = start;
    
    bool eof = false;
    while ( ( (ptr - start) <= length ) && !eof)
    {
        RenderCommandType cmd = array::readValue<RenderCommandType>(ptr);
        switch(cmd)
        {
            case RenderCommandType::ApplyProgram:
            {
                ProgramID id = array::readValue<ProgramID>(ptr);
                mDevice->applyShader(id);
                break;
            }
            case RenderCommandType::ApplyUniforms:
            {
                ProgramID id = array::readValue<ProgramID>(ptr);
                uint8 count = array::readValue<uint8>(ptr);
                uint64 offset = array::readValue<uint64>(ptr);
                
                mDevice->applyUniforms(id, (void*)ptr, count);
                ptr += offset;
                break;
            }
            case RenderCommandType::DrawMesh:
            {
                VertexArrayObjectID vao = array::readValue<VertexArrayObjectID>(ptr);
                uint64 indicesCount = array::readValue<uint64>(ptr);
                mDevice->drawMesh(vao, indicesCount);
                break;
            }
            case RenderCommandType::DrawLine:
            {
                VertexArrayObjectID vao = array::readValue<VertexArrayObjectID>(ptr);
                uint32 count = array::readValue<uint32>(ptr);
                uint8 width = array::readValue<uint8>(ptr);
                DrawMode mode = array::readValue<DrawMode>(ptr);
                mDevice->drawLine(vao, count, width, mode);
                break;
            }
            case RenderCommandType::ClearContext:
            {
                mDevice->clearContext();
                break;
            }
            case RenderCommandType::SetTexture2DAlignment:
            {
                TextureID id = array::readValue<TextureID>(ptr);
                uint8 alignment = array::readValue<uint8>(ptr);
                mDevice->setTexture2DAlignment(id, alignment);
                break;
            }
            case RenderCommandType::SetTexture2DSubImage:
            {
                TextureID id = array::readValue<TextureID>(ptr);
                TextureLevel level = array::readValue<TextureLevel>(ptr);
                int16 xoff = array::readValue<int16>(ptr);
                int16 yoff = array::readValue<int16>(ptr);
                uint16 width = array::readValue<uint16>(ptr);
                uint16 height = array::readValue<uint16>(ptr);
                textures::Format format = array::readValue<textures::Format>(ptr);
                textures::DataType type = array::readValue<textures::DataType>(ptr);
                void* data = (void*)ptr;
                
                uint8 bpp = textures::bppByFormat(format);
                ptr += (width + xoff) *  (height * yoff) * bpp;
                
                mDevice->setTexture2DSubImage(id, level, xoff, yoff, width, height, format, type, data);
                break;
            }
            case RenderCommandType::UpdateTexture2D:
            {
                TextureID id = array::readValue<TextureID>(ptr);
                TextureLevel level = array::readValue<TextureLevel>(ptr);
                textures::InternalFormat internal = array::readValue<textures::InternalFormat>(ptr);
                uint16 width = array::readValue<uint16>(ptr);
                uint16 height = array::readValue<uint16>(ptr);
                int16 border = array::readValue<int16>(ptr);
                textures::Format format = array::readValue<textures::Format>(ptr);
                textures::DataType type = array::readValue<textures::DataType>(ptr);
                void* data = (void*)ptr;
                
                uint8 bpp = textures::bppByFormat(format);
                ptr += width * height * bpp;
                
                mDevice->updateTexture2D(id, level, internal, width, height, border, format, type, data);
                break;
            }
            case RenderCommandType::SetState:
            {
                RenderState& state = array::readValue<RenderState>(ptr);
                mDevice->setState(state);
                break;
            }
            case RenderCommandType::Empty:
            {
                eof = true;
                break;
            }
            default:
                msg_assert(false, "unknown render command");
        }
    }
    
//    RenderCommandQueue::doDebugPtr(nullptr);
}
//------------------------------------------------------------------------------
}//namespace graphics
