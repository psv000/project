//
//  RenderOptions.h
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#pragma once

#include <math/vec4.h>

namespace graphics
{

class IRenderSurface;
struct RenderOptions
{
    IRenderSurface* surface;
    vec4f clearColor;
};
    
}//namespace graphics
