//
//  Texture.h
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#pragma once

#include <math/vec3.h>
#include <graphics/RenderPrimitives.h>
#include "TextureTypes.h"

namespace textures
{
    
class Texture final
{
public:
    Texture(graphics::TextureID, Format, InternalFormat, Filtering, const vec3f&);
    ~Texture();
    
    Texture(const Texture&) = delete;
    Texture operator=(const Texture&) = delete;
    
    graphics::TextureID id() const { return mID; }
    vec3f size() const { return mSize; }
    Format format() const { return mFormat; }
    InternalFormat internalFormat() const { return mInternalFormat; }
    Filtering filtering() const { return mFiltering; }
    graphics::TextureLevel level() const { return mLevel; }
    const void* data() const { return mData; }
    
    void setContent(void* content) { mData = content; }
    
private:
    graphics::TextureID mID;
    
    vec3f mSize;
    
    Format mFormat;
    InternalFormat mInternalFormat;
    Filtering mFiltering;
    
    graphics::TextureLevel mLevel;
    
    void* mData;
};
    
}//namespace textures
