//
//  TextureTypes.h
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#pragma once

#include "TextureTypes.h"

namespace textures
{

inline uint8 bppByFormat(Format format)
{
    switch (format) {
        case Format::Alpha:
        case Format::Red:
            return 8;
        case Format::RGB:
            return 24;
        case Format::RGBA:
            return 32;
        case Format::Unknown:
            return 0;
    }
    return 0;
}

}//namespace textures
