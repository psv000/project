//
//  TextureTypes.h
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#pragma once

#include <common/inttypes.h>

namespace textures
{

enum class DataType : uint8
{
    UnsignedByte = 0,
    UnsignedShort,
    UnsignedInt,
    Unknown
};
    
enum class Format : uint8
{
    RGB = 0,
    RGBA,
    Alpha,
    Red,
    Unknown
};

enum class InternalFormat : uint8
{
    RGB = 0,
    RGBA,
    Alpha,
    Red8,
    Unknown
};

enum class Filtering : uint8
{
    Linear = 0,
    Unknown
};

}//namespace textures
