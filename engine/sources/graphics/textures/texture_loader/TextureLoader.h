//
//  TextureLoader.h
//  engine
//
//  Created by s.popov on 02/10/2017.
//
//

#pragma once

#include <graphics/RenderPrimitives.h>
#include <graphics/textures/TextureTypes.h>
#include <math/vec3.h>

namespace textures
{

class Texture;
namespace TextureLoader
{
    
void init();
void deinit();
    
Texture* loadFile(const char*);
bool saveFile(const char*, Texture*);
    
Texture* createTexture(graphics::TextureID,
                       Format,
                       InternalFormat,
                       Filtering,
                       vec3f);

void obtainTexture(Texture*);
void releaseTexture(Texture*);
    
}//namespace TextureLoader

}//namespace textures
