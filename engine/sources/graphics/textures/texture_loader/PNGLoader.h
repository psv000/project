//
//  PNGLoader.h
//  engine
//
//  Created by s.popov on 02/10/2017.
//
//

#pragma once

namespace FileSystem
{
class File;
}//namespace FileSystem
namespace textures
{
struct ImageFileInfo;
namespace PNGLoader
{

bool readFile(ImageFileInfo*, FileSystem::File*);
bool writeFile(FileSystem::File*, int, int, const void*);

}//namespace PNGLoader
}//namespace textures
