//
//  ImageFilePrerequisites.h
//  engine
//
//  Created by s.popov on 02/10/2017.
//
//

#pragma once

#include <graphics/textures/TextureTypes.h>
#include <common/inttypes.h>

namespace textures
{
    
struct ImageFileInfo
{
    uint32 width;
    uint32 height;
    void* data;
    Format format;
    InternalFormat internalFormat;
};
    
}//namespace textures
