//
//  TextureLoader.cpp
//  engine
//
//  Created by s.popov on 02/10/2017.
//
//

#include "TextureLoader.h"
#include "ImageFilePrerequisites.h"
#include "PNGLoader.h"

#include <graphics/RenderPrimitives.h>
#include <graphics/textures/Texture.h>

#include <core/file_system/FileSystem.h>

#include <core/memory/allocations.h>
#include <core/memory/MemoryManager.h>

#include <core/Activity.h>
#include <graphics/Render.h>
#include <graphics/IGraphicsDevice.h>

#include <utils/cache.h>
#include <utils/hash.h>

#include <common/formatstring.h>
#include <logs.h>
#include <asserts.h>

#define TEXTURES_COUNT_IN_POOL (4)
#define TEXTURES_CACHE_SIZE (32)

namespace textures
{
    
namespace TextureLoader
{

memory::PoolAllocator* gTexturePool = nullptr;
    
using TextureCacheInfo = utils::CacheInfo<Texture>;
using TextureCache = utils::CacheCollection<Texture>;
TextureCache gCache;
uint32 mNewTextureID {0};
    
}//namespace TextureLoader
//------------------------------------------------------------------------------
void TextureLoader::init()
{
    mNewTextureID = 0;
    gTexturePool = memory::MemoryManager::createPool<Texture>("texture_allocator",
                                                              TEXTURES_COUNT_IN_POOL);
    gCache.realloc(TEXTURES_CACHE_SIZE);
}
//------------------------------------------------------------------------------
void TextureLoader::deinit()
{
    mNewTextureID = 0;
    msg_assert(gCache.size() == 0, "textures cache isn\'t empty");
    
    memory::MemoryManager::destructPool(gTexturePool);
}
//------------------------------------------------------------------------------
Texture* TextureLoader::loadFile(const char* filename)
{
    auto hash = utils::hash::string(filename);
    TextureCacheInfo info {hash, nullptr, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        ++(gCache[i].ref_count);
        return gCache[i].item;
    }
    
    Texture* texture = nullptr;
    ImageFileInfo imageInfo;
    
    bool result = false;
    FileSystem::File file = FileSystem::loadFile(filename, FileSystem::LocationType::Resources);
    
    msg_assert(!file.empty(), format_string("Can\'t read file <%s>", file.name()).c_str());
    
    if (file.extension() == FileSystem::Extension::PNG)
    {
        result = PNGLoader::readFile(&imageInfo, &file);
    }

    msg_assert(!file.empty(), format_string("Can\'t read image file <%s>", file.name()).c_str());
    
    if (result)
    {
        auto device = Activity::render()->graphicsDevice();
        graphics::TextureID textureID = device->createTexture2D();
        
        msg_assert(textureID != graphics::INVALID_TEXTURE_ID, "invalid texture id");
        
        device->updateTexture2D(textureID,
                                graphics::TextureLevel::L0,
                                imageInfo.internalFormat,
                                imageInfo.width, imageInfo.height, 0,
                                imageInfo.format,
                                textures::DataType::UnsignedByte,
                                imageInfo.data);
        
        
        
        texture = memory::allocate<Texture>(*gTexturePool,
                                            textureID,
                                            imageInfo.format,
                                            imageInfo.internalFormat,
                                            textures::Filtering::Unknown,
                                            vec3f(imageInfo.width, imageInfo.height, 0));
        
        memory::dealloc(imageInfo.data);
        
        gCache.push_back({hash, texture, 1});
    }
    
    msg_assert(texture, "Couldn\'t create texture");
    
    return texture;
}
//------------------------------------------------------------------------------
bool TextureLoader::saveFile(const char* filename, Texture* texture)
{
    bool result = false;
    
    FileSystem::File file = FileSystem::saveFile(filename, FileSystem::LocationType::Root);
    
    if (file.extension() == FileSystem::Extension::PNG)
    {
        auto size = texture->size();
        result = PNGLoader::writeFile(&file, size.x, size.y, texture->data());
        if (result)
            LOGI("png file <%s> was successfully saved", filename);
    }
    return false;
}
//------------------------------------------------------------------------------
Texture* TextureLoader::createTexture(graphics::TextureID id,
                                      Format format,
                                      InternalFormat internalFormat,
                                      Filtering filtering,
                                      vec3f size)
{
    string name = format_string("%ud", ++mNewTextureID);
    auto hash = utils::hash::string(name.c_str());
    TextureCacheInfo info {hash, nullptr, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        msg_assert(false, "texture hash collision");
        return nullptr;
    }
    Texture* texture = memory::allocate<Texture>(*gTexturePool,
                                                 id,
                                                 format,
                                                 internalFormat,
                                                 filtering,
                                                 size);
    gCache.push_back({hash, texture, 1});
    return texture;
}
//------------------------------------------------------------------------------
void TextureLoader::obtainTexture(Texture* texture)
{
    TextureCacheInfo info {utils::hash::invalid_hash, texture, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        auto &info = gCache[i];
        ++(info.ref_count);
        return;
    }
    msg_assert(false, "there is no texture in a cache");
}
//------------------------------------------------------------------------------
void TextureLoader::releaseTexture(Texture* texture)
{
    TextureCacheInfo info {utils::hash::invalid_hash, texture, 0};
    int64 i = gCache.find(info);
    if (i >= 0)
    {
        auto &info = gCache[i];
        --(info.ref_count);
        if (info.ref_count != 0)
            return;
        gCache.erase(info);
    }
    
    auto device = Activity::render()->graphicsDevice();
    device->destroyTexture2D(texture->id());
    
    memory::deallocate(*gTexturePool, texture);
}
//------------------------------------------------------------------------------
}//namespace textures
