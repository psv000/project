//
//  PNGLoader.cpp
//  engine
//
//  Created by s.popov on 02/10/2017.
//
//

#include "PNGLoader.h"
#include "ImageFilePrerequisites.h"

#include <core/file_system/FileSystem.h>
#include <core/memory/memory.h>

#include <logs.h>
#include <asserts.h>

#include <png.h>

class InputStream
{
FileSystem::File* mFile;
public:

InputStream(FileSystem::File* file):
mFile(file)
{
}

size_t read(byte* data, const size_t length)
{
    return mFile->read((char*)data, length);
}

};

namespace textures
{
//------------------------------------------------------------------------------
void custom_read_data(png_structp png_ptr, png_bytep data, png_size_t length)
{
    png_voidp io_ptr = png_get_io_ptr(png_ptr);

    void_assert(io_ptr != NULL);
    InputStream& inputStream = *(InputStream*)io_ptr;
    const size_t bytesRead = inputStream.read(
            (byte*)data,
            (size_t)length);

    void_assert((png_size_t)bytesRead == length);
}
//------------------------------------------------------------------------------
bool PNGLoader::readFile(ImageFileInfo* info, FileSystem::File* fp)
{
    int width, height;
    png_byte color_type;
    png_byte bit_depth;
    
    png_structp png_ptr;
    png_infop info_ptr;
    int number_of_passes;
    png_bytep* row_pointers;
    
    char header[8];    // 8 is the maximum size that can be checked
    if (fp->empty())
    {
        LOGE("[read_png_file] File %s could not be opened for reading", fp->name());
        return false;
    }
    
    fp->read(header, 8);
    if (png_sig_cmp((png_const_bytep)header, 0, 8))
    {
        LOGE("[read_png_file] File %s is not recognized as a PNG file", fp->name());
        return false;
    }
    
    
    /* initialize stuff */
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    
    if (!png_ptr)
    {
        LOGE("[read_png_file] png_create_read_struct failed");
        return false;
    }
    
    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
        LOGE("[read_png_file] png_create_info_struct failed");
        return false;
    }
    
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        LOGE("[read_png_file] Error during init_io");
        return false;
    }

    InputStream istream(fp);
    png_voidp io_ptr = (png_voidp)&istream;
    png_set_read_fn(png_ptr, io_ptr, custom_read_data);

//    png_init_io(png_ptr, fp->handle());
    png_set_sig_bytes(png_ptr, 8);
    
    png_read_info(png_ptr, info_ptr);
    
    width = png_get_image_width(png_ptr, info_ptr);
    height = png_get_image_height(png_ptr, info_ptr);
    color_type = png_get_color_type(png_ptr, info_ptr);
    bit_depth = png_get_bit_depth(png_ptr, info_ptr);
    
    number_of_passes = png_set_interlace_handling(png_ptr);
    png_read_update_info(png_ptr, info_ptr);
    
    /* read file */
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        LOGE("[read_png_file] Error during read_image");
        return false;
    }
    //------------------------------------------------------------------------------
    
    int64 rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    
    // glTexImage2d requires rows to be 4-byte aligned
    rowbytes += 3 - ((rowbytes-1) % 4);
    
    png_bytep image_data;
    image_data = (png_bytep)memory::alloc(rowbytes * height * sizeof(png_byte)+15);
    if (image_data == NULL)
    {
        LOGE("error: could not allocate memory for PNG image data");
        png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
        return false;
    }
    
    // row_pointers is for pointing to image_data for reading the png with libpng
    row_pointers = (png_bytep*)memory::alloc(height * sizeof(png_bytep));
    if (row_pointers == NULL)
    {
        LOGE("error: could not allocate memory for PNG row pointers");
        png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
        memory::dealloc(image_data);
        return 0;
    }
    
    // set the individual row_pointers to point at the correct offsets of image_data
    int i;
    for (i = 0; i < height; i++)
    {
        row_pointers[height - 1 - i] = image_data + i * rowbytes;
    }
    
    //------------------------------------------------------------------------------
    
    png_read_image(png_ptr, row_pointers);
    
    {
        info->width = width;
        info->height = height;
        info->format = Format::RGBA;
        info->internalFormat = InternalFormat::RGBA;
        info->data = image_data;
    }
    
    png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
    memory::dealloc(row_pointers);
    return true;
}
//------------------------------------------------------------------------------
bool PNGLoader::writeFile(FileSystem::File* fp, int w, int h, const void* data)
{
    int width = w, height = h;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    png_bytep row = NULL;
    
    /* create file */
    if (!fp)
    {
        LOGE("[write_png_file] File could not be opened for writing");
        return false;
    }
    
    /* initialize stuff */
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        LOGE("Could not allocate write struct");
        return false;
    }
    
    // Initialize info structure
    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
        LOGE("Could not allocate info struct");
        return false;
    }
    
    // Setup Exception handling
    if (setjmp(png_jmpbuf(png_ptr))) {
        LOGE("Error during png creation");
        return false;
    }
    
//    png_init_io(png_ptr, fp->handle());
    
    // Write header (8 bit colour depth)
    png_set_IHDR(png_ptr, info_ptr, width, height,
                 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    
    png_write_info(png_ptr, info_ptr);
    
    // Allocate memory for one row (4 bytes per pixel - RGBA)
    row = (png_bytep) memory::alloc(4 * width * sizeof(png_byte));
    
    png_bytepp rows = (png_bytepp)png_malloc(png_ptr, height * sizeof(png_bytep));
    for (int i = 0; i < height; ++i)
        rows[i] = (png_bytep)((png_bytep)data + (height - i) * width * 4);
    
    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, NULL);
    
    if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
    if (row != NULL) memory::dealloc(row);
    
    return true;
}
//------------------------------------------------------------------------------
}//namespace textures
