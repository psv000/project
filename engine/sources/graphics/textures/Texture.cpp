//
//  Texture.cpp
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#include "Texture.h"

namespace textures
{
//------------------------------------------------------------------------------
Texture::Texture(graphics::TextureID id,
                 Format format,
                 InternalFormat internalFormat,
                 Filtering filtering,
                 const vec3f& size):
mID(id),
mSize(size),
mFormat(format),
mInternalFormat(internalFormat),
mFiltering(filtering),
mLevel(graphics::TextureLevel::L0),
mData(nullptr)
{
    
}
//------------------------------------------------------------------------------
Texture::~Texture()
{
    
}
//------------------------------------------------------------------------------
}//namespace textures
