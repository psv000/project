//
//  DrawHelper.h
//  engine
//
//  Created by s.popov on 24/10/2017.
//
//

#pragma once

#include <math/vec2.h>
#include "Colors.h"
#include "VerticesTypes.h"

#include <graphics/shaders/ProgramArgs.h>
#include <graphics/RenderState.h>
#include <graphics/RenderPrimitives.h>

#include <core/Activity.h>
#include <core/IPlatformContext.h>

#include <math/helpers.h>

#include <vector>

namespace graphics
{
    
using ColoredLinePoints = std::vector<v3fc4b>;

class Program;
class Mesh;

namespace DrawHelper
{

ColoredLinePoints coloredLine(vec2f*, uint32, Color4B);
    
struct StaticAttribute
{
    MeshVertexComponent component;
    void* data;
};
    
void queueMeshToRender(VertexArrayObjectID,
                       uint64,
                       Program*,
                       const ProgramArgs&);

void queueLineToRender(VertexArrayObjectID,
                       VertexBufferID,
                       uint32,
                       uint8,
                       DrawMode,
                       Program*,
                       const ProgramArgs&);
    
void createCircle(Mesh*, uint32*, uint32,
                  void*, uint32,
                  float);
//------------------------------------------------------------------------------
inline void alignByPixel(void* vertices, uint16 size, uint16 step, uint64 count)
{
    float designScaleFactor = Activity::designScaleFactor();
    char* v = (char*)vertices;
    for (uint64 i = 0; i < count; ++i)
    {
        vec3f& pos = *(vec3f*)(v + size * i + step );
        pos.x = math::trunc(pos.x * designScaleFactor);
        pos.y = math::trunc(pos.y * designScaleFactor);
        pos.z = math::trunc(pos.z * designScaleFactor);
    }
}
//------------------------------------------------------------------------------
inline void alignLineByPixel(void* vertices, uint16 size, uint16 step, uint64 count)
{
    float designScaleFactor = Activity::designScaleFactor();
    char* v = (char*)vertices;
    for (uint64 i = 0; i < count; ++i)
    {
        vec3f& pos = *(vec3f*)(v + size * i + step );
        pos.x = math::trunc(pos.x * designScaleFactor) + 0.5f;
        pos.y = math::trunc(pos.y * designScaleFactor) + 0.5f;
        pos.z = math::trunc(pos.z * designScaleFactor) + 0.5f;
    }
}
//------------------------------------------------------------------------------
inline void scaleMeshByDensity(void* vertices, uint16 size, uint16 step, uint64 count)
{
    float designScaleFactor = Activity::designScaleFactor();
    char* v = (char*)vertices;
    for (uint64 i = 0; i < count; ++i)
    {
        vec3f& pos = *(vec3f*)(v + size * i + step );
        pos *= designScaleFactor;
    }
}
//------------------------------------------------------------------------------
}//namespace DrawHelper
}//namespace graphics
