//
//  Sprite.h
//  engine
//
//  Created by s.popov on 04/10/2017.
//
//

#pragma once

#include "Node.h"
#include "Colorizable.h"
#include <graphics/RenderPrimitives.h>
#include <graphics/VerticesTypes.h>
#include <graphics/RenderState.h>
#include <graphics/shaders/ProgramArgs.h>

#include <math/rect.h>

namespace graphics
{
    
struct Material;
class Program;
class Sprite final
    :   public Node,
        public Colorizable
{
public:
    Sprite();
    ~Sprite();
    
    Sprite(const Sprite&) = delete;
    Sprite& operator=(const Sprite&) = delete;
    
    void setMaterial(const char*);
    void setMaterial(Material*);
    void update(const Transform&) override;
    
    void setStretchPoint(const vec2i&);
    void setStretchRect(const vec2i&, const vec2i&);
    
    void setSize(const vec2f&) override;
    
    void setDirty() { mIsDirty = true; }
    bool isDirty() const { return mIsDirty; }
    
    void updateContent();
    
private:
    void updateSpriteRect();
    
private:
    Program* mShader;
    ProgramArgs mProgramArgs;
    
    Material* mMaterial;
    
    VertexArrayObjectID mVAO;
    VertexBufferID mVBO;
    IndexBufferID mIBO;
    
    v3fu2us mVertices[16];
    uint8 mVerticesCount;
    
    uint32 mIndices[42];
    uint32 mIndicesCount;
    
    recti mStretchRect;

    bool mIsDirty;
};
    
}//namespace graphics
