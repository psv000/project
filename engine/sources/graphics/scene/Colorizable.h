//
//  Colorizable.h
//  chart
//
//  Created by s.popov on 20/02/2018.
//
//

#pragma once

#include <graphics/Colors.h>

namespace graphics
{
    
class Colorizable
{
public:
    Colorizable();
    ~Colorizable();
    
    Colorizable(const Colorizable&) = delete;
    Colorizable&operator=(const Colorizable&) = delete;
    
    void setBaseColor(const Color4F&);
    const Color4F& baseColor() { return mBaseColor; }
    
    void setAlpha(float);
    float alpha() const { return mAlpha; }
    
    const Color4F& resultColor() const { return mResultColor; }
    
    void setVisible(bool v) { mVisible = v; }
    bool isVisible() const { return mVisible; }
    
private:
    void recalculate();
    
protected:
    Color4F mResultColor;
    Color4F mBaseColor;
    float mAlpha;
    bool mVisible;
};
    
}//namespace graphics
