//
//  Node.cpp
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#include "Node.h"
#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include "SceneContainer.h"
#include <common/inttypes.h>
#include <asserts.h>
#include <logs.h>

#include <math/helpers.h>

#include <glm/gtc/matrix_transform.hpp>

namespace graphics
{
//------------------------------------------------------------------------------
Node::Node():
mTransform(1.f),
mWorldTransform(1.f),
mPosition(0.f),
mScaling(1.f),
mSize(1.f),
mRotationAngle(0.f),
mQuat(1.f, 0.f, 0.f, 0.f),
mAnchor(0.5f),
mPivot(0.5f),
mScene(nullptr),
mZOrder(0),
mTransformIndex(0)
{
}
//------------------------------------------------------------------------------
void Node::setPosition(const vec2f& position)
{
    mPosition = position;
}
//------------------------------------------------------------------------------
const vec2f& Node::getPosition() const
{
    return mPosition;
}
//------------------------------------------------------------------------------
void Node::setScaling(const vec2f& scaling)
{
    if ( scaling.x != 1.f || scaling.y != 1.f )
    {
        mTransformIndex |= 1;
    }
    else
    {
        mTransformIndex &= ~1;
    }

    mScaling = scaling;
}
//------------------------------------------------------------------------------
const vec2f& Node::getScaling()
{
    return mScaling;
}
//------------------------------------------------------------------------------
void Node::setSize(const vec2f& size)
{
    mSize = size;
}
//------------------------------------------------------------------------------
const vec2f& Node::getSize() const
{
    return mSize;
}
//------------------------------------------------------------------------------
void Node::setRotation(float angle)
{
    if ( angle != 0.f )
    {
        mTransformIndex |= 2;
    }
    else
    {
        mTransformIndex &= ~2;
    }
    
    mRotationAngle = angle;
    mQuat = glm::angleAxis(angle, glm::vec3(0.f, 0.f, 1.f));
}
//------------------------------------------------------------------------------
float Node::getRotation() const
{
    return mRotationAngle;
}
//------------------------------------------------------------------------------
void Node::setAnchor(const vec2f& anchor)
{
    mAnchor = anchor;
}
//------------------------------------------------------------------------------
const vec2f& Node::getAnchor() const
{
    return mAnchor;
}
//------------------------------------------------------------------------------
void Node::setPivot(const vec2f& pivot)
{
    mPivot = pivot;
}
//------------------------------------------------------------------------------
const vec2f& Node::getPivot() const
{
    return mPivot;
}
//------------------------------------------------------------------------------
inline void result_transform0(Transform& result,
                              const Transform& parent,
                              const Transform& self,
                              const Transform& scale,
                              const Transform& rotation)
{
    result = parent * self;
}
//------------------------------------------------------------------------------
inline void result_transform1(Transform& result,
                              const Transform& parent,
                              const Transform& self,
                              const Transform& scale,
                              const Transform& rotation)
{
    result = parent * self * rotation;
}
//------------------------------------------------------------------------------
inline void result_transform2(Transform& result,
                              const Transform& parent,
                              const Transform& self,
                              const Transform& scale,
                              const Transform& rotation)
{
    result = parent * self * rotation * scale;
}
//------------------------------------------------------------------------------
inline void result_transform3(Transform& result,
                              const Transform& parent,
                              const Transform& self,
                              const Transform& scale,
                              const Transform& rotation)
{
    result = parent * self * rotation * rotation * scale;
}
//------------------------------------------------------------------------------
using result_transform_func_t = void (*)(Transform& result,
                                         const Transform& parent,
                                         const Transform& self,
                                         const Transform& scale,
                                         const Transform& rotation);
result_transform_func_t funcs [] =
{
    result_transform0,
    result_transform1,
    result_transform2,
    result_transform3
};

//------------------------------------------------------------------------------
void Node::update(const Transform& transform)
{
    const float designScaleFactor = Activity::designScaleFactor();

    vec2f psize(0.f);
    if( mScene )
    {
        psize = mScene->size();
    }
    else
    {
        static auto defaultSize = vec2f(0.f);
        psize = defaultSize;
    }
    
    auto position = mPosition;
    if (mPivot.x == 1.f) position.x *= -1.f;
    if (mPivot.y == 1.f) position.y *= -1.f;
    
    mTransform[3][0] = math::round(designScaleFactor * ( position.x - mSize.x * mScaling.x * mAnchor.x + psize.x * mPivot.x ) );
    mTransform[3][1] = math::round(designScaleFactor * ( position.y - mSize.y * mScaling.y * mAnchor.y + psize.y * mPivot.y ) );
    
    const Transform rotation = glm::mat4_cast(mQuat);
    
    static Transform scaling;
    scaling[0][0] = mScaling.x;
    scaling[1][1] = mScaling.y;
    
//    mWorldTransform = transform * mTransform * rotation * scaling;
    funcs[mTransformIndex](mWorldTransform,
                           transform,
                           mTransform,
                           rotation,
                           scaling);
}
//------------------------------------------------------------------------------
}//namespace graphics
