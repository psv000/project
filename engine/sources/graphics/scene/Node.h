//
//  Node.h
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#pragma once

#include "Transform.h"
#include <math/vec2.h>
#include <math/vec3.h>
#include <math/quat.h>
#include <vector>

namespace graphics
{

class SceneContainer;
class Node
{
public:
    Node();
    virtual ~Node() {}
    
    virtual void setPosition(const vec2f&);
    const vec2f& getPosition() const;
    
    void setScaling(const vec2f&);
    const vec2f& getScaling();
    
    virtual void setSize(const vec2f&);
    const vec2f& getSize() const;
    
    void setRotation(float);
    float getRotation() const;
    
    void setAnchor(const vec2f&);
    const vec2f& getAnchor() const;
    
    void setPivot(const vec2f&);
    const vec2f& getPivot() const;
    
    virtual void update(const Transform&);
    
    const Transform& getTransform() const { return mTransform; }
    const Transform& getWorldTransform() const { return mWorldTransform; }
    
    void setScene(SceneContainer* scene) { mScene = scene; }
    const SceneContainer* scene() const { return mScene; }
    
    void setZOrder(int order) { mZOrder = order; }
    int zOrder() const { return mZOrder; }
    
public:
    static const int MIN_NODE_ORDER { INT_MIN };
    static const int MAX_NODE_ORDER { INT_MAX };

protected:
    Transform mTransform;
    Transform mWorldTransform;
    
    vec2f mPosition;
    vec2f mScaling;
    vec2f mSize;
    
    float mRotationAngle;
    quat mQuat;
    
    vec2f mAnchor;
    vec2f mPivot;
    
    SceneContainer* mScene;
    
    int mZOrder;
    int mTransformIndex;
};
    
}//namespace graphics
