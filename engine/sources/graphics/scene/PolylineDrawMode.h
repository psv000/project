//
//  PolylineDrawMode.h
//  engine
//
//  Created by s.popov on 30/03/2018.
//
//

#pragma once

#include <common/inttypes.h>

namespace graphics
{

enum class PolylineDrawMode : uint8
{
    Single,
    Strip,
    Loop
};

}//namespace graphics
