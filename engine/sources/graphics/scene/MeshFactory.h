//
//  MeshFactory.h
//  engine
//
//  Created by s.popov on 27/09/2017.
//
//

#pragma once

#include "Mesh.h"
#include <graphics/VerticesTypes.h>

namespace graphics
{
namespace MeshFactory
{

void init();
void deinit();
Mesh* createMesh(const string& = "");
Mesh* createMesh(const string&, const string&, const string& = "");
Mesh* createMesh(VerticesType, void*, uint32, uint32*, uint32, const string& = "");
void destroyMesh(Mesh*);
    
}//MeshFactory
}//namespace graphics
