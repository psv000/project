//
//  SceneContainerFactory.h
//  engine
//
//  Created by s.popov on 28/09/2017.
//
//

#pragma once

#include "SceneContainer.h"

namespace graphics
{
namespace SceneContainerFactory
{
    
void init();
void deinit();
SceneContainer* createSceneContainer(Camera*, const vec3f&);
void destroySceneContainer(SceneContainer*);
    
}//namespace SceneContainerFactory
}//namespace graphics
