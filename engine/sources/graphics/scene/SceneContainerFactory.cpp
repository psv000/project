//
//  SceneContainerFactory.cpp
//  engine
//
//  Created by s.popov on 28/09/2017.
//
//

#include "SceneContainerFactory.h"
#include <core/memory/allocations.h>
#include <core/memory/MemoryManager.h>

#define SCENE_CONTAINERS_IN_POOL (128)

namespace graphics
{

namespace SceneContainerFactory
{

memory::PoolAllocator* gSceneContainerPool = nullptr;

}//namespace SceneContainerFactory
//------------------------------------------------------------------------------
void SceneContainerFactory::init()
{
    gSceneContainerPool = memory::MemoryManager::createPool<SceneContainer>("scene_container_allocator",
                                                                            SCENE_CONTAINERS_IN_POOL);
}
//------------------------------------------------------------------------------
void SceneContainerFactory::deinit()
{
    memory::MemoryManager::destructPool(gSceneContainerPool);
}
//------------------------------------------------------------------------------
SceneContainer* SceneContainerFactory::createSceneContainer(Camera* camera, const vec3f& size)
{
    auto scene = memory::allocate<SceneContainer>(*gSceneContainerPool, camera, size);
    return scene;
}
//------------------------------------------------------------------------------
void SceneContainerFactory::destroySceneContainer(SceneContainer* scene)
{
    memory::deallocate(*gSceneContainerPool, scene);
}
//------------------------------------------------------------------------------
}//namespace graphics
