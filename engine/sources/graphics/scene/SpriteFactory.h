//
//  SpriteFactory.h
//  engine
//
//  Created by s.popov on 04/10/2017.
//
//

#pragma once

#include "Sprite.h"

namespace graphics
{
namespace SpriteFactory
{

void init();
void deinit();
    
Sprite* createSprite(const char*);
Sprite* createSprite(Material*);
void destroySprite(Sprite*);

}//SpriteFactory
}//namespace graphics
