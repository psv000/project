//
//  Polyline.h
//  engine
//
//  Created by s.popov on 04/12/2017.
//
//

#pragma once

#include "Node.h"
#include "Colorizable.h"
#include <graphics/RenderPrimitives.h>
#include <graphics/VerticesTypes.h>
#include <graphics/RenderState.h>
#include <graphics/shaders/ProgramArgs.h>
#include "PolylineDrawMode.h"

namespace graphics
{

class Program;
class Polyline final
    :   public Node,
        public Colorizable
{
public:
    Polyline();
    ~Polyline();

    Polyline(const Polyline&) = delete;
    Polyline& operator=(const Polyline&) = delete;

    void setProgram(const char*, const char*);

    void setPoints(VerticesType, const void*, uint32, float);
    void update(const Transform&) override;

    void setDrawMode(PolylineDrawMode mode) { mDrawMode = mode; }
    void setThickness(float val) { mThickness = val; }

private:
    Program* mShader;
    ProgramArgs mProgramArgs;

    VertexArrayObjectID mVAO;
    VertexBufferID mVBO;
    IndexBufferID mIBO;

    uint64 mIndicesCount;

    PolylineDrawMode mDrawMode;

    float mThickness;
};
}//namespace graphics
