//
//  Colorizable.cpp
//  engine
//
//  Created by s.popov on 20/02/2018.
//
//

#include "Colorizable.h"

namespace graphics
{
//------------------------------------------------------------------------------
Colorizable::Colorizable():
mBaseColor(1.f),
mResultColor(1.f),
mAlpha(1.f),
mVisible(true)
{
    
}
//------------------------------------------------------------------------------
Colorizable::~Colorizable()
{
    
}
//------------------------------------------------------------------------------
void Colorizable::setBaseColor(const Color4F& color)
{
    mBaseColor = color;
    Colorizable::recalculate();
}
//------------------------------------------------------------------------------
void Colorizable::setAlpha(float alpha)
{
    mAlpha = alpha;
    Colorizable::recalculate();
}
//------------------------------------------------------------------------------
void Colorizable::recalculate()
{
    mResultColor = mBaseColor;
    mResultColor.a *= mAlpha;
}
//------------------------------------------------------------------------------
}//namespace graphics
