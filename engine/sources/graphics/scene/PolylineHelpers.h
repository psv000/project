//
//  PolylineHelpers.h
//  engine
//
//  Created by s.popov on 30/03/2018.
//
//

#pragma once

#include "PolylineDrawMode.h"
#include <core/memory/memory.h>
#include <math/vec3.h>
#include <math/vec-helpers.h>
#include <logs.h>

namespace graphics
{
namespace polylineHelpers
{

inline void polygonize(const void*, uint32, uint16, uint16,
                       void*&, uint32&, uint16, uint16,
                       uint32*& , uint64&,
                       PolylineDrawMode,
                       float);

inline void polygonizeSingleLine(const char*, uint32, uint16, uint16,
                                 char*, uint32, uint16, uint16,
                                 uint32* , uint64,
                                 float);

inline void polygonizeStripLine(const char*, uint32, uint16, uint16,
                                char*, uint32, uint16, uint16,
                                uint32* , uint64,
                                float);

inline void polygonizeLoopLine(const char*, uint32, uint16, uint16,
                               char*, uint32, uint16, uint16,
                               uint32* , uint64,
                               float);

inline void polygonizeSegment(const vec3f*,
                              const vec3f&,
                              vec3f**, uint32*,
                              uint32, float);
    
inline void smoothStripLine();
inline void smoothLoopLine();
    
inline void colorize(const void*, uint32, uint16, uint16,
                     void*, uint32, uint16, uint16,
                     PolylineDrawMode);

inline void colorizeSingle(const void*, uint32, uint16, uint16,
                           void*, uint32, uint16, uint16);
inline void colorizeStrip(const void*, uint32, uint16, uint16,
                          void*, uint32, uint16, uint16);
    
inline void getNormal(vec3f&, const vec3f*);
    
inline bool intersection(vec3f* line, vec3f& out);
inline bool isclockwise(vec3f* points);
    
}//polylineHelpers
//------------------------------------------------------------------------------
inline char* readValue(char* in, uint16 size, uint16 step, uint64 i)
{
    return &in[size * i + step];
}
//------------------------------------------------------------------------------
inline const char* readValue(const char* in, uint16 size, uint16 step, uint64 i)
{
    return &in[size * i + step];
}
//------------------------------------------------------------------------------
bool polylineHelpers::isclockwise(vec3f* p)
{
    return ( p[1].x - p[0].x ) * ( p[2].y - p[0].y ) - ( p[2].x - p[0].x ) * ( p[1].y - p[0].y ) > 0;
}
//------------------------------------------------------------------------------
inline float Det(float a, float b, float c, float d)
{
    return a * d - b * c;
}
//------------------------------------------------------------------------------
bool polylineHelpers::intersection(vec3f* line, vec3f& out)
{
    float x1 = line[0].x;
    float x2 = line[1].x;
    float x3 = line[2].x;
    float x4 = line[3].x;
    
    float y1 = line[0].y;
    float y2 = line[1].y;
    float y3 = line[2].y;
    float y4 = line[3].y;
    
    if ( fabsf(x2 - x3) < 0.1f && fabsf(y2 - y3) < 0.1f )
    {
        out.x = x2;
        out.y = y2;
        return true;
    }
    
    float detL1 = Det(x1, y1, x2, y2);
    float detL2 = Det(x3, y3, x4, y4);
    float x1mx2 = x1 - x2;
    float x3mx4 = x3 - x4;
    float y1my2 = y1 - y2;
    float y3my4 = y3 - y4;
    
    float xnom = Det(detL1, x1mx2, detL2, x3mx4);
    float ynom = Det(detL1, y1my2, detL2, y3my4);
    float denom = Det(x1mx2, y1my2, x3mx4, y3my4);
    if(denom == 0.0)//Lines don't seem to cross
    {
        out.x = NAN;
        out.y = NAN;
        return false;
    }
    
    out.x = xnom / denom;
    out.y = ynom / denom;
    if( !isfinite(out.x) || !isfinite(out.y) ) //Probably a numerical issue
    {
        return false;
    }
    
    return true;
}
//------------------------------------------------------------------------------
void polylineHelpers::polygonizeSegment(const vec3f* vertices,
                                        const vec3f& normal,
                                        vec3f** out, uint32* indices,
                                        uint32 verticesOffset, float thickness)
{
    *(out[0]) = vertices[0] - normal * thickness;
    *(out[1]) = vertices[0] + normal * thickness;
    *(out[2]) = vertices[1] + normal * thickness;
    *(out[3]) = vertices[1] - normal * thickness;
    
    indices[0] = verticesOffset + 0; indices[3] = verticesOffset + 0;
    indices[1] = verticesOffset + 1; indices[4] = verticesOffset + 2;
    indices[2] = verticesOffset + 2; indices[5] = verticesOffset + 3;
}
//------------------------------------------------------------------------------
inline void polylineHelpers::getNormal(vec3f& normal, const vec3f* points)
{
    float ybuffer = 0.f;
    vec3f line = points[1] - points[0];
    
    normal = math::normalize(line);
    ybuffer = normal.y;
    normal.y = normal.x;
    normal.x = -ybuffer;
    normal.z = 0.f;
}
//------------------------------------------------------------------------------
void polylineHelpers::polygonizeSingleLine(const char* in, uint32 inCount, uint16 inSize, uint16 inStep,
                                           char* out, uint32 outCount, uint16 outSize, uint16 outStep,
                                           uint32* indices, uint64 indicesCount,
                                           float thickness)
{
    vec3f normal(0.f);
    
    vec3f line[2];
    vec3f* outpoints[4];
    
    uint32 verticesPerSegment {4};
    uint64 indicesPerSegment {6};
    
    uint32 verticesOffset = 0;
    uint32 indicesOffset = 0;
    for (uint64 i = 0; i < inCount * 0.5f; ++i)
    {
        line[0] = *(vec3f*)readValue(in, inSize, inStep, 2 * i + 0);
        line[1] = *(vec3f*)readValue(in, inSize, inStep, 2 * i + 1);
        
        polylineHelpers::getNormal(normal, line);

        outpoints[0] = (vec3f*)readValue(out, outSize, outStep, verticesOffset + 0);
        outpoints[1] = (vec3f*)readValue(out, outSize, outStep, verticesOffset + 1);
        outpoints[2] = (vec3f*)readValue(out, outSize, outStep, verticesOffset + 2);
        outpoints[3] = (vec3f*)readValue(out, outSize, outStep, verticesOffset + 3);
        
        polylineHelpers::polygonizeSegment(line, normal,
                                           outpoints, &indices[indicesOffset],
                                           verticesOffset, thickness);

        verticesOffset += verticesPerSegment;
        indicesOffset += indicesPerSegment;
    }
}
//------------------------------------------------------------------------------
void polylineHelpers::polygonizeLoopLine(const char* in, uint32 inCount, uint16 inSize, uint16 inStep,
                                         char* out, uint32 outCount, uint16 outSize, uint16 outStep,
                                         uint32* indices, uint64 indicesCount,
                                         float thickness)
{
}
//------------------------------------------------------------------------------
void polylineHelpers::polygonize(const void* in, uint32 inCount, uint16 inSize, uint16 inStep,
                                 void*& out, uint32& outCount, uint16 outSize, uint16 outStep,
                                 uint32*& indices, uint64& indicesCount,
                                 PolylineDrawMode drawMode,
                                 float thickness)
{
    switch(drawMode)
    {
        case PolylineDrawMode::Single:
            
            outCount = inCount * 2;
            out = memory::alloc( outCount * outSize );
            
            indicesCount = outCount / 4 * 6;
            indices = (uint32*)memory::alloc( indicesCount * sizeof(uint32) );
            
            polylineHelpers::polygonizeSingleLine((const char*)in, inCount, inSize, inStep,
                                                  (char*&)out, outCount, outSize, outStep,
                                                  indices, indicesCount, thickness);
            break;
        case PolylineDrawMode::Strip:
            
            outCount = (uint32)GET_MAX( (int64)0, 4 + ( (int64)inCount - 2 ) * 3 );
            //at least one segment
            if (outCount > 1)
            {
                out = memory::alloc( outCount * outSize );
                
                indicesCount = ( inCount - 1 ) * 6 + ( inCount - 2 ) * 3;
                indices = (uint32*)memory::alloc( indicesCount * sizeof(uint32) );
                
                polylineHelpers::polygonizeStripLine((const char*)in, inCount, inSize, inStep,
                                                     (char*&)out, outCount, outSize, outStep,
                                                     indices, indicesCount, thickness);
            }
            else
            {
                outCount = 0;
            }
            break;
        case PolylineDrawMode::Loop:
            
            outCount = 4 + ( inCount - 2 ) * 3;
            out = memory::alloc( outCount * outSize );
            
            indicesCount = ( inCount - 1 ) * 6 + ( inCount - 2 ) * 3;
            indices = (uint32*)memory::alloc( indicesCount * sizeof(uint32) );
            
            polylineHelpers::polygonizeLoopLine((const char*)in, inCount, inSize, inStep,
                                                (char*&)out, outCount, outSize, outStep,
                                                indices, indicesCount, thickness);
            break;
    }
}
//------------------------------------------------------------------------------
void polylineHelpers::colorize(const void* in, uint32 inCount, uint16 inSize, uint16 inStep,
                               void* out, uint32 outCount, uint16 outSize, uint16 outStep,
                               PolylineDrawMode drawMode)
{
    switch(drawMode)
    {
        case PolylineDrawMode::Single:
            polylineHelpers::colorizeSingle(in, inCount, inSize, inStep,
                                            out, outCount, outSize, outStep);

            break;
        case PolylineDrawMode::Strip:
            polylineHelpers::colorizeStrip(in, inCount, inSize, inStep,
                                           out, outCount, outSize, outStep);
            break;
        case PolylineDrawMode::Loop:
            void_assert(false || !"under construction");
            break;
    }
}
//------------------------------------------------------------------------------
void polylineHelpers::colorizeSingle(const void* in, uint32 inCount, uint16 inSize, uint16 inStep,
                                     void* out, uint32 outCount, uint16 outSize, uint16 outStep)
{
    (void) outCount;
    char* inData = (char*)in;
    char* outData = (char*)out;
    const int newVerticesPerOld { 2 };
    for (uint32 i = 0; i < inCount; ++i)
    {
        const vec4b& incolor = *(vec4b*)(inData + inSize * i + inStep);
        
        *(vec4b*)(outData + outSize * ( newVerticesPerOld * i + 0 ) + outStep) = incolor;
        *(vec4b*)(outData + outSize * ( newVerticesPerOld * i + 1 ) + outStep) = incolor;
    }
}
//------------------------------------------------------------------------------
void polylineHelpers::colorizeStrip(const void* in, uint32 inCount, uint16 inSize, uint16 inStep,
                                    void* out, uint32 outCount, uint16 outSize, uint16 outStep)
{
    (void) outCount;
    char* inData = (char*)in;
    char* outData = (char*)out;
    const int newVerticesPerOld { 3 };
    {
        const vec4b& incolor = *(vec4b*)(inData + inStep);
        *(vec4b*)(outData + outStep) = incolor;
        *(vec4b*)(outData + outSize + outStep) = incolor;
    }
    for (uint32 i = 0; i < inCount - 2; ++i)
    {
        const vec4b& incolor = *(vec4b*)(inData + inSize * i + inStep);
        
        *(vec4b*)(outData + outSize * ( newVerticesPerOld * i + 2 ) + outStep) = incolor;
        *(vec4b*)(outData + outSize * ( newVerticesPerOld * i + 3 ) + outStep) = incolor;
        *(vec4b*)(outData + outSize * ( newVerticesPerOld * i + 4 ) + outStep) = incolor;
    }
    {
        const vec4b& incolor = *(vec4b*)(inData + inSize * ( inCount - 1 ) + inStep);
        *(vec4b*)(outData + outSize * ( outCount - 2 ) + outStep) = incolor;
        *(vec4b*)(outData + outSize * ( outCount - 1 ) + outStep) = incolor;
    }
}
//------------------------------------------------------------------------------
void polylineHelpers::polygonizeStripLine(const char* in, uint32 inCount, uint16 inSize, uint16 inStep,
                                          char* out, uint32 outCount, uint16 outSize, uint16 outStep,
                                          uint32* indices, uint64 indicesCount,
                                          float thickness)
{
    const vec3f* inpoints[3];
    vec3f* outpoints[3];
    vec3f normal;
    vec3f lastNormal;
    
    vec3f line[3];
    vec3f intersectsLines[4];
    vec3f result[3];
    
    bool isConterClockWise;
    float sign;
    int indexForUpper;
    int indexForLower;
    
    uint32 indicesOffset = 0;
    uint64 verticesOffset = 0;
    int verticesPerSegment {2};
    int verticesPerBend {1};
    uint32 indicesPerSegment {6};
    uint32 indicesPerBend {3};
    
    {
        outpoints[0] = (vec3f*)readValue(out, outSize, outStep, 0);
        outpoints[1] = (vec3f*)readValue(out, outSize, outStep, 1);
        
        inpoints[0] = (const vec3f*)readValue(in, inSize, inStep, 0);
        inpoints[1] = (const vec3f*)readValue(in, inSize, inStep, 1);
        inpoints[2] = (const vec3f*)readValue(in, inSize, inStep, 2);
        
        line[0] = *inpoints[0];
        line[1] = *inpoints[1];
        line[2] = *inpoints[2];
        
        polylineHelpers::getNormal(normal, line);
        
        result[0] = line[0] + normal * thickness;
        result[1] = line[0] - normal * thickness;
        
        *outpoints[0] = result[0];
        *outpoints[1] = result[1];
        
        {
            indices[indicesOffset + 0] = 0;
            indices[indicesOffset + 1] = 2;
            indices[indicesOffset + 2] = 3;
            
            indices[indicesOffset + 3] = 0;
            indices[indicesOffset + 4] = 1;
            indices[indicesOffset + 5] = 2;
            
            indicesOffset += indicesPerSegment;
            verticesOffset += verticesPerSegment;
        }
    }
    
    for (uint64 i = 0; i < inCount - 2; ++i)
    {
        inpoints[0] = (const vec3f*)readValue(in, inSize, inStep, i + 0);
        inpoints[1] = (const vec3f*)readValue(in, inSize, inStep, i + 1);
        inpoints[2] = (const vec3f*)readValue(in, inSize, inStep, i + 2);
        
        line[0] =  *inpoints[0];
        line[1] =  *inpoints[1];
        line[2] =  *inpoints[2];
        
        isConterClockWise = polylineHelpers::isclockwise(line);
        sign = (float)( (int)isConterClockWise * 2 - 1 );
        intersectsLines[0] = line[0] + sign * normal * thickness;
        intersectsLines[1] = line[1] + sign * normal * thickness;
        
        lastNormal = normal;
        polylineHelpers::getNormal(normal, &line[1]);
        
        intersectsLines[2] = line[1] + sign * normal * thickness;
        intersectsLines[3] = line[2] + sign * normal * thickness;
        
        result[0] = line[1] - sign * lastNormal * thickness;
        polylineHelpers::intersection(intersectsLines, result[1]);
        result[2] = line[1] - sign * normal * thickness;
        
        indexForUpper = (int)isConterClockWise;
        indexForLower = 1 - (int)isConterClockWise;
        
        outpoints[0] = (vec3f*)readValue(out, outSize, outStep, lengthof(outpoints) * i + 2);
        outpoints[1] = (vec3f*)readValue(out, outSize, outStep, lengthof(outpoints) * i + 3);
        outpoints[2] = (vec3f*)readValue(out, outSize, outStep, lengthof(outpoints) * i + 4);
        
        *outpoints[0] = result[indexForLower];
        *outpoints[1] = result[indexForUpper];
        *outpoints[2] = result[2];
        
        {
            uint32 x, y;
            
            if ( sign > 0.f )
            {
                x = (uint32)(2 + verticesOffset);
                y = (uint32)(1 + verticesOffset);
            }
            else
            {
                x = (uint32)(0 + verticesOffset);
                y = (uint32)(2 + verticesOffset);
            }
            
            indices[indicesOffset + 0] = (uint32)(0 + verticesOffset);
            indices[indicesOffset + 1] = (uint32)(2 + verticesOffset);
            indices[indicesOffset + 2] = (uint32)(1 + verticesOffset);
            
            indices[indicesOffset + 3] = x;
            indices[indicesOffset + 4] = (uint32)(3 + verticesOffset);
            indices[indicesOffset + 5] = (uint32)(4 + verticesOffset);
            
            indices[indicesOffset + 6] = x;
            indices[indicesOffset + 7] = (uint32)(4 + verticesOffset);
            indices[indicesOffset + 8] = y;
            
            indicesOffset += indicesPerSegment + indicesPerBend;
            verticesOffset += verticesPerSegment + verticesPerBend;
        }
    }
    {
        outpoints[0] = (vec3f*)readValue(out, outSize, outStep, outCount - 2);
        outpoints[1] = (vec3f*)readValue(out, outSize, outStep, outCount - 1);
        
        inpoints[0] = (const vec3f*)readValue(in, inSize, inStep, inCount - 2);
        inpoints[1] = (const vec3f*)readValue(in, inSize, inStep, inCount - 1);
        
        line[0] = *inpoints[0];
        line[1] = *inpoints[1];
        
        polylineHelpers::getNormal(normal, line);
        
        result[0] = line[1] - normal * thickness;
        result[1] = line[1] + normal * thickness;
        
        *outpoints[0] = result[0];
        *outpoints[1] = result[1];
    }
}
//------------------------------------------------------------------------------
}//namespace graphics
