//
//  Line.h
//  engine
//
//  Created by s.popov on 23/10/2017.
//
//

#pragma once

#include "Node.h"
#include "Colorizable.h"
#include <graphics/RenderPrimitives.h>
#include <graphics/VerticesTypes.h>
#include <graphics/RenderState.h>
#include <graphics/shaders/ProgramArgs.h>

namespace graphics
{
    
class Program;
class Line final
    :   public Node,
        public Colorizable
{
public:
    Line();
    ~Line();
    
    Line(const Line&) = delete;
    Line& operator=(const Line&) = delete;

    void setPoints(VerticesType, const void*, uint32, float);
    void setPoints(v3f*, uint32, float, Color4F);
    
    void update(const Transform&) override;
    void setDrawMode(DrawMode mode) { mDrawMode = mode; }
    void setDash(float, float);
    
private:
    void configureSolidPlainLine(v3f*, uint32, float, Color4F);
    void configureSolidLine(v3fc4b*, uint32, float);
    void configureDashLine(v3fc4bu2us*, uint32, float);
    
private:
    Program* mShader;
    ProgramArgs mProgramArgs;
    
    VertexArrayObjectID mVAO;
    VertexBufferID mVBO;
    
    uint32 mVerticesCount;
    
    uint8 mThickness;
    float mDashLength;
    float mPeriod;
    
    DrawMode mDrawMode;
    
    enum class Style : uint8
    {
        Solid = 0,
        Dash
    } mStyle;
    
    string mName;
};
    
}//namespace graphics
