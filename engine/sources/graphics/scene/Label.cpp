//
//  Label.cpp
//  engine
//
//  Created by s.popov on 06/10/2017.
//
//

#include "Label.h"
#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include <graphics/Render.h>
#include <graphics/RenderCommandQueue.h>
#include <graphics/IGraphicsDevice.h>
#include <graphics/shaders/Program.h>
#include <graphics/textures/Texture.h>
#include <graphics/fonts/TextRender.h>
#include <graphics/DrawHelper.h>

#define DEFAULT_FONT ("fonts/default.ttf")
#define DEAFULT_SIZE (24.f)
#define DEAFULT_OUTLINE_SIZE (0.f)
#define DEAFULT_GAMMA (2.f)

namespace graphics
{
//------------------------------------------------------------------------------
Label::Label():
Node(),
mShader(nullptr),
mTexture(nullptr),
mIndicesCount(0),
mDirty(true)
{
    auto device = Activity::render()->graphicsDevice();
    
    mVBO = device->createVertexBuffer();
    mIBO = device->createIndexBuffer();
    mVAO = device->createVertexArrayObject();
    
    mShader = device->createProgram("shaders/text.vert", "shaders/text.frag");
    {
        UEArray ueArray;
        
        UniformElement elements[] = {
            { "umvp",       UniformType::Mat4 },
            { "utexture0",  UniformType::Texture2D },
            { "ucolor",     UniformType::Vec4 }
        };
        
        ueArray.elements = elements;
        ueArray.size = lengthof(elements);
        
        UniformContainer containers[ueArray.size];
        
        device->applyShader(mShader->id());
        device->createUniformContainer(mShader->id(), ueArray, containers);
        
        for (uint8 i = 0; i < ueArray.size; ++i)
        {
            mProgramArgs.registerArgument(ueArray.elements[i].name.c_str(), containers[i].location);
        }
    }
    
    mFont.name = DEFAULT_FONT;
    
    mFont.color = Color::White;
    mFont.backgroundColor = Color::None;
    mFont.outlineColor = Color::None;
    
    mFont.size = DEAFULT_SIZE;
    mFont.outlineSize = DEAFULT_OUTLINE_SIZE;
    mFont.gamma = DEAFULT_GAMMA;
}
//------------------------------------------------------------------------------
Label::~Label()
{
    auto device = Activity::render()->graphicsDevice();
    
    device->destroyShader(mShader);
    
    device->destroyVertexBuffer(mVBO);
    device->destroyIndexBuffer(mIBO);
    device->destroyVertexArrayObject(mVAO);
}
//------------------------------------------------------------------------------
void Label::setText(const string& text)
{
    mText = text;
    mDirty = true;
}
//------------------------------------------------------------------------------
void Label::setFontName(const string& fontName)
{
    mFont.name = fontName;
    mDirty = true;
}
//------------------------------------------------------------------------------
void Label::setFont(const fonts::FontProperties& font)
{
    mFont = font;
    mDirty = true;
}
//------------------------------------------------------------------------------
void Label::setFontSize(float size)
{
    mFont.size = size;
    mDirty = true;
}
//------------------------------------------------------------------------------
void Label::setVisible(bool val)
{
    mVisible = val;
}
//------------------------------------------------------------------------------
void Label::updateContent()
{
    if (mText == "")
    {
        return;
    }
    
    mDirty = false;
    
    graphics::v3fc4bu2us* vertices = nullptr;
    uint32* indices = nullptr;
    uint64 verticesCount = 0;
    uint32 indicesCount = 0;
    vec2f size(0.f);
    
    fonts::TextRender::renderText(mText.c_str(), mFont, mTexture,
                                  vertices,
                                  indices,
                                  verticesCount,
                                  indicesCount,
                                  size);
    
    mIndicesCount = indicesCount;
    mSize = vec3f(size.x, size.y, 0.f);

    auto device = Activity::render()->graphicsDevice();
    
    uint16 vertSize = 0;
    MVCArray mvcArray;
    {
        vertSize = sizeof(v3fc4bu2us);
        
        static MeshVertexComponent components[] = {
            { VertexAttribute::Position,    3,  VertexDataType::Float },
            { VertexAttribute::Color,       4,  VertexDataType::Byte },
            { VertexAttribute::Texture,     2,  VertexDataType::UnsignedShort }
        };
        
        mvcArray.components = components;
        mvcArray.size = lengthof(components);
    }
    
    device->updateIndexBuffer(mIBO, BufferUsage::StaticDraw, (void*)indices, indicesCount * sizeof(uint32));
    device->updateVertexBuffer(mVBO, vertices, verticesCount * vertSize);
    
    memory::dealloc(indices);
    memory::dealloc(vertices);
    
    device->applyShader(mShader->id());
    device->updateVertexArrayObject(mvcArray, mVAO, mVBO, mIBO);
}
//------------------------------------------------------------------------------
void Label::update(const Transform& transform)
{
    if ( !Colorizable::isVisible() ) return;
    
    if ( mDirty )
    {
        Label::updateContent();
    }
    
    if ( !mIndicesCount ) return;
    
    Node::update(transform);
    {
        float scale = Activity::designScaleFactor();
        mTransform[3][1] += mSize.y * scale;
        mWorldTransform = transform * mTransform;
    }
    
    msg_assert(mTexture != nullptr || mText == "", "label hasn\'t texture");
    if (mTexture == nullptr)
    {
        return;
    }

    {
        mProgramArgs.applyArgument("umvp", Node::mWorldTransform);
        mProgramArgs.applyArgument("utexture0", (TexturePack){ mTexture->level(), mTexture->id()});
        mProgramArgs.applyArgument("ucolor", Colorizable::mResultColor);
    }
    
    DrawHelper::queueMeshToRender(mVAO, mIndicesCount, mShader, mProgramArgs);
}
//------------------------------------------------------------------------------
}//namespace graphics
