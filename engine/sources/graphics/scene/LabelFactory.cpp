//
//  LabelFactory.cpp
//  engine
//
//  Created by s.popov on 06/10/2017.
//
//

#include "LabelFactory.h"

#include "SpriteFactory.h"
#include <core/memory/allocations.h>
#include <core/memory/MemoryManager.h>

#define LABEL_COUNT_IN_POOL (128)

namespace graphics
{
    
namespace LabelFactory
{
memory::PoolAllocator* gLabelPool = nullptr;
}//namespace LabelFactory
//------------------------------------------------------------------------------
void LabelFactory::init()
{
    gLabelPool = memory::MemoryManager::createPool<Label>("label_allocator",
                                                          LABEL_COUNT_IN_POOL);
}
//------------------------------------------------------------------------------
void LabelFactory::deinit()
{
    memory::MemoryManager::destructPool(gLabelPool);
}
//------------------------------------------------------------------------------
Label* LabelFactory::createLabel()
{
    auto label = memory::allocate<Label>(*gLabelPool);
    return label;
}
//------------------------------------------------------------------------------
Label* LabelFactory::createLabel(const char* text)
{
    auto label = memory::allocate<Label>(*gLabelPool);
    label->setText(text);
    return label;
}
//------------------------------------------------------------------------------
Label* LabelFactory::createLabel(const char* text, const char* font)
{
    auto label = memory::allocate<Label>(*gLabelPool);
    label->setFontName(font);
    label->setText(text);
    return label;
}
//------------------------------------------------------------------------------
Label* LabelFactory::createLabel(const char* font, float size)
{
    auto label = memory::allocate<Label>(*gLabelPool);
    label->setFontSize(size);
    return label;
}
//------------------------------------------------------------------------------
void LabelFactory::destroyLabel(Label* label)
{
    memory::deallocate(*gLabelPool, label);
}
//------------------------------------------------------------------------------
}//namespace graphics
