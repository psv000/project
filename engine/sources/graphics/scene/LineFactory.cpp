//
//  LineFactory.cpp
//  engine
//
//  Created by s.popov on 23/10/2017.
//
//

#include "LineFactory.h"
#include <core/memory/allocations.h>
#include <core/memory/allocators/PoolAllocator.h>
#include <core/memory/MemoryManager.h>

#define LINES_COUNT_IN_POOL (200)
#define POLYLINE_LINES_COUNT_IN_POOL (200)

namespace graphics
{
    
namespace LineFactory
{
memory::PoolAllocator* gLinePool = nullptr;
memory::PoolAllocator* gPolylinePool = nullptr;

}//namespace LineFactory
//------------------------------------------------------------------------------
void LineFactory::init()
{
    gLinePool = memory::MemoryManager::createPool<Line>("line_allocator",
                                                        LINES_COUNT_IN_POOL);
    gPolylinePool = memory::MemoryManager::createPool<Polyline>("polyline_allocator",
                                                                POLYLINE_LINES_COUNT_IN_POOL);
}
//------------------------------------------------------------------------------
void LineFactory::deinit()
{
    memory::MemoryManager::destructPool(gLinePool);
    memory::MemoryManager::destructPool(gPolylinePool);
}
//------------------------------------------------------------------------------
Line* LineFactory::createLine()
{
    auto line = memory::allocate<Line>(*gLinePool);
    return line;
}
//------------------------------------------------------------------------------
Line* LineFactory::createLine(graphics::VerticesType type, const void* vertices, uint32 count, uint8 thickness)
{
    auto line = memory::allocate<Line>(*gLinePool);
    line->setPoints(type, vertices, count, thickness);
    return line;
}
//------------------------------------------------------------------------------
void LineFactory::destroyLine(Line* line)
{
    memory::deallocate(*gLinePool, line);
}
//------------------------------------------------------------------------------
Polyline* LineFactory::createPolyline()
{
    auto line = memory::allocate<Polyline>(*gPolylinePool);
    line->setProgram("shaders/mesh.vert", "shaders/mesh.frag");
    return line;
}
//------------------------------------------------------------------------------
Polyline* LineFactory::createPolyline(graphics::VerticesType type, const void* vertices, uint32 count, float thickness)
{
    auto line = memory::allocate<Polyline>(*gPolylinePool);
    line->setProgram("shaders/mesh.vert", "shaders/mesh.frag");
    line->setPoints(type, vertices, count, thickness);
    return line;
}
//------------------------------------------------------------------------------
void LineFactory::destroyPolyline(Polyline* line)
{
    memory::deallocate(*gPolylinePool, line);
}
//------------------------------------------------------------------------------
}//namespace graphics
