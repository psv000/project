//
//  SceneContainer.cpp
//  engine
//
//  Created by s.popov on 27/09/2017.
//
//

#include "SceneContainer.h"
#include "Node.h"
#include "Camera.h"
#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include <graphics/Render.h>

namespace graphics
{
//------------------------------------------------------------------------------
SceneContainer::SceneContainer(Camera* camera, const vec3f& size):
mSceneTransform(1.f),
mCamera(camera),
mPivot(0.f),
mSize(size),
mZOrder(0)
{
    mState.setFlags(RenderState::BLEND_ENABLED);
    mState.setBlendFunc(RenderState::BlendFunc::SrcAlpha, RenderState::BlendFunc::OneMinusSrcAlpha);
}
//------------------------------------------------------------------------------
SceneContainer::~SceneContainer()
{
    mObjects.clear();
}
//------------------------------------------------------------------------------
void SceneContainer::addNode(Node* node)
{
    if (!SceneContainer::hasNode(node))
    {
        node->setScene(this);
        mObjects.push_back(node);
        
        SceneContainer::resortNodes();
    }
}
//------------------------------------------------------------------------------
bool SceneContainer::hasNode(Node* node)
{
    return node->scene() == this;
}
//------------------------------------------------------------------------------
void SceneContainer::removeNode(Node* node)
{
    if (!node)
    {
        return;
    }
    node->setScene(nullptr);
    auto it = std::find(mObjects.begin(), mObjects.end(), node);
    void_assert(node == *it);
    if (it != mObjects.end()) mObjects.erase(it);
}
//------------------------------------------------------------------------------
void SceneContainer::resortNodes()
{
    std::sort(mObjects.begin(), mObjects.end(), [](const Node* l, const Node* r)
    {
        return l->zOrder() < r->zOrder();
    });
}
//------------------------------------------------------------------------------
void SceneContainer::setPosition(const vec2f& p)
{
    float designScaleFactor = Activity::designScaleFactor();
    mSceneTransform[3][0] = p.x * designScaleFactor;
    mSceneTransform[3][1] = p.y * designScaleFactor;
    
    if ( mState.isScissorEnabled() )
    {
        float designScaleFactor = Activity::designScaleFactor();
        mState.setScissor(mSceneTransform[3][0], mSceneTransform[3][1],
                          mSize.x * designScaleFactor, mSize.y * designScaleFactor);
    }
}
//------------------------------------------------------------------------------
vec2f SceneContainer::position() const
{
    float designScaleFactor = Activity::designScaleFactor();
    return vec2f(mSceneTransform[3][0] / designScaleFactor,
                 mSceneTransform[3][1] / designScaleFactor);
}
//------------------------------------------------------------------------------
void SceneContainer::setPivot(const vec3f &v)
{
    mPivot = v;
}
//------------------------------------------------------------------------------
vec3f SceneContainer::size() const
{
    return mSize;
}
//------------------------------------------------------------------------------
void SceneContainer::startUpdate()
{
    Activity::render()->registerDrawable(this);
}
//------------------------------------------------------------------------------
void SceneContainer::stopUpdate()
{
    Activity::render()->unregisterDrawable(this);
}
//------------------------------------------------------------------------------
void SceneContainer::update()
{
    {
        auto& commandQueue = Activity::render()->renderCommandQueue();
        commandQueue.beginCommand(RenderCommandType::SetState);
        commandQueue.addValue(mState);
        commandQueue.endCommand();
    }
    
    auto mvp = mCamera->viewProjection() * mSceneTransform;
    for (Node* object : mObjects)
    {
        object->update(mvp);
    }
}
//------------------------------------------------------------------------------
void SceneContainer::resize(uint16 width, uint16 height)
{
    mSize.x = width;
    mSize.y = height;
    
    if ( mState.isScissorEnabled() )
    {
        float designScaleFactor = Activity::designScaleFactor();
        mState.setScissor(mSceneTransform[3][0], mSceneTransform[3][1],
                          mSize.x * designScaleFactor, mSize.y * designScaleFactor);
    }
}
//------------------------------------------------------------------------------
void SceneContainer::scissor(bool enabled)
{
    if ( enabled )
        mState.setFlags(RenderState::SCISSOR_ENABLED);
    else
        mState.unsetFlags(RenderState::SCISSOR_ENABLED);
}
//------------------------------------------------------------------------------
void SceneContainer::clear()
{
    mObjects.clear();
}
//------------------------------------------------------------------------------
}//namespace graphics
