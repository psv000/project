//
//  Camera.h
//  engine
//
//  Created by s.popov on 21/09/2017.
//
//

#pragma once

#include <math/mat4.h>
#include <math/vec3.h>
#include <math/rect.h>

#include <common/inttypes.h>

#include <graphics/scene/Transform.h>

namespace graphics
{
    
class Camera
{
public:
    
    struct Options
    {
        vec3f minScaling {0.f};
        vec3f maxScaling {0.f};
        
        rectf bound {0.f};
        
        bool ignoreBoundingX {false};
        bool ignoreBoundingY {false};
        bool ignoreBoundingZ {false};
    };
    
    enum class Type : uint8
    {
        Orthographic = 0,
        Perspective
    };
    
public:
    Camera(Camera::Type);
    
    void setOptions(const Options& options) { mOptions = options; }
    
    const Transform& view() const { return mView; }
    const Transform& projection() const { return mProjection; }
    
    const Transform& viewProjection() const { return mViewProjection; }
    
    void updateProjections(int width, int height);
    inline void update() { if ( mIsDirty ) { mViewProjection = mProjection * mView; } }
    
    void setPosition(const vec3f&);
    vec3f position() const;
    
    void setScaling(const vec3f&);
    vec3f scaling() const;
    
private:
    Type mType;
    
    Transform mView;
    Transform mProjection;
    Transform mViewProjection;
    
    Options mOptions;
    
    bool mIsDirty;
};
    
}//namespace graphics
