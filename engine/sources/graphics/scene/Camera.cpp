//
//  Camera.cpp
//  engine
//
//  Created by s.popov on 21/09/2017.
//
//

#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>

#include <core/Activity.h>
#include <graphics/Render.h>

#include <math/vec-helpers.h>

namespace graphics
{
//------------------------------------------------------------------------------
Camera::Camera(Camera::Type type):
mType(type),
mView(1.f),
mProjection(1.f),
mViewProjection(1.f),
mIsDirty(false)
{
}
//------------------------------------------------------------------------------
void Camera::updateProjections(int width, int height)
{
    if (mType == Camera::Type::Orthographic)
    {
        mProjection = glm::ortho(0.f, (float)width,
                                 0.f, (float)height,
                                 -1.f, 1.f);
    }
    else if (mType == Camera::Type::Perspective)
    {
        float aspect = (float)width / height;
        mProjection = glm::perspective(120.f, aspect, 1.0f, -1.f);
    }
    
    mViewProjection = mProjection * mView;
}
//------------------------------------------------------------------------------
inline float setPositionValue(float val, float inf, float sup, bool ignore)
{
    if ( !ignore )
    {
        return math::clamp(val, inf, sup);
    }
    return val;
}
//------------------------------------------------------------------------------
void Camera::setPosition(const vec3f& p)
{
    mView[3][0] = setPositionValue(p.x, mOptions.bound.origin.x, mOptions.bound.size.x, mOptions.ignoreBoundingX);
    mView[3][1] = setPositionValue(p.y, mOptions.bound.origin.y, mOptions.bound.size.y, mOptions.ignoreBoundingY);
//    mView[3][2] = setPositionValue(p.z, mOptions.bound.origin.z, mOptions.bound.size.z, mOptions.ignoreBoundingZ);
    mIsDirty = true;
}
//------------------------------------------------------------------------------
vec3f Camera::position() const
{
    return vec3f(mView[3][0], mView[3][1], mView[3][2]);
}
//------------------------------------------------------------------------------
inline float setScaleValue(float val, float inf, float sup, float defaultValue)
{
    if ( inf && sup )
    {
        return math::clamp(val, inf, sup);
    }
    return defaultValue;
}
//------------------------------------------------------------------------------
void Camera::setScaling(const vec3f& s)
{
    mView[0][0] = setScaleValue(s.x, mOptions.minScaling.x, mOptions.maxScaling.x, mView[0][0]);
    mView[1][1] = setScaleValue(s.y, mOptions.minScaling.y, mOptions.maxScaling.y, mView[1][1]);
    mView[2][2] = setScaleValue(s.z, mOptions.minScaling.z, mOptions.maxScaling.z, mView[2][2]);
    mIsDirty = true;
}
//------------------------------------------------------------------------------
vec3f Camera::scaling() const
{
    return vec3f(mView[0][0], mView[1][1], mView[2][2]);
}
//------------------------------------------------------------------------------
}//namespace graphics
