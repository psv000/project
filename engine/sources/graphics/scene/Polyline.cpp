//
//  Polyline.cpp
//  engine
//
//  Created by s.popov on 04/12/2017.
//
//

#include "Polyline.h"
#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include <graphics/Render.h>
#include <graphics/IGraphicsDevice.h>
#include <graphics/shaders/Program.h>
#include <graphics/DrawHelper.h>
#include "PolylineHelpers.h"

#include <logs.h>

namespace graphics
{
    
//------------------------------------------------------------------------------
Polyline::Polyline():
mShader(nullptr),
mIndicesCount(0),
mDrawMode(PolylineDrawMode::Strip),
mThickness(1.f)
{
    auto device = Activity::render()->graphicsDevice();
    
    mVBO = device->createVertexBuffer();
    mIBO = device->createIndexBuffer();
    mVAO = device->createVertexArrayObject();
}
//------------------------------------------------------------------------------
Polyline::~Polyline()
{
    auto device = Activity::render()->graphicsDevice();
    
    device->destroyShader(mShader);
    
    device->destroyVertexBuffer(mVBO);
    device->destroyIndexBuffer(mIBO);
    device->destroyVertexArrayObject(mVAO);
}
//------------------------------------------------------------------------------
void Polyline::setProgram(const char* vertex, const char* fragment)
{
    auto device = Activity::render()->graphicsDevice();
    mShader = device->createProgram(vertex, fragment);
    {
        UEArray ueArray;
        
        UniformElement elements[] =
        {
            {"umvp", UniformType::Mat4},
            {"ucolor", UniformType::Vec4}
        };
        
        ueArray.elements = elements;
        ueArray.size = lengthof(elements);
        
        UniformContainer containers[ueArray.size];
        
        auto device = Activity::render()->graphicsDevice();
        device->applyShader(mShader->id());
        device->createUniformContainer(mShader->id(), ueArray, containers);
        
        for (uint8 i = 0; i < ueArray.size; ++i)
        {
            mProgramArgs.registerArgument(ueArray.elements[i].name.c_str(), containers[i].location);
        }
    }
}
//------------------------------------------------------------------------------
void Polyline::setPoints(VerticesType vertexType, const void* inVertices, uint32 inVerticesCount, float thickness)
{
    uint16 vertexSize = 0;
    MVCArray mvcArray;
    
    void* outVertices = nullptr;
    uint32 outVerticesCount = 0;
    
    uint32* indices = nullptr;
    mIndicesCount = 0;
    
    mThickness = thickness;
    
    switch(vertexType)
    {
        case VerticesType::V3f_C4u:
        {
            vertexSize = sizeof(v3fc4b);

            polylineHelpers::polygonize(inVertices, inVerticesCount, vertexSize, 0,
                                        outVertices, outVerticesCount, vertexSize, 0,
                                        indices, mIndicesCount, mDrawMode, mThickness * 0.5f);
            
            polylineHelpers::colorize(inVertices, inVerticesCount, vertexSize, sizeof(vec3f),
                                      outVertices, outVerticesCount, vertexSize, sizeof(vec3f),
                                      mDrawMode);
            
            DrawHelper::scaleMeshByDensity(outVertices, vertexSize, 0, outVerticesCount);
            
            static MeshVertexComponent components[] = {
                { VertexAttribute::Position, 3, VertexDataType::Float },
                { VertexAttribute::Color, 4, VertexDataType::Byte }
            };
            mvcArray.components = components;
            mvcArray.size = lengthof(components);
            
            break;
        }
        case VerticesType::V3f:
            vertexSize = sizeof(v3f);
            
            polylineHelpers::polygonize(inVertices, inVerticesCount, vertexSize, 0,
                                        outVertices, outVerticesCount, vertexSize, 0,
                                        indices, mIndicesCount, mDrawMode, mThickness * 0.5f);
            
            DrawHelper::scaleMeshByDensity(outVertices, vertexSize, 0, outVerticesCount);
            
            static MeshVertexComponent components[] = {
                { VertexAttribute::Position, 3, VertexDataType::Float }
            };
            mvcArray.components = components;
            mvcArray.size = lengthof(components);
            
            break;
        default:
            msg_assert(false, "unknown vertices type");
            break;
    }
    {
        auto device = Activity::render()->graphicsDevice();
        
        device->updateIndexBuffer(mIBO, BufferUsage::StaticDraw, (void*)indices, mIndicesCount * sizeof(uint32));
        device->updateVertexBuffer(mVBO, outVertices, outVerticesCount * vertexSize);
        
        device->applyShader(mShader->id());
        device->updateVertexArrayObject(mvcArray, mVAO, mVBO, mIBO);
        
        memory::dealloc(outVertices);
        memory::dealloc(indices);
    }
}
//------------------------------------------------------------------------------
void Polyline::update(const Transform& transform)
{
    if ( !Colorizable::isVisible() ) return;
    
    Node::update(transform);
    
    mProgramArgs.applyArgument("umvp", Node::mWorldTransform);
    mProgramArgs.applyArgument("ucolor", Colorizable::mResultColor);
    DrawHelper::queueMeshToRender(mVAO,
                                  mIndicesCount,
                                  mShader,
                                  mProgramArgs);
}
//------------------------------------------------------------------------------
}//namespace graphics
