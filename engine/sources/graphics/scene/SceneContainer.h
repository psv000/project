//
//  SceneContainer.h
//  engine
//
//  Created by s.popov on 27/09/2017.
//
//

#pragma once

#include "Transform.h"
#include <graphics/IDrawable.h>
#include <graphics/RenderState.h>
#include <math/vec2.h>
#include <math/vec3.h>
#include <vector>

namespace graphics
{

class Camera;
class Node;
class SceneContainer final : public IDrawable
{
public:
    SceneContainer(Camera*, const vec3f&);
    ~SceneContainer();
    
    SceneContainer(const SceneContainer&) = delete;
    SceneContainer& operator=(const SceneContainer&) = delete;
    
    void addNode(Node*);
    void removeNode(Node*);
    bool hasNode(Node*);
    
    void resortNodes();
    
    void update() override;
    void resize(uint16, uint16);
    
    void startUpdate();
    void stopUpdate();
    
    void setPosition(const vec2f&);
    vec2f position() const;
    
    void setPivot(const vec3f &);
    vec3f pivot() const { return mPivot; }
    vec3f size() const;
    
    void setZOrder(int order) override { mZOrder = order; }
    int zOrder() const override { return mZOrder; }
    
    void scissor(bool);
    
    const Camera* camera() const { return mCamera; }
    Camera* camera() { return mCamera; }
    
    void clear();
    
public:
    static const int MIN_SCENE_ORDER { INT_MIN };
    static const int MAX_SCENE_ORDER { INT_MAX };
    
private:
    using SceneObjects = std::vector<Node*>;
    SceneObjects mObjects;
    
    Transform mSceneTransform;
    Camera* mCamera;

    RenderState mState;
    
    vec3f mPivot;
    vec3f mSize;
    
    int mZOrder;
};
    
}//namespace graphics
