//
//  LineFactory.h
//  engine
//
//  Created by s.popov on 23/10/2017.
//
//

#pragma once

#include "Line.h"
#include "Polyline.h"

namespace graphics
{

namespace LineFactory
{

void init();
void deinit();

Line* createLine();
Line* createLine(graphics::VerticesType, const void*, uint32, uint8);
void destroyLine(Line*);
    
Polyline* createPolyline();
Polyline* createPolyline(graphics::VerticesType, const void*, uint32, float);
void destroyPolyline(Polyline*);
    
}//namespace LineFactory
    
}//namespace graphics
