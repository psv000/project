//
//  Transform.h
//  engine
//
//  Created by s.popov on 27/09/2017.
//
//

#pragma once

#include <math/mat4.h>

using Transform = mat4f;
