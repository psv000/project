//
//  Sprite.cpp
//  engine
//
//  Created by s.popov on 04/10/2017.
//
//

#include "Sprite.h"
#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include <graphics/Render.h>
#include <graphics/IGraphicsDevice.h>
#include <graphics/shaders/Program.h>
#include <graphics/DrawHelper.h>

#include <graphics/scene/SceneContainer.h>

#include <graphics/MaterialManager.h>
#include <graphics/Material.h>
#include <graphics/textures/Texture.h>

#include <common/formatstring.h>

namespace graphics
{
//------------------------------------------------------------------------------
Sprite::Sprite():
Node(),
mShader(nullptr),
mProgramArgs(),
mMaterial(nullptr),
mVerticesCount(0),
mIndicesCount(0),
mStretchRect(-1),
mIsDirty(true)
{
    auto device = Activity::render()->graphicsDevice();
    
    mVBO = device->createVertexBuffer();
    mIBO = device->createIndexBuffer();
    mVAO = device->createVertexArrayObject();

    mShader = device->createProgram("shaders/texture.vert", "shaders/texture.frag");
    {
        UEArray ueArray;
        
        UniformElement elements[] =
        {
            { "umvp", UniformType::Mat4},
            { "utexture0", UniformType::Texture2D },
            { "ucolor", UniformType::Vec4 }
        };
        
        ueArray.elements = elements;
        ueArray.size = lengthof(elements);
        
        UniformContainer containers[ueArray.size];
        
        auto device = Activity::render()->graphicsDevice();
        device->applyShader(mShader->id());
        device->createUniformContainer(mShader->id(), ueArray, containers);
        
        for (uint8 i = 0; i < ueArray.size; ++i)
        {
            mProgramArgs.registerArgument(ueArray.elements[i].name.c_str(), containers[i].location);
        }
    }
}
//------------------------------------------------------------------------------
Sprite::~Sprite()
{
    if (mMaterial != nullptr)
    {
        MaterialManager::releaseMaterial(mMaterial);
    }
    
    auto device = Activity::render()->graphicsDevice();
    
    device->destroyShader(mShader);
    
    device->destroyVertexBuffer(mVBO);
    device->destroyIndexBuffer(mIBO);
    device->destroyVertexArrayObject(mVAO);
}
//------------------------------------------------------------------------------
void Sprite::setMaterial(const char* name)
{
    if (mMaterial != nullptr)
    {
        MaterialManager::releaseMaterial(mMaterial);
    }
    
    mMaterial = MaterialManager::obtainMaterial(name);
    msg_assert(mMaterial != nullptr, format_string("atlas for material with name <%s> didn't load", name).c_str());
    
    Sprite::setDirty();
}
//------------------------------------------------------------------------------
void Sprite::setMaterial(Material* material)
{
    if (mMaterial != nullptr)
    {
        MaterialManager::releaseMaterial(mMaterial);
    }
    
    mMaterial = material;
    msg_assert(mMaterial->texture != nullptr, "material hasn\'t texture");
    
    Sprite::setDirty();
}
//------------------------------------------------------------------------------
void Sprite::updateContent()
{
    MVCArray mvcArray;
    {
        const uint8 AttributesCount {2};
        static MeshVertexComponent components[AttributesCount];
        components[0] = {VertexAttribute::Position, 3, VertexDataType::Float};
        components[1] = {VertexAttribute::Texture, 2, VertexDataType::UnsignedShort};

        mvcArray.components = components;
        mvcArray.size = lengthof(components);
    }

    Sprite::updateSpriteRect();
    
    auto device = Activity::render()->graphicsDevice();
    device->applyShader(mShader->id());
    device->updateVertexArrayObject(mvcArray, mVAO, mVBO, mIBO);
}
//------------------------------------------------------------------------------
void Sprite::update(const Transform& transform)
{
    if (!Colorizable::isVisible()) { return; }
    
    if ( Sprite::isDirty() )
    {
        Sprite::updateContent();
        mIsDirty = false;
    }

    Node::update(transform);
    
    mProgramArgs.applyArgument("umvp", Node::mWorldTransform);
    mProgramArgs.applyArgument("utexture0", (TexturePack){mMaterial->texture->level(),
                                                            mMaterial->texture->id()});
    
    mProgramArgs.applyArgument("ucolor", Colorizable::mResultColor);
    
    msg_assert(mMaterial != nullptr, "sprite hasn\'t material");
    if (mMaterial == nullptr)
    {
        return;
    }
    
    DrawHelper::queueMeshToRender(mVAO, mIndicesCount, mShader, mProgramArgs);
}
//------------------------------------------------------------------------------
void Sprite::setStretchPoint(const vec2i& point)
{
    Sprite::setStretchRect(point, point);
}
//------------------------------------------------------------------------------
void Sprite::setStretchRect(const vec2i& origin, const vec2i& size)
{
    float logicScaleFactor = Activity::logicScaleFactor();
    
    mStretchRect.origin = origin;
    mStretchRect.size = size;
    
    mStretchRect.origin.x *= logicScaleFactor;
    mStretchRect.origin.y *= logicScaleFactor;
    mStretchRect.size.x *= logicScaleFactor;
    mStretchRect.size.y *= logicScaleFactor;
    
    Sprite::setDirty();
}
//------------------------------------------------------------------------------
void Sprite::updateSpriteRect()
{
    if ( !mMaterial ) { return; }
    
    auto &mat_origin = mMaterial->origin;
    auto &mat_rect = mMaterial->rect;
    auto &mat_size = mMaterial->size;
    
    float logicScaleFactor = Activity::logicScaleFactor();
    float sizeMultiplier = Activity::designScaleFactor();
    
    if ( mStretchRect.origin.x < 0 || mStretchRect.origin.y < 0 )
    {
        mVerticesCount = 4;
        mVertices[0] = { vec3f(0.f, 0.f, 0.f), mat_origin };
        mVertices[1] = { vec3f(1.f, 0.f, 0.f), vec2us(mat_rect.x, mat_origin.y) };
        mVertices[2] = { vec3f(1.f, 1.f, 0.f), mat_rect };
        mVertices[3] = { vec3f(0.f, 1.f, 0.f), vec2us(mat_origin.x, mat_rect.y) };
        
        mIndicesCount = 6;
        mIndices[0] = 0;    mIndices[3] = 0;
        mIndices[1] = 1;    mIndices[4] = 2;
        mIndices[2] = 2;    mIndices[5] = 3;
        
        Node::mSize = mMaterial->size;
        Node::mSize.x /= logicScaleFactor;
        Node::mSize.y /= logicScaleFactor;
    }
    else
    {
        auto textureSize = mMaterial->texture->size();
        
        uint16 xrect = ( (float)(mStretchRect.origin.x + (float)mat_origin.x / USHRT_MAX * textureSize.x ) / textureSize.x ) * USHRT_MAX;
        uint16 yrect = ( (float)(mStretchRect.origin.y + (float)mat_origin.y / USHRT_MAX * textureSize.y ) / textureSize.y ) * USHRT_MAX;
        
        uint16 wrect = ( (float)(mStretchRect.size.x + (float)mat_origin.x / USHRT_MAX * textureSize.x ) / textureSize.x ) * USHRT_MAX;
        uint16 hrect = ( (float)(mStretchRect.size.y + (float)mat_origin.y / USHRT_MAX * textureSize.y ) / textureSize.y ) * USHRT_MAX;

        mSize.x *= logicScaleFactor;
        mSize.y *= logicScaleFactor;
        
        mSize.x = GET_MAX(mSize.x, (float)mat_size.x);
        mSize.y = GET_MAX(mSize.y, (float)mat_size.y);
        
        float xdiff = ( mSize.x - mat_size.x ) / mSize.x;
        float ydiff = ( mSize.y - mat_size.y ) / mSize.y;

        mSize.x /= logicScaleFactor;
        mSize.y /= logicScaleFactor;
        
        float x = 0.5 - 0.5 * xdiff;
        float y = 0.5 - 0.5 * ydiff;
        
        float w = 0.5 + 0.5 * xdiff;
        float h = 0.5 + 0.5 * ydiff;
        
        void_assert(    xrect < mat_rect.x && yrect < mat_rect.y
                    &&  wrect < mat_rect.x && hrect < mat_rect.y );
        
        mVerticesCount = 16;
        mVertices[0] = { vec3f(0.f, 0.f, 0.f)   , vec2us(mat_origin) };
        mVertices[1] = { vec3f(x, 0.f, 0.f)     , vec2us(xrect, mat_origin.y) };
        mVertices[2] = { vec3f(x, y, 0.f)       , vec2us(xrect, yrect) };
        mVertices[3] = { vec3f(0.f, y, 0.f)     , vec2us(mat_origin.x, yrect) };
        
        mVertices[4] = { vec3f(w, 0.f, 0.f)     , vec2us(wrect, mat_origin.y) };
        mVertices[5] = { vec3f(1.f, 0.f, 0.f)   , vec2us(mat_rect.x, mat_origin.y) };
        mVertices[6] = { vec3f(1.f, y, 0.f)     , vec2us(mat_rect.x, yrect) };
        mVertices[7] = { vec3f(w, y, 0.f)     , vec2us(wrect, yrect) };
        
        mVertices[8] =  { vec3f(w, h, 0.f)       , vec2us(wrect, hrect) };
        mVertices[9] =  { vec3f(1.f, h, 0.f)     , vec2us(mat_rect.x, hrect) };
        mVertices[10] = { vec3f(1.f, 1.f, 0.f)  , vec2us(mat_rect.x, mat_rect.y) };
        mVertices[11] = { vec3f(w, 1.f, 0.f)    , vec2us(wrect, mat_rect.y) };
        
        mVertices[12] = { vec3f(0.f, h, 0.f)    , vec2us(mat_origin.x, hrect) };
        mVertices[13] = { vec3f(x, h, 0.f)      , vec2us(xrect, hrect) };
        mVertices[14] = { vec3f(x, 1.f, 0.f)    , vec2us(xrect, mat_rect.y) };
        mVertices[15] = { vec3f(0.f, 1.f, 0.f)  , vec2us(mat_origin.x, mat_rect.y) };
        
        mIndicesCount = 42;
        
        mIndices[0] = 0;    mIndices[3] = 0;
        mIndices[1] = 1;    mIndices[4] = 2;
        mIndices[2] = 2;    mIndices[5] = 3;
        
        mIndices[6] = 4;    mIndices[9] = 4;
        mIndices[7] = 5;    mIndices[10] = 6;
        mIndices[8] = 6;    mIndices[11] = 7;
        
        mIndices[12] = 8;    mIndices[15] = 8;
        mIndices[13] = 9;    mIndices[16] = 10;
        mIndices[14] = 10;   mIndices[17] = 11;
        
        mIndices[18] = 12;   mIndices[21] = 12;
        mIndices[19] = 13;   mIndices[22] = 14;
        mIndices[20] = 14;   mIndices[23] = 15;
        
        mIndices[24] = 3;    mIndices[27] = 3;
        mIndices[25] = 6;    mIndices[28] = 9;
        mIndices[26] = 9;    mIndices[29] = 12;
        
        mIndices[30] = 1;    mIndices[33] = 1;
        mIndices[31] = 4;    mIndices[34] = 7;
        mIndices[32] = 7;    mIndices[35] = 2;
        
        mIndices[36] = 13;   mIndices[39] = 13;
        mIndices[37] = 8;    mIndices[40] = 11;
        mIndices[38] = 11;   mIndices[41] = 14;
    }
    
    auto v_multiplier = vec3f(mSize * sizeMultiplier, 1.f);
    for (int i = 0; i < mVerticesCount; ++i)
    {
        mVertices[i].pos *= v_multiplier;
    }
    
    auto device = Activity::render()->graphicsDevice();
    device->updateIndexBuffer(mIBO, BufferUsage::StaticDraw, (void*)mIndices, mIndicesCount * sizeof(uint32));
    device->updateVertexBuffer(mVBO, mVertices, mVerticesCount * sizeof(v3fu2us));
}
//------------------------------------------------------------------------------
void Sprite::setSize(const vec2f& size)
{
    Node::setSize(size);
    Sprite::setDirty();
}
//------------------------------------------------------------------------------
}//namespace graphics
