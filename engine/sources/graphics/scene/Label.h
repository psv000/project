//
//  Label.h
//  engine
//
//  Created by s.popov on 06/10/2017.
//
//

#pragma once

#include "Node.h"
#include "Colorizable.h"
#include <common/stringtypes.h>

#include <graphics/RenderPrimitives.h>
#include <graphics/RenderState.h>
#include <graphics/fonts/TextPrerequisites.h>
#include <graphics/shaders/ProgramArgs.h>

namespace textures
{
class Texture;
}//namespace textures

namespace graphics
{

class Program;
class Label final
    :   public Node,
        public Colorizable
{
public:
    Label();
    ~Label();
    
    Label(const Label&) = delete;
    Label& operator=(const Label&) = delete;
    
    void setFont(const fonts::FontProperties&);
    void setFontName(const string&);
    void setFontSize(float);
    
    fonts::FontProperties font() const { return mFont; }
    fonts::FontProperties& font() { return mFont; }
    const string& fontName() const { return mFont.name; }
    
    void setText(const string&);
    const string& text() const { return mText; }
    
    void update(const Transform&) override;
    
    void setVisible(bool);
    void updateContent();
    
private:
    Program* mShader;
    ProgramArgs mProgramArgs;
    
    VertexArrayObjectID mVAO;
    VertexBufferID mVBO;
    IndexBufferID mIBO;
    
    textures::Texture* mTexture;
    
    string mText;
    fonts::FontProperties mFont;
    
    uint32 mIndicesCount;

    bool mDirty;
};
    
}//namespace graphics
