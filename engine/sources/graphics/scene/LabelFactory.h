//
//  LabelFactory.h
//  engine
//
//  Created by s.popov on 06/10/2017.
//
//

#pragma once

#include "Label.h"

namespace graphics
{

namespace LabelFactory
{

void init();
void deinit();
Label* createLabel();
Label* createLabel(const char*);
Label* createLabel(const char*, const char*);
Label* createLabel(const char*, float);
void destroyLabel(Label*);
    
}//namespace LabelFactory
    
}//namespace graphics
