//
//  Mesh.h
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#pragma once

#include "Node.h"
#include "Colorizable.h"
#include <graphics/RenderPrimitives.h>
#include <graphics/VerticesTypes.h>
#include <graphics/shaders/ProgramArgs.h>

namespace graphics
{

class Program;
class Mesh final
    :   public Node,
        public Colorizable
{
public:
    Mesh();
    ~Mesh();
    
    Mesh(const Mesh&) = delete;
    Mesh& operator=(const Mesh&) = delete;
    
    void setProgram(const char*, const char*);
    ProgramID program() const;
    void setContent(VerticesType, void*, uint32, uint32*, uint64);
    void update(const Transform&) override;
    
    ProgramArgs& programArgs() { return mProgramArgs; }
    
    void setName(const string& name)
    {
#ifdef DEBUG_ENABLED
        mName = name;
#endif//DEBUG_ENABLED
    }
    
private:
    Program* mShader;    
    
    VertexArrayObjectID mVAO;
    VertexBufferID mVBO;
    IndexBufferID mIBO;
    uint64 mIndicesCount;
    ProgramArgs mProgramArgs;
    
#ifdef DEBUG_ENABLED
    string mName;
#endif//DEBUG_ENABLED
};
    
}//namespace graphics
