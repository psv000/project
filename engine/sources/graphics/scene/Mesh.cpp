//
//  Mesh.cpp
//  engine
//
//  Created by s.popov on 20/09/2017.
//
//

#include "Mesh.h"
#include <core/Activity.h>
#include <graphics/Render.h>
#include <graphics/RenderCommandQueue.h>
#include <graphics/IGraphicsDevice.h>
#include <graphics/shaders/Program.h>
#include <graphics/VerticesTypes.h>
#include <graphics/DrawHelper.h>

#include <utils/arrayutils.h>

#include <logs.h>

namespace graphics
{
//------------------------------------------------------------------------------
Mesh::Mesh():
Node(),
mShader(nullptr),
mIndicesCount(0)
{
    auto device = Activity::render()->graphicsDevice();
    
    mVBO = device->createVertexBuffer();
    mIBO = device->createIndexBuffer();
    mVAO = device->createVertexArrayObject();
}
//------------------------------------------------------------------------------
Mesh::~Mesh()
{
    auto device = Activity::render()->graphicsDevice();
    
    device->destroyShader(mShader);
    
    device->destroyVertexBuffer(mVBO);
    device->destroyIndexBuffer(mIBO);
    device->destroyVertexArrayObject(mVAO);
}
//------------------------------------------------------------------------------
void Mesh::setProgram(const char* vertex, const char* fragment)
{
    auto device = Activity::render()->graphicsDevice();
    if ( mShader ) device->destroyShader(mShader);
    mShader = device->createProgram(vertex, fragment);
    {
        UEArray ueArray;
        
        UniformElement elements[] = {
            { "umvp", UniformType::Mat4 },
            { "ucolor", UniformType::Vec4 }
        };
        
        ueArray.elements = elements;
        ueArray.size = lengthof(elements);
        
        UniformContainer containers[ueArray.size];
        
        auto device = Activity::render()->graphicsDevice();
        device->applyShader(mShader->id());
        device->createUniformContainer(mShader->id(), ueArray, containers);
        
        for (uint8 i = 0; i < ueArray.size; ++i)
        {
            mProgramArgs.registerArgument(ueArray.elements[i].name.c_str(), containers[i].location);
        }
    }
}
//------------------------------------------------------------------------------
ProgramID Mesh::program() const
{
    return mShader->id();
}
//------------------------------------------------------------------------------
void Mesh::setContent(VerticesType vtype, void* vertices, uint32 verticesCount, uint32* indices, uint64 iCount)
{
    uint16 vertexSize = 0;
    MVCArray mvcArray;
    switch(vtype)
    {
        case VerticesType::V3f:
        {
            vertexSize = sizeof(v3f);
            
            static MeshVertexComponent components[] = {
                { VertexAttribute::Position, 3, VertexDataType::Float }
            };
            mvcArray.components = components;
            mvcArray.size = lengthof(components);
            break;
        }
        case VerticesType::V3f_U2us:
        {
            vertexSize = sizeof(v3fu2us);
            
            static MeshVertexComponent components[] = {
                { VertexAttribute::Position, 3, VertexDataType::Float },
                { VertexAttribute::Texture, 2, VertexDataType::UnsignedShort }
            };
            mvcArray.components = components;
            mvcArray.size = lengthof(components);
            break;
        }
        case VerticesType::V3f_C4u:
        {
            vertexSize = sizeof(v3fc4b);
            
            static MeshVertexComponent components[] = {
                { VertexAttribute::Position, 3, VertexDataType::Float },
                { VertexAttribute::Color, 4, VertexDataType::Byte }
            };
            mvcArray.components = components;
            mvcArray.size = lengthof(components);
            break;
        }
        case VerticesType::V3f_C4u_U2us:
        {
            vertexSize = sizeof(v3fc4bu2us);
            
            static MeshVertexComponent components[] = {
                { VertexAttribute::Position, 3, VertexDataType::Float },
                { VertexAttribute::Color, 4, VertexDataType::Byte },
                { VertexAttribute::Texture, 2, VertexDataType::UnsignedShort }
            };
            mvcArray.components = components;
            mvcArray.size = lengthof(components);
            break;
        }
        default:
            msg_assert(false, "unknown vertices type");
            break;
    }
    
    DrawHelper::alignByPixel(vertices, vertexSize, 0, verticesCount);
    
    mIndicesCount = iCount;
    auto device = Activity::render()->graphicsDevice();
    
    device->updateIndexBuffer(mIBO, BufferUsage::StaticDraw, (void*)indices, mIndicesCount * sizeof(uint32));
    device->updateVertexBuffer(mVBO, vertices, verticesCount * vertexSize);
    
    device->applyShader(mShader->id());
    device->updateVertexArrayObject(mvcArray, mVAO, mVBO, mIBO);
}
//------------------------------------------------------------------------------
void Mesh::update(const Transform& transform)
{
    if ( !mIndicesCount ) return;
    if ( !Colorizable::isVisible() ) return;
    
    Node::update(transform);
    
    mProgramArgs.applyArgument("umvp", Node::mWorldTransform);
    mProgramArgs.applyArgument("ucolor", Colorizable::mResultColor);
    
    DrawHelper::queueMeshToRender(mVAO, mIndicesCount, mShader, mProgramArgs);
}
//------------------------------------------------------------------------------
}//namespace graphics
