//
//  MeshFactory.cpp
//  engine
//
//  Created by s.popov on 27/09/2017.
//
//

#include "MeshFactory.h"
#include <core/memory/allocations.h>
#include <core/memory/MemoryManager.h>

#define MESH_SIZE (sizeof(Mesh))
#define MESHES_COUNT_IN_POOL (128)

namespace graphics
{

namespace MeshFactory
{

memory::PoolAllocator* gMeshPool = nullptr;

}//MeshFactory
//------------------------------------------------------------------------------
void MeshFactory::init()
{
    gMeshPool = memory::MemoryManager::createPool<Mesh>("mesh_allocator",
                                                        MESHES_COUNT_IN_POOL);
}
//------------------------------------------------------------------------------
void MeshFactory::deinit()
{
    memory::MemoryManager::destructPool(gMeshPool);
}
//------------------------------------------------------------------------------
Mesh* MeshFactory::createMesh(const string& name)
{
    auto mesh = memory::allocate<Mesh>(*gMeshPool);
    mesh->setName(name);
    mesh->setProgram("shaders/mesh.vert", "shaders/mesh.frag");
    return mesh;
}
//------------------------------------------------------------------------------
Mesh* MeshFactory::createMesh(const string& vertexShader, const string& fragmentShader, const string& name)
{
    auto mesh = memory::allocate<Mesh>(*gMeshPool);
    mesh->setName(name);
    mesh->setProgram(vertexShader.c_str(), fragmentShader.c_str());
    return mesh;
}
//------------------------------------------------------------------------------
Mesh* MeshFactory::createMesh(VerticesType type, void* vertices, uint32 vCount, uint32* indices, uint32 iCount, const string& name)
{
    auto mesh = memory::allocate<Mesh>(*gMeshPool);
    mesh->setName(name);
    mesh->setProgram("shaders/mesh.vert", "shaders/mesh.frag");
    mesh->setContent(type, vertices, vCount, indices, iCount);
    return mesh;
}
//------------------------------------------------------------------------------
void MeshFactory::destroyMesh(Mesh* mesh)
{
    memory::deallocate(*gMeshPool, mesh);
}
//------------------------------------------------------------------------------
}//namespace graphics
