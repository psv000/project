//
//  SpriteFactory.cpp
//  engine
//
//  Created by s.popov on 04/10/2017.
//
//

#include "SpriteFactory.h"
#include <core/memory/allocations.h>
#include <core/memory/MemoryManager.h>

#define SPRITE_SIZE (sizeof(Sprite))
#define SPRITES_COUNT_IN_POOL (128)

namespace graphics
{
namespace SpriteFactory
{

memory::PoolAllocator* gSpritePool = nullptr;

}//SpriteFactory
//------------------------------------------------------------------------------
void SpriteFactory::init()
{
    gSpritePool = memory::MemoryManager::createPool<Sprite>("sprite_allocator",
                                                            SPRITES_COUNT_IN_POOL);
}
//------------------------------------------------------------------------------
void SpriteFactory::deinit()
{
    memory::MemoryManager::destructPool(gSpritePool);
}
//------------------------------------------------------------------------------
Sprite* SpriteFactory::createSprite(const char* materialName)
{
    auto sprite = memory::allocate<Sprite>(*gSpritePool);
    sprite->setMaterial(materialName);
    return sprite;
}
//------------------------------------------------------------------------------
Sprite* SpriteFactory::createSprite(Material* material)
{
    auto sprite = memory::allocate<Sprite>(*gSpritePool);
    sprite->setMaterial(material);
    return sprite;
}
//------------------------------------------------------------------------------
void SpriteFactory::destroySprite(Sprite* sprite)
{
    memory::deallocate(*gSpritePool, sprite);
}
//------------------------------------------------------------------------------
}//namespace graphics
