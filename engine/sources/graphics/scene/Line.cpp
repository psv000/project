//
//  Line.cpp
//  engine
//
//  Created by s.popov on 23/10/2017.
//
//

#include "Line.h"
#include <core/Activity.h>
#include <core/IPlatformContext.h>
#include <graphics/Render.h>
#include <graphics/IGraphicsDevice.h>
#include <graphics/shaders/Program.h>
#include <graphics/DrawHelper.h>

#include <logs.h>

namespace graphics
{
//------------------------------------------------------------------------------
Line::Line():
mShader(nullptr),
mVerticesCount(0),
mThickness(1u),
mDashLength(1.f),
mPeriod(1.f),
mDrawMode(DrawMode::LineStrip),
mStyle(Style::Solid)
{
    auto device = Activity::render()->graphicsDevice();
    
    mVBO = device->createVertexBuffer();
    mVAO = device->createVertexArrayObject();
}
//------------------------------------------------------------------------------
Line::~Line()
{
    auto device = Activity::render()->graphicsDevice();
    
    device->destroyShader(mShader);
    
    device->destroyVertexBuffer(mVBO);
    device->destroyVertexArrayObject(mVAO);
}
//------------------------------------------------------------------------------
void Line::setPoints(VerticesType vtype, const void* vertices, uint32 verticesCount, float thickness)
{
    mVerticesCount = verticesCount;
    mThickness = math::ceil( thickness * Activity::designScaleFactor() );

    switch(vtype)
    {
        case VerticesType::V3f_C4u:
        {
            Line::configureSolidLine( (v3fc4b*)vertices, verticesCount, thickness );
            break;
        }
        case VerticesType::V3f_C4u_U2us:
        {
            Line::configureDashLine( (v3fc4bu2us*)vertices, verticesCount, thickness );
            break;
        }
        default:
            msg_assert(false, "unknown vertices type");
            break;
    }

}
//------------------------------------------------------------------------------
void Line::setPoints(v3f* vertices, uint32 verticesCount, float thickness, Color4F color)
{
    mVerticesCount = verticesCount;
    mThickness = math::ceil( thickness * Activity::designScaleFactor() );
    Line::configureSolidPlainLine(vertices, verticesCount, thickness, color);
}
//------------------------------------------------------------------------------
void Line::configureSolidPlainLine(v3f* vertices, uint32 verticesCount, float thickness, Color4F color)
{
    mStyle = Style::Solid;
    uint64 size = sizeof(v3f);
    
    static MeshVertexComponent components[] =
    {
        { VertexAttribute::Position, 3, VertexDataType::Float }
    };
    
    MVCArray mvcArray;
    mvcArray.components = components;
    mvcArray.size = lengthof(components);
    
    DrawHelper::alignLineByPixel(vertices, size, 0, verticesCount);
    
    auto device = Activity::render()->graphicsDevice();
    if ( mShader ) device->destroyShader(mShader);
    mShader = device->createProgram("shaders/plain_mesh.vert", "shaders/plain_mesh.frag");
    
    {
        UEArray ueArray;
        UniformElement elements[] = {
            { "umvp", UniformType::Mat4 },
            { "ucolor", UniformType::Vec4 }
        };
        
        ueArray.elements = elements;
        ueArray.size = lengthof(elements);
        
        UniformContainer containers[ueArray.size];
        device->applyShader(mShader->id());
        device->createUniformContainer(mShader->id(), ueArray, containers);
        
        for (uint8 i = 0; i < ueArray.size; ++i)
        {
            mProgramArgs.registerArgument(ueArray.elements[i].name.c_str(), containers[i].location);
        }
    }
    
    device->updateVertexBuffer(mVBO, vertices, verticesCount * size);
    device->applyShader(mShader->id());
    device->updateVertexArrayObject(mvcArray, mVAO, mVBO, INVALID_IBO);
    
    Colorizable::setBaseColor(color);
}
//------------------------------------------------------------------------------
void Line::configureSolidLine(v3fc4b* vertices, uint32 verticesCount, float thickness)
{
    mStyle = Style::Solid;
    uint64 size = sizeof(v3fc4b);
    
    static MeshVertexComponent components[] =
    {
        { VertexAttribute::Position, 3, VertexDataType::Float },
        { VertexAttribute::Color, 4, VertexDataType::Byte }
    };
    
    MVCArray mvcArray;
    mvcArray.components = components;
    mvcArray.size = lengthof(components);
    
    DrawHelper::alignLineByPixel(vertices, size, 0, verticesCount);
    
    auto device = Activity::render()->graphicsDevice();
    if ( mShader ) device->destroyShader(mShader);
    mShader = device->createProgram("shaders/mesh.vert", "shaders/mesh.frag");
    
    {
        UEArray ueArray;
        UniformElement elements[] = {
            { "umvp", UniformType::Mat4 },
            { "ucolor", UniformType::Vec4 }
        };
        
        ueArray.elements = elements;
        ueArray.size = lengthof(elements);
        
        UniformContainer containers[ueArray.size];
        device->applyShader(mShader->id());
        device->createUniformContainer(mShader->id(), ueArray, containers);
        
        for (uint8 i = 0; i < ueArray.size; ++i)
        {
            mProgramArgs.registerArgument(ueArray.elements[i].name.c_str(), containers[i].location);
        }
    }
    
    device->updateVertexBuffer(mVBO, vertices, verticesCount * size);
    device->applyShader(mShader->id());
    device->updateVertexArrayObject(mvcArray, mVAO, mVBO, INVALID_IBO);
}
//------------------------------------------------------------------------------
void Line::configureDashLine(v3fc4bu2us* vertices, uint32 verticesCount, float thickness)
{
    mStyle = Style::Dash;
    uint64 size = sizeof(v3fc4bu2us);
    
    static MeshVertexComponent components[] =
    {
        { VertexAttribute::Position, 3, VertexDataType::Float },
        { VertexAttribute::Color, 4, VertexDataType::Byte },
        { VertexAttribute::Texture, 2, VertexDataType::UnsignedShort }
    };
    
    MVCArray mvcArray;
    mvcArray.components = components;
    mvcArray.size = lengthof(components);
    
    DrawHelper::alignLineByPixel(vertices, size, 0, verticesCount);
    
    auto device = Activity::render()->graphicsDevice();
    if ( mShader ) device->destroyShader(mShader);
    mShader = device->createProgram("shaders/line.vert", "shaders/line.frag");
    
    {
        UEArray ueArray;
        UniformElement elements[] = {
            { "umvp", UniformType::Mat4 },
            { "udash", UniformType::Float},
            { "uperiod", UniformType::Float },
            { "ucolor", UniformType::Vec4 }
        };
        
        ueArray.elements = elements;
        ueArray.size = lengthof(elements);
        
        UniformContainer containers[ueArray.size];
        
        device->applyShader(mShader->id());
        device->createUniformContainer(mShader->id(), ueArray, containers);
        
        for (uint8 i = 0; i < ueArray.size; ++i)
        {
            mProgramArgs.registerArgument(ueArray.elements[i].name.c_str(), containers[i].location);
        }
        
        mProgramArgs.applyArgument("udash", mDashLength);
        mProgramArgs.applyArgument("uperiod", mPeriod);
    }
    
    device->updateVertexBuffer(mVBO, vertices, verticesCount * size);
    device->applyShader(mShader->id());
    device->updateVertexArrayObject(mvcArray, mVAO, mVBO, INVALID_IBO);
}
//------------------------------------------------------------------------------
void Line::update(const Transform& transform)
{
    if ( !mVerticesCount ) return;
    if ( !Colorizable::isVisible() ) return;

    Node::update(transform);
    
    mProgramArgs.applyArgument("umvp", Node::mWorldTransform);
    mProgramArgs.applyArgument("ucolor", Colorizable::mResultColor);

    DrawHelper::queueLineToRender(mVAO,
                                  mVBO,
                                  mVerticesCount,
                                  mThickness,
                                  mDrawMode,
                                  mShader,
                                  mProgramArgs);
}
//------------------------------------------------------------------------------
void Line::setDash(float period, float dashLength)
{
    mPeriod = period;
    mDashLength = dashLength;
    if ( Style::Dash == mStyle )
    {
        mProgramArgs.applyArgument("uperiod", mPeriod);
        mProgramArgs.applyArgument("udash", mDashLength);
    }
}
//------------------------------------------------------------------------------
}//namespace graphics
