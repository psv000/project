//
//  GraphicsDeviceFactory.cpp
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#include "GraphicsDeviceFactory.h"
#include <core/memory/memory.h>
#ifdef GLES_RENDER_ENABLED
#include "GLES/GLESGraphicsDevice.h"
#endif //GLES_RENDER_ENABLED

namespace graphics
{
namespace GraphicsDeviceFactory
{

IGraphicsDevice* createGraphicsDevice()
{
#ifdef GLES_RENDER_ENABLED
    return memory::construct<GLESGraphicsDevice>();
#else
    return nullptr;
#endif //GLES_RENDER_ENABLED
}
    
void destructGraphicsDevice(IGraphicsDevice* graphicsDevice)
{
    memory::destruct(graphicsDevice);
}

}//namespace GraphicsDeviceFactory
}//namespace graphics
