//
//  GraphicsDeviceFactory.h
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#pragma once

#include "IGraphicsDevice.h"

namespace graphics
{
namespace GraphicsDeviceFactory
{
    
IGraphicsDevice* createGraphicsDevice();
void destructGraphicsDevice(IGraphicsDevice*);

}//namespace GraphicsDeviceFactory
}//namespace graphics
