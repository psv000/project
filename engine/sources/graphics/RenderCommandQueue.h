//
//  RendercommandQueue.h
//  engine
//
//  Created by s.popov on 21/09/2017.
//
//

#pragma once

#include "RenderPrimitives.h"
#include <common/inttypes.h>

namespace graphics
{
class IGraphicsDevice;
class RenderCommandQueue final
{
public:
    RenderCommandQueue();
    ~RenderCommandQueue();
    RenderCommandQueue(const RenderCommandQueue&) = delete;
    RenderCommandQueue& operator=(const RenderCommandQueue&) = delete;
    
    void init(IGraphicsDevice*);
    void deinit();
    
    void beginCommand(RenderCommandType command);

    template <typename T>
    void addValue(const T& val, byte** start = nullptr, byte** end = nullptr)
    {
        byte* ptr = &mCommands[mCommandsAllocated];
        byte* head = ptr;
        
        if (start) { *start = ptr; }
        
        array::addValue(ptr, val);
        
        if (mDebugPointer)
        {
            void_assert(&mCommands[mCommandsAllocated] >= mDebugPointer && mDebugPointer <= ptr);
        }
        
        mCommandsAllocated += ptr - head;
        
        if (end) { *end = ptr; }
        
        void_assert( mCommandsAllocated < mCommandsBufferLength);
    }
    
    template <typename T>
    byte* reserve()
    {
        byte* ptr = &mCommands[mCommandsAllocated];
        byte* head = ptr;
        byte* aligned = array::reserve<T>(ptr);
        
        if (mDebugPointer)
        {
            void_assert(&mCommands[mCommandsAllocated] >= mDebugPointer && mDebugPointer <= ptr);
        }
        
        mCommandsAllocated += ptr - head;
        void_assert( mCommandsAllocated < mCommandsBufferLength);
        return aligned;
    }
    
    template<typename T>
    void addArray(T*& data, uint64 size)
    {
        byte* ptr = &mCommands[mCommandsAllocated];
        byte* head = ptr;
        array::addArray(ptr, data, size);
        
        if (mDebugPointer)
        {
            void_assert(&mCommands[mCommandsAllocated] >= mDebugPointer && mDebugPointer <= ptr);
        }
        
        mCommandsAllocated += ptr - head;
        void_assert(mCommandsAllocated < mCommandsBufferLength);
    }
    
    inline void endCommand()
    {
    }
    
    inline void doDebugPtr(byte* ptr)
    {
        mDebugPointer = ptr;
    }
    
    void sort();
    void clear();
    
    void proccessQueue();
    
    double queueFullness() const { return mQueueFullness; }
    
private:
    IGraphicsDevice* mDevice;

    byte* mCommands;
    uint64 mCommandsAllocated;
    uint64 mCommandsBufferLength;
    
    byte* mDebugPointer;
    
    double mQueueFullness;
    
};
    
}//namespace graphics
