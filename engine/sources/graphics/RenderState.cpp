//
//  RenderState.cpp
//  engine
//
//  Created by s.popov on 05/10/2017.
//
//

#include "RenderState.h"

namespace graphics
{
//------------------------------------------------------------------------------
RenderState::RenderState():
mFlags(CLEAR_FLAGS),
mScissor( { 0, 0, 0, 0 } ),
mSrcFunc(BlendFunc::One),
mDstFunc(BlendFunc::One)
{
    
}
//------------------------------------------------------------------------------
void RenderState::setScissor(uint16 x, uint16 y, uint16 w, uint16 h)
{
    mScissor.x = x;
    mScissor.y = y;
    mScissor.w = w;
    mScissor.h = h;
}
//------------------------------------------------------------------------------
void RenderState::setBlendFunc(RenderState::BlendFunc src, RenderState::BlendFunc dst)
{
    mSrcFunc = src;
    mDstFunc = dst;
}
//------------------------------------------------------------------------------
}//namespace graphics
