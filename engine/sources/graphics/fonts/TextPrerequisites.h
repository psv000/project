//
//  TextPrerequisites.h
//  engine
//
//  Created by s.popov on 06/10/2017.
//
//

#pragma once

#include <graphics/VerticesTypes.h>
#include <common/stringtypes.h>
#include <graphics/Colors.h>

namespace textures
{

class Texture;
    
}//namespace textures

namespace fonts
{

struct FontProperties
{
    string name;
    
    vec4b color;
    vec4b backgroundColor;
    vec4b outlineColor;
    
    float size;
    float outlineSize;
    float gamma;
};

}//namespace fonts
