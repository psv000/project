//
//  TextRender.cpp
//  engine
//
//  Created by s.popov on 02/10/2017.
//
//

#include "TextRender.h"

#include <font-manager.h>
#include <text-buffer.h>

#include <graphics/textures/texture_loader/TextureLoader.h>
#include <graphics/textures/Texture.h>

#include <core/IPlatformContext.h>
#include <core/Activity.h>
#include <graphics/Render.h>
#include <graphics/IGraphicsDevice.h>
#include <graphics/VerticesTypes.h>
#include <graphics/DrawHelper.h>

#include <core/file_system/FileSystem.h>

#include <common/SimpleArray.h>
#include <common/formatstring.h>

#include <logs.h>

#define FONT_TEXTURE_DEPTH LCD_FILTERING_OFF
#define MAX_RENDER_MODS 4

namespace fonts
{

namespace TextRender
{

ftgl::font_manager_t* gFontManager;
textures::Texture* gFontManagerTexture;

static const void* custom_read_data(const char* filename, size_t* length)
{
    FileSystem::File file = FileSystem::loadFile(filename, FileSystem::LocationType::Resources);
    *length = file.length();
    void* data = malloc(*length);
    file.read((char*)data, *length);
    return data;
}

//------------------------------------------------------------------------------
}//namespace TextRender
//------------------------------------------------------------------------------
bool TextRender::init()
{
    gFontManager = ftgl::font_manager_new(512, 512, FONT_TEXTURE_DEPTH);
    if (!gFontManager)
    {
        LOGE("can\'t initialize font manager");
        return false;
    }

    gFontManager->read_data_func = custom_read_data;
    
    auto device = Activity::render()->graphicsDevice();
    ftgl::texture_atlas_t* atlas = gFontManager->atlas;
    graphics::TextureID id = device->createTexture2D();
    atlas->id = id;
    
    gFontManagerTexture = textures::TextureLoader::createTexture(id,
                                                                 textures::Format::Alpha,
                                                                 textures::InternalFormat::Alpha,
                                                                 textures::Filtering::Unknown,
                                                                 vec3f(atlas->width, atlas->height, 0.f));
    
    void* data = memory::alloc(atlas->width * atlas->height * FONT_TEXTURE_DEPTH);
    memset(data, 0, atlas->width * atlas->height * FONT_TEXTURE_DEPTH);
    device->updateTexture2D(gFontManagerTexture->id(),
                            graphics::TextureLevel::L0,
                            textures::InternalFormat::Alpha,
                            gFontManagerTexture->size().x,
                            gFontManagerTexture->size().y,
                            gFontManagerTexture->size().z,
                            textures::Format::Alpha,
                            textures::DataType::UnsignedByte,
                            data);
    memory::dealloc(data);

    return true;
}
//------------------------------------------------------------------------------
void TextRender::deinit()
{
    textures::TextureLoader::releaseTexture(gFontManagerTexture);
    ftgl::font_manager_delete(gFontManager);
    gFontManager = nullptr;
}
//------------------------------------------------------------------------------
bool TextRender::renderText(const char* text,
                            const FontProperties& properties,
                            textures::Texture*& texture,
                            graphics::v3fc4bu2us*& vertices,
                            uint32*& indices,
                            uint64& verticesCount,
                            uint32& indicesCount,
                            vec2f& size)
{
    auto device = Activity::render()->graphicsDevice();
    float designScaleFactor = Activity::designScaleFactor();
    
    string filename = properties.name;
    ftgl::markup_t markup = {
        .family  = &filename[0],
        .size    = properties.size * designScaleFactor,
        .bold    = 0,
        .italic  = 0,
        .spacing = 0.0,
        .gamma   = 1.5,
        .foreground_color = {{
            properties.color.r,
            properties.color.g,
            properties.color.b,
            properties.color.a
        }},
        .background_color = {{
            properties.backgroundColor.r,
            properties.backgroundColor.g,
            properties.backgroundColor.b,
            properties.backgroundColor.a,
        }},
        .underline = 0,
        .underline_color = {{
            Color::None.r,
            Color::None.g,
            Color::None.b,
            Color::None.a,
        }},
        .overline = 0,
        .overline_color = {{
            Color::None.r,
            Color::None.g,
            Color::None.b,
            Color::None.a,
        }},
        .strikethrough = 0,
        .strikethrough_color = {{
            Color::None.r,
            Color::None.g,
            Color::None.b,
            Color::None.a,
        }},
        .font = 0,
    };
    
    StackMemArray<ftgl::rendermode_t> renderMods;
    StackMemArray<ftgl::bvec4> colors;
    StackMemArray<float> thickness;
    
    renderMods.realloc(MAX_RENDER_MODS);
    colors.realloc(MAX_RENDER_MODS);
    thickness.realloc(MAX_RENDER_MODS);
    
    if (properties.outlineSize > 0.f)
    {
        renderMods.push_back(ftgl::RENDER_OUTLINE_POSITIVE);
        colors.push_back({{
            properties.outlineColor.r,
            properties.outlineColor.g,
            properties.outlineColor.b,
            properties.outlineColor.a
        }});
        thickness.push_back(properties.outlineSize);
    }
    {
        renderMods.push_back(ftgl::RENDER_NORMAL);
        colors.push_back({{
            properties.color.r,
            properties.color.g,
            properties.color.b,
            properties.color.a
        }});
        thickness.push_back(0.f);
    }
    
    ftgl::texture_atlas_t* atlas = gFontManager->atlas;
    size_t oldUsed = atlas->used;
    ftgl::text_buffer_t* buffer = ftgl::text_buffer_new();
    
    ftgl::vec2 pen;
    for (int i = 0; i < renderMods.size(); ++i)
    {
        markup.foreground_color = colors[i];
        markup.font = font_manager_get_from_markup( gFontManager, &markup );
        
        markup.font->rendermode = renderMods[i];
        markup.font->outline_thickness = thickness[i];
        
        pen = {{0, 0}};
        ftgl::text_buffer_printf( buffer, &pen,
                                 &markup, text,
                                 NULL );
    }
    
    if (atlas->used != oldUsed)
    {
        device->updateTexture2D(gFontManagerTexture->id(),
                                graphics::TextureLevel::L0,
                                textures::InternalFormat::Alpha,
                                atlas->width, atlas->height, 0,
                                textures::Format::Alpha,
                                textures::DataType::UnsignedByte, atlas->data);
    }
    
    auto textBuffer = buffer->buffer;
    auto textVertices = textBuffer->vertices;
    auto textIndices = textBuffer->indices;
    
    {
        verticesCount = textVertices->size;
        indicesCount = (uint32)textIndices->size;
        
        vertices = (graphics::v3fc4bu2us*)memory::stack_alloc(verticesCount * sizeof(graphics::v3fc4bu2us));
        indices = (uint32*)memory::stack_alloc(indicesCount * sizeof(uint32));
        
        ftgl::vec4 bounds = ftgl::text_buffer_get_bounds( buffer, &pen );
        size = vec2f(bounds.width / designScaleFactor, bounds.height / designScaleFactor);
        
        for (int i = 0; i < verticesCount; ++i)
        {
            auto glyph = (const ftgl::glyph_vertex_t*)ftgl::vector_get(textVertices, i);
            auto& v = *(vertices + i);
            v.pos = { glyph->x, glyph->y, glyph->z };
            v.color = { glyph->r, glyph->g, glyph->b, glyph->a };
            v.uv = { glyph->u, glyph->v };
        }
        
        for (int i = 0; i < indicesCount; ++i)
        {
            *(indices + i) = (uint32)*((size_t*)ftgl::vector_get(textIndices, i));
        }
    }
    
    texture = gFontManagerTexture;
    
    ftgl::text_buffer_delete(buffer);
    return true;
}
//------------------------------------------------------------------------------
}//namespace fonts
