//
//  TextRender.h
//  engine
//
//  Created by s.popov on 02/10/2017.
//
//

#pragma once

#include "TextPrerequisites.h"

namespace textures
{
    
class Texture;
    
}//namespace textures

namespace fonts
{

namespace TextRender
{
    
bool init();
void deinit();
bool renderText(const char*,
                const FontProperties&,
                textures::Texture*&,
                graphics::v3fc4bu2us*&,
                uint32*&,
                uint64&,
                uint32&,
                vec2f&);

}//namespace TextRender
    
}//namespace fonts
