//
//  VerticesTypes.h
//  engine
//
//  Created by s.popov on 21/09/2017.
//
//

#pragma once

#include <math/vec2.h>
#include <math/vec3.h>
#include <math/vec4.h>
#include <common/inttypes.h>

namespace graphics
{

enum class VerticesType : uint8
{
    V3f = 0,
    V3f_C4u,
    V3f_C4u_U2us,
    V3f_U2us,
    V3f_U2us_C4u_S1f_G1f
};
    
struct v3f
{
    vec3f pos;      //3*4=12
};//=12
    
struct v3fc4b
{
    vec3f pos;      //3*4=12
    vec4b color;    //4*1=4
};//=16
    
struct v3fu2us
{
    vec3f pos;      //3*4=12
    vec2us uv;      //2*2=4
};//=16
    
struct v3fc4bu2us
{
    vec3f pos;      //3*4=12
    vec4b color;    //4*1=4
    vec2us uv;      //2*2=4
};//=20
    
struct v3fc4bu2uss1fg1f
{
    vec3f pos;      //3*4=12
    vec4b color;    //4*1=4
    vec2us uv;      //2*2=4
    float shift;    //4
    float gamma;    //4
};//=28
    
}//namespace graphics
