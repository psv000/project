//
//  IGraphicsDevice.h
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#pragma once

#include "RenderPrimitives.h"
#include <graphics/RenderOptions.h>
#include <graphics/RenderState.h>
#include "textures/TextureTypes.h"

namespace graphics
{
    
class Program;
class IGraphicsDevice
{
public:
    virtual ~IGraphicsDevice() {}
    
    virtual void initDevice(const RenderOptions&) = 0;
    virtual void deinitDevice() = 0;
    
    virtual void setClearColor(float, float, float, float) = 0;
    
    virtual void onResize(uint16, uint16) = 0;
    
    virtual FramebufferID createFrameBuffer() = 0;
    virtual void destroyFrameBuffer(FramebufferID) = 0;
    
    virtual VertexBufferID createVertexBuffer() = 0;
    virtual void destroyVertexBuffer(VertexBufferID) = 0;
    virtual void updateVertexBuffer(VertexBufferID, const void*, uint64) = 0;
    
    virtual IndexBufferID createIndexBuffer() = 0;
    virtual void updateIndexBuffer(IndexBufferID, BufferUsage, void*, uint64) = 0;
    virtual void destroyIndexBuffer(IndexBufferID) = 0;
    
    virtual VertexArrayObjectID createVertexArrayObject() = 0;
    virtual void updateVertexArrayObject(const MVCArray&,
                                         VertexArrayObjectID,
                                         VertexBufferID,
                                         IndexBufferID) = 0;
    
    virtual void destroyVertexArrayObject(VertexArrayObjectID) = 0;
    
    virtual Program* createProgram(const char*, const char*) = 0;
    virtual void destroyShader(Program*) = 0;
    
    virtual void createUniformContainer(ProgramID, const UEArray&, UniformContainer*) = 0;
    
    virtual void applyShader(ProgramID) = 0;
    virtual void applyUniforms(ProgramID, void*, uint8) = 0;
    
    virtual void drawMesh(VertexArrayObjectID, uint64) = 0;
    virtual void drawLine(VertexArrayObjectID, uint32, uint8, DrawMode) = 0;

    virtual void clearContext() = 0;
    
    virtual TextureID createTexture2D() = 0;
    virtual void setTexture2DAlignment(TextureID, uint8) = 0;
    virtual void setTexture2DSubImage(TextureID,
                                      TextureLevel,
                                      int16, int16,
                                      uint16, uint16,
                                      textures::Format,
                                      textures::DataType,
                                      const void*) = 0;
    virtual void updateTexture2D(TextureID,
                                 TextureLevel,
                                 textures::InternalFormat,
                                 uint16, uint16, int16,
                                 textures::Format,
                                 textures::DataType,
                                 const void*) = 0;
    virtual void destroyTexture2D(TextureID) = 0;
    
    virtual void setState(const RenderState&) = 0;
};
    
}//namespace graphics
