#pragma once

#include "inttypes.h"

#include <asserts.h>
#include <core/memory/memory.h>
//------------------------------------------------------------------------------
template<typename __type>
class BaseArray
{
public:
    BaseArray();
    virtual ~BaseArray();
    
    BaseArray(BaseArray&&);
    BaseArray&& operator=(BaseArray&&);
    
    BaseArray(const BaseArray&) = delete;
    BaseArray& operator=(const BaseArray&) = delete;

    void push_back(const __type&);
    template<typename ...Args>
    __type& emplace_back   (Args... args)
    {
        void_assert(mSize <= mCapacity);
        new((void *)(&mData[mSize])) __type(args...);
        return mData[mSize++];
    }
    void erase(const __type&);
    int64 find(const __type& v) const;
    
    bool empty() const { return !mData || !mSize; }
    
    const __type& back() const { void_assert(mData); return mData[mSize - 1]; }
    __type& back() { void_assert(mData); return mData[mSize - 1]; }

    __type* data() { return mData; }
    const __type* data() const { return mData; }
    int64 size() const { return mSize; }
    int64 capacity() const { return mCapacity; }

    const __type& operator[](int64 i) const { void_assert(mData); return mData[i]; }
    __type& operator[](int64 i) { void_assert(mData); return mData[i]; }

    const __type& at(int64 i) const { void_assert(mData); return mData[i]; }
    __type& at(int64 i) { void_assert(mData); return mData[i]; }

    virtual void realloc(int64) = 0;
    virtual void resize(int64) = 0;
    void clear();

protected:
    __type *mData;
    int64 mSize;
    int64 mCapacity;
};
//------------------------------------------------------------------------------
template<typename __type>
BaseArray<__type>::BaseArray():
    mData(nullptr),
    mSize(0),
    mCapacity(0)
{
}
//------------------------------------------------------------------------------
template<typename __type>
BaseArray<__type>::~BaseArray()
{
    BaseArray::clear();
}
//------------------------------------------------------------------------------
template<typename __type>
BaseArray<__type>::BaseArray(BaseArray<__type>&& original):
mData(original.mData),
mSize(original.mSize),
mCapacity(original.mCapacity)
{
    original.mData = nullptr;
    original.mSize = 0;
    original.mCapacity = 0;
}
//------------------------------------------------------------------------------
template<typename __type>
BaseArray<__type>&& BaseArray<__type>::operator=(BaseArray<__type>&& original)
{
    if ( this == &original )
    {
        return *this;
    }
    
    mData = original.mData;
    mSize = original.mSize;
    mCapacity = original.mCapacity;
    
    original.mData = nullptr;
    original.mSize = nullptr;
    original.mCapacity = nullptr;
    
    return *this;
}
//------------------------------------------------------------------------------
template<typename __type>
void BaseArray<__type>::push_back(const __type& v)
{
    void_assert(mData);
    void_assert(mSize < mCapacity);
    mData[mSize] = v;
    ++mSize;
}
//------------------------------------------------------------------------------
template<typename __type>
void BaseArray<__type>::erase(const __type& v)
{
    void_assert(mData);
    int64 i = 0;
    while(i < mSize && v != mData[i])
    {
        ++i;
    }
    if (i != mSize)
    {
        while(i < mSize)
        {
            mData[i].~__type();
            new((void *)(mData + i)) __type(*(mData + i + 1));
            ++i;
        }
        --mSize;
    }
}
//------------------------------------------------------------------------------
template<typename __type>
int64 BaseArray<__type>::find(const __type& v) const
{
    void_assert(mData);
    int64 i = 0;
    while(i < mSize)
    {
        if (v == mData[i])
        {
            return i;
        }
        ++i;
    }
    return -1;
}
//------------------------------------------------------------------------------
template<typename __type>
void BaseArray<__type>::clear()
{
    for (int64 i = 0; i < mSize; ++i)
    {
        mData[i].~__type();
    }
    mSize = 0;
}
//=================================================================================
template<typename __type>
class StackMemArray : public BaseArray<__type>
{
public:
    StackMemArray();
    ~StackMemArray();
    
    void realloc(int64) override;
    void resize(int64) override;
private:
};
//------------------------------------------------------------------------------
template<typename __type>
StackMemArray<__type>::StackMemArray():
BaseArray<__type>()
{
}
//------------------------------------------------------------------------------
template<typename __type>
StackMemArray<__type>::~StackMemArray()
{
    memory::dealloc(BaseArray<__type>::mData);
}
//------------------------------------------------------------------------------
template<typename __type>
void StackMemArray<__type>::realloc(int64 capacity)
{
    void_assert( !BaseArray<__type>::mData );
    BaseArray<__type>::mCapacity = capacity;
    BaseArray<__type>::mData = (__type*)memory::stack_alloc(capacity * sizeof(__type));
}
//------------------------------------------------------------------------------
template<typename __type>
void StackMemArray<__type>::resize(int64 size)
{
    BaseArray<__type>::clear();
    if (size > BaseArray<__type>::mCapacity)
    {
        StackMemArray<__type>::realloc(size);
    }
    
    for (uint64 i = 0; i < size; ++i)
    {
        BaseArray<__type>::emplace_back();
    }
}
//=================================================================================
template<typename __type>
class HeapMemArray : public BaseArray<__type>
{
public:
    HeapMemArray();
    ~HeapMemArray();
    
    void realloc(int64) override;
    void resize(int64) override;
};
//------------------------------------------------------------------------------
template<typename __type>
HeapMemArray<__type>::HeapMemArray():
BaseArray<__type>()
{
}
//------------------------------------------------------------------------------
template<typename __type>
HeapMemArray<__type>::~HeapMemArray()
{
    if(BaseArray<__type>::mData)
    {
        memory::dealloc(BaseArray<__type>::mData);
    }
}
//------------------------------------------------------------------------------
template<typename __type>
void HeapMemArray<__type>::realloc(int64 capacity)
{
    BaseArray<__type>::clear();
    memory::dealloc(BaseArray<__type>::mData);
    BaseArray<__type>::mCapacity = capacity;
    BaseArray<__type>::mData = (__type*)memory::alloc(capacity * sizeof(__type));
}
//------------------------------------------------------------------------------
template<typename __type>
void HeapMemArray<__type>::resize(int64 size)
{
    if (size > BaseArray<__type>::mCapacity)
    {
        HeapMemArray<__type>::realloc(size);
    }
    else
    {
        HeapMemArray<__type>::clear();
    }
    
    for (uint64 i = 0; i < size; ++i)
    {
        BaseArray<__type>::emplace_back();
    }
}
//------------------------------------------------------------------------------
