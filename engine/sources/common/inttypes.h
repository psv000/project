//
//  inttypes.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <limits>
#include <climits>

using int8  = signed char;
using int16 = short int;
using int32 = int;
using int64 = long long int;

using byte = unsigned char;
using uint8  = unsigned char;
using uint16 = unsigned short int;
using uint32 = unsigned int;
using uint64 = unsigned long int;

#define __GET_MIN_INT_WITHOUT_BRANCHING__(left, right) ( right ^ ( (left ^ right) & -(left < right) ) )
#define __GET_MAX_INT_WITHOUT_BRANCHING__(left, right) ( left ^ ( (left ^ right) & -(left < right) ) )
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
template<typename T>
inline T __max(const T &left, const T &right)
{
    return (left < right) ? (right) : (left);
}
//------------------------------------------------------------------------------
template<>
inline int8 __max<int8>(const int8 &left, const int8 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline int16 __max<int16>(const int16 &left, const int16 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline int32 __max<int32>(const int32 &left, const int32 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline int64 __max<int64>(const int64 &left, const int64 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint8 __max<uint8>(const uint8 &left, const uint8 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint16 __max<uint16>(const uint16 &left, const uint16 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint32 __max<uint32>(const uint32 &left, const uint32 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint64 __max<uint64>(const uint64 &left, const uint64 &right)
{
    return __GET_MAX_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
template<typename T>
inline T __min(const T &left, const T &right)
{
    return (left < right) ? (left) : (right);
}
//------------------------------------------------------------------------------
template<>
inline int8 __min<int8>(const int8 &left, const int8 &right)
{
    return __GET_MIN_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline int16 __min<int16>(const int16 &left, const int16 &right)
{
    return __GET_MIN_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline int32 __min<int32>(const int32 &left, const int32 &right)
{
    return right ^ ((left ^ right) & -(left < right));
}
//------------------------------------------------------------------------------
template<>
inline int64 __min<int64>(const int64 &left, const int64 &right)
{
    return __GET_MIN_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint8 __min<uint8>(const uint8 &left, const uint8 &right)
{
    return __GET_MIN_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint16 __min<uint16>(const uint16 &left, const uint16 &right)
{
    return __GET_MIN_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint32 __min<uint32>(const uint32 &left, const uint32 &right)
{
    return __GET_MIN_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
template<>
inline uint64 __min<uint64>(const uint64 &left, const uint64 &right)
{
    return __GET_MIN_INT_WITHOUT_BRANCHING__(left, right);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
template<typename T>
inline void __swap(T &left, T & right)
{
    T buffer = left;
    left = right;
    right = buffer;
}
//------------------------------------------------------------------------------
#define GET_MAX(l, r) __max(l, r)
#define GET_MIN(l, r) __min(l, r)
#define SWAP(l, r) __swap(l, r)
