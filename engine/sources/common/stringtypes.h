//
//  string.h
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#pragma once

#include <string.h>
#include <string>

using string = std::string;
//------------------------------------------------------------------------------
inline string trim(const string& str,
                   const string& whitespace = " \t")
{
    const auto begin = str.find_first_not_of(whitespace);
    if (begin == string::npos)
    {
        return "";
    }
    
    const auto end = str.find_last_not_of(whitespace);
    const auto range = end - begin + 1;
    
    return str.substr(begin, range);
}
//------------------------------------------------------------------------------
inline string reduce(const string& str,
                     const string& fill = " ",
                     const string& whitespace = " \t")
{
    auto result = trim(str, whitespace);
    
    auto begin = result.find_first_of(whitespace);
    while (begin != string::npos)
    {
        const auto end = result.find_first_not_of(whitespace, begin);
        const auto range = end - begin;
        
        result.replace(begin, range, fill);
        
        const auto newStart = begin + fill.length();
        begin = result.find_first_of(whitespace, newStart);
    }
    
    return result;
}
//------------------------------------------------------------------------------
