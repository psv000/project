//
//  formatstring.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include "inttypes.h"
#include "stringtypes.h"
#include <cstdio>

#define DEFAULT_STRING_BUFFER_LENGTH (256)
//------------------------------------------------------------------------------
template<typename... Args, int64 __buffer_length = DEFAULT_STRING_BUFFER_LENGTH>
string format_string(const char* f, Args... args)
{
    char string_buffer[__buffer_length + 1];
    int length = snprintf(string_buffer, __buffer_length, f, args...);
    (void) length;  

    string_buffer[__buffer_length] = '\0';
    return string(string_buffer);
}
//------------------------------------------------------------------------------
