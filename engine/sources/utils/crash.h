#pragma once

#include <common/inttypes.h>
#include <math/helpers.h>
#include <utils/memory/pointer_math.h>
#include <exception>
#include <memory>
#include <asserts.h>

namespace utils
{

namespace crash
{

bool run {false};
    
enum class Type
{
    Abort = 0,
    Terminate,
    Sigbus,
    Segv1,
    Segv2,
    Segv3
};
    
void on()   { run = true; }
void off()  { run = false; }
    
void doit(Type);

inline void abort();
inline void terminate();
inline void sigbus();
inline void segv(char pattern, uint32 length);
inline void segv(uint32*, uint32, uint32 = 0x0badbeef);
inline void segv();
    
}//namespace crash
//------------------------------------------------------------------------------
void crash::doit(crash::Type type)
{
    if (!run) return;
    
    switch(type)
    {
        case crash::Type::Abort:
            crash::abort();
            break;
        case crash::Type::Terminate:
            crash::terminate();
            break;
        case crash::Type::Sigbus:
            crash::sigbus();
            break;
        case crash::Type::Segv1:
            crash::segv(0xba, 100000);
            break;
        case crash::Type::Segv2:
            crash::segv((uint32*)0x105fe6789, 100000, 0x0badbeef);
            break;
        case crash::Type::Segv3:
            crash::segv();
            break;
        default:
            void_assert(false || !":-P");
            break;
    }
}
//------------------------------------------------------------------------------
void crash::abort()
{
    ::abort();
}
//------------------------------------------------------------------------------
void crash::terminate()
{
    std::terminate();
}
//------------------------------------------------------------------------------
void crash::sigbus()
{
    char* memptr = (char*)malloc( sizeof(char) * 1000 );
    int pad = alignof(int);
    const int value = 10;
    *(int*)memptr[pad - 1] = value;
    free(memptr);
}
//------------------------------------------------------------------------------
void crash::segv(char pattern, uint32 length)
{
    void* memptr = malloc( sizeof(char) );
    void* corrupted = memptr;
    int32 shift = math::random(-0xfff, 0xfff) + 0x1;
    if ( shift > 0)
        memory::pointer_add(corrupted, shift);
    else
        memory::pointer_sub(corrupted, -shift);
    
    memset( (char*)corrupted, pattern, sizeof(char) * length );
    free(memptr);
}
//------------------------------------------------------------------------------
void crash::segv(uint32* memptr, uint32 length, uint32 pattern)
{
    memset( memptr, pattern, sizeof(uint32) * length);
}
//------------------------------------------------------------------------------
void crash::segv()
{
    void* memptr = malloc( sizeof(char) );
    free(memptr);
    free(memptr);
}
//------------------------------------------------------------------------------

}//namespace utils
