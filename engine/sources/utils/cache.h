#pragma once

#include "hash.h"
#include "common/SimpleArray.h"

namespace utils
{
    
template<typename __type>
struct CacheInfo
{
    utils::hash::hash_t hash;
    __type* item;
    int64 ref_count{0};

    bool operator==(const CacheInfo &v) const
    {
        return v.hash == this->hash || v.item == this->item;
    }

    bool operator!=(const CacheInfo &v) const
    {
        return !(v == *this);
    }
};

template<typename __type>
using CacheCollection = HeapMemArray<CacheInfo<__type>>;

namespace cache
{

template<typename __type, int64 __capacity>
int find(const char* str, const CacheCollection<__type>& cachecollection)
{
    auto hash = utils::hash::string(str);
    CacheInfo<__type> info {hash, nullptr, 0};
    return cachecollection.find(info);
}

}//namespace cache
}//namespace utils
