//
//  range.h
//  engine
//
//  Created by s.popov on 12/04/2018.
//
//

#pragma once

#include <common/inttypes.h>

namespace utils
{
namespace range
{

void splitRange(double, double, uint32, double*&, uint32&);

}//namespace range
}//namespace utils
