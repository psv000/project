#include "hash.h"

//http://stackoverflow.com/questions/8317508/hash-function-for-a-string
#define A 54059
#define B 76963
#define FIRSTH 37

namespace utils
{
namespace hash
{

hash_t string(const char* s)
{
   uint32 h = FIRSTH;
   while (*s)
   {
     h = (h * A) ^ (s[0] * B);
     s++;
   }
   return (hash_t)h;
}

}//namespace hash
}//namespace utils
