//
//  arrayutils.h
//  engine
//
//  Created by s.popov on 25/09/2017.
//
//

#pragma once

#include <utils/memory/helpers.h>
#include <common/SimpleArray.h>

#define lengthof(a) (sizeof(array::length_of(a)))

#define foreach( container, function,  ...)\
    for (auto& item : container)\
    {\
        item->function( __VA_ARGS__ );\
    }

#define check_vector_capacity(x,y,z) \
if (x.capacity() < y + x.size())\
{\
    x.reserve(x.capacity() + z);\
}

#define copy_array(x, y) \
for (uint64 i = 0, end = x.size(); i < end; ++i)\
{\
    y.push_back(x[i]);\
}

#define check_hma_capacity(x,y,z) \
if (x.capacity() < y + x.size())\
{\
    realloc(x.capacity() + z);\
}

namespace array
{
    
template <typename T, uint64 N>
char ( &length_of( T (&array)[N] ) ) [N];

template <typename T, uint64 N>
char ( &length_of( const T (&array)[N] ) ) [N];

#define volumeof(T) ( alignof(T) + sizeof(T) )

template <typename T>
void addValue(byte*& ptr, const T& val)
{
    byte* aligned = (byte*)utils::memory::align_forward( ptr, alignof(T) );
    *(T*)aligned = val;
    ptr += sizeof(T) + aligned - ptr;
}

template <typename T>
byte* reserve(byte*& ptr)
{
    byte* aligned = (byte*)utils::memory::align_forward( ptr, alignof(T) );
    ptr += sizeof(T) + aligned - ptr;
    return aligned;
}
    
template<typename T>
T& readValue(byte*& ptr)
{
    byte* aligned = (byte*)utils::memory::align_forward( ptr, alignof(T) );
    T& val = *(T*)aligned;
    ptr += sizeof(T) + aligned - ptr;
    return val;
}
    
template<typename T>
void addArray(byte*& ptr, T*& data, uint64 size)
{
    auto bytelength = size * sizeof(T);
    memcpy(ptr, data, bytelength);
    ptr += bytelength;
}
    
template<typename T>
uint64 readArray(byte*& ptr, T*& data, uint64 size)
{
    data = (T*)ptr;
    auto bytelength = size * sizeof(T);
    ptr += bytelength;
    return bytelength;
}

}//namespace array
