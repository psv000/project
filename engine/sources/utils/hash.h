#pragma once

#include "common/inttypes.h"

namespace utils
{
namespace hash
{

using hash_t = int64;
static hash_t invalid_hash{-1};
hash_t string(const char* s);

}//namespace hash
}//namespace utils
