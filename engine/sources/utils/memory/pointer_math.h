//
//  pointer_math.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <common/inttypes.h>

namespace utils
{
namespace memory
{

template<typename __type = char>
void *pointer_add(void *p, uint64 length)
{
    return (void*)((__type *)p + length);
}

template<typename __type = char>
const void *pointer_add(const void *p, uint64 length)
{
    return (const void*)((const __type *)p + length);
}

template<typename __type = char>
void *pointer_sub(void *p, uint64 length)
{
    return (void*)((__type *)p - length);
}

template<typename __type = char>
const void *pointer_sub(const void *p, uint64 length)
{
    return (const void*)((const __type *)p - length);
}

}//namespace memory
}//namespace utils
