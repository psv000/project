//
//  helpers.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <common/inttypes.h>
#include <core/memory/header.h>

namespace utils
{
namespace memory
{

namespace
{

const int64 HEADER_PAD_VALUE = 0xffffffffffffffffu;
using uptr = int64;

}//static

inline void* align_forward(const void* address, uint8 alignment)
{
    void* aligned = (void*)( ( reinterpret_cast<uptr>(address) + static_cast<uptr>(alignment - 1) )
                             & static_cast<uptr>( ~(alignment - 1) ) );
    return aligned;
}

inline uint8 align_forward_adjustment(const void* address, uint8 alignment)
{
    uint8 adjustment =  alignment - ( reinterpret_cast<uptr>(address) & static_cast<uptr>(alignment - 1) );
    if(adjustment == alignment)
    {
        return 0;
    }

    return adjustment;
}

inline uint8 align_forward_adjustment_with_header(const void* address, uint8 alignment, uint8 headerSize)
{
    uint8 adjustment = align_forward_adjustment(address, alignment);
    uint8 neededSpace = headerSize;
    if(adjustment < neededSpace)
    {
        neededSpace -= adjustment;

        adjustment += alignment * (neededSpace / alignment);
        if(neededSpace % alignment > 0)
        {
            adjustment += alignment;
        }
    }

    return adjustment;
}

inline void *data_pointer(::memory::header_t *header, uint8 align)
{
    void *ptr = header + 1;
    return align_forward(ptr, align);
}

inline ::memory::header_t *header(void *data)
{
    int64 *ptr = (int64 *)data;
    while (ptr[-1] == HEADER_PAD_VALUE)
    {
        --ptr;
    }
    return (::memory::header_t *)ptr - 1;
}

inline int64 size_with_padding(int64 size, uint8 alignment)
{
    return size + alignment + sizeof(::memory::header_t);
}

inline void fill(::memory::header_t *header, void *data, int64 size)
{
    header->length = size;
    int64 *ptr = (int64 *)(header + 1);
    while (ptr < data)
    {
        *ptr++ = HEADER_PAD_VALUE;
    }
}

}//namespace memory
}//namespace utils
