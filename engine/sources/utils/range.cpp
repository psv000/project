//
//  range.cpp
//  engine
//
//  Created by s.popov on 12/04/2018.
//

#include "range.h"
#include "arrayutils.h"
#include <math/helpers.h>
#include <core/memory/memory.h>
#include <memory.h>
#include <cmath>
#include <asserts.h>

namespace utils
{
namespace range
{

const double e_sqrt[] = {
    sqrt(50.),
    sqrt(10.),
    sqrt(2.)
};
    
const double e_val[] = {
    10.,
    5.,
    2.,
    1.
};

double tickIncrement(double first, double last, uint32 count);

}//namespace range
}//namespace utils
//------------------------------------------------------------------------------
double utils::range::tickIncrement(double first, double last, uint32 count)
{
    double step = ( last - first ) / GET_MAX((double)count, 0.);
    double power = math::trunc(log(step) / log(10.));
    double error = step / pow(10., power);
    
    int i = 0, end = lengthof(e_sqrt);
    while(i < end && error < e_sqrt[i])
    {
        i++;
    }
    double eval = e_val[i];
    double result = (power >= 0.) ? eval * pow(10., power) : -pow(10., -power) / eval;
    return result;
}
//------------------------------------------------------------------------------
void utils::range::splitRange(double first, double last, uint32 count, double*& array, uint32& length)
{
    if (count <= 0 || (last - first) <= 0.f)
    {
        length = 0;
        return;
    }
    if ( first == last && count > 0 )
    {
        length = count;
        array = (double*)::memory::alloc( length * sizeof(double) );
        memset(array, first, length * sizeof(double));
        return;
    }
    
    double start = GET_MIN(first, last);
    double stop = GET_MAX(first, last);
    double step = tickIncrement(start, stop, count);
    
    if (step == 0. || isinf(step))
    {
        length = 0;
        return;
    }
    
    if (step > 0.)
    {
        start = math::ceil(start / step);
        stop = math::trunc(stop / step);
        length = math::ceil(stop - start + 1);
        array = (double*)::memory::alloc( length * sizeof(double) );
        for (uint32 i = 0; i < length; ++i)
        {
            array[i] = (start + i) * step;
        }
    }
    else
    {
        start = math::trunc(start * step);
        stop = math::ceil(stop * step);
        length = math::ceil(start - stop + 1);
        array = (double*)::memory::alloc( length * sizeof(double) );
        for (uint32 i = 0; i < length; ++i)
        {
            array[i] = (start - i) / step;
        }
    }
}
//------------------------------------------------------------------------------
