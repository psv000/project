//
//  vec-helpers.h
//  engine
//
//  Created by s.popov on 29/09/2017.
//
//

#pragma once

#include "vec2.h"
#include "vec3.h"
#include "vec4.h"

namespace math
{
//------------------------------------------------------------------------------
template<typename T, glm::precision P = glm::precision::defaultp, template <typename, glm::precision> class vec_type>
T length(const vec_type<T, P>& v)
{
    return glm::length(v);
}
//------------------------------------------------------------------------------
template<typename T, glm::precision P = glm::precision::defaultp, template <typename, glm::precision> class vec_type>
T distance(const vec_type<T, P>& l, const vec_type<T, P>& r)
{
    return glm::distance(l, r);
}
//------------------------------------------------------------------------------
template<typename T>
T clamp(const T& current, const T& min, const T& max)
{
    return glm::clamp(current, min, max);
}
//------------------------------------------------------------------------------
template<typename T>
T normalize(const T& vec)
{
    return glm::normalize(vec);
}
//------------------------------------------------------------------------------
}//namespace math_helpers
