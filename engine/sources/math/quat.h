//
//  quat.h
//  engine
//
//  Created by s.popov on 13/12/2017.
//
//

#pragma once

#include <glm/gtc/quaternion.hpp>

using quat = glm::tquat<float, glm::precision::defaultp>;
