//
//  vec4.h
//  engine
//
//  Created by s.popov on 13/09/2017.
//
//

#pragma once

#include <glm/glm.hpp>
#include <common/inttypes.h>

template<typename T, glm::precision P = glm::precision::defaultp>
using vec4 = glm::tvec4<T, P>;

using vec4f = glm::tvec4<float>;
using vec4u = glm::tvec4<uint32>;
using vec4b = glm::tvec4<byte>;
using vec4i = glm::tvec4<int>;
