//
//  mat4.h
//  engine
//
//  Created by s.popov on 18/09/2017.
//
//

#pragma once

#include <glm/glm.hpp>

using mat4f = glm::tmat4x4<float, glm::precision::highp>;
