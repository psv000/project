//
//  type_trect.h
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#pragma once

#include <math/vec2.h>
#include <math/vec3.h>
#include <math/vec4.h>
#include <memory.h>

namespace math
{

template<typename __type>
struct trect
{
    vec2<__type> origin;
    vec2<__type> size;
    
    trect();
    trect(const trect<__type>&);
    trect& operator=(const trect<__type>&);
    
    trect(const vec2<__type>&, const vec2<__type>&);
    
    trect(__type);
    trect(__type, __type, __type, __type);
    
    bool contain(const vec2<__type>&) const;
    trect<__type> intersection(const trect<__type>&) const;
    bool empty() const;
};
//------------------------------------------------------------------------------
template<typename __type>
trect<__type>::trect():
origin((__type)0),
size((__type)0)
{

}
//------------------------------------------------------------------------------
template<typename __type>
trect<__type>::trect(const trect<__type>& rect):
origin(rect.origin),
size(rect.size)
{

}
//------------------------------------------------------------------------------
template<typename __type>
trect<__type>& trect<__type>::operator=(const trect<__type>& rect)
{
    origin = rect.origin;
    size = rect.size;
    return *this;
}
//------------------------------------------------------------------------------
template<typename __type>
trect<__type>::trect(const vec2<__type>& origin, const vec2<__type>& size):
origin(origin),
size(size)
{
    
}
//------------------------------------------------------------------------------
template<typename __type>
trect<__type>::trect(__type x, __type y, __type z, __type w):
origin(x, y),
size(z, w)
{
    
}
//------------------------------------------------------------------------------
template<typename __type>
trect<__type>::trect(__type v):
origin(v),
size(v)
{
    
}
//------------------------------------------------------------------------------
template<typename __type>
bool trect<__type>::contain(const vec2<__type>& p) const
{
    return origin.x <= p.x && p.x <= ( origin.x + size.x )
    && origin.y <= p.y && p.y <= ( size.y + origin.y );
}
//------------------------------------------------------------------------------
template<typename __type>
trect<__type> trect<__type>::intersection(const trect<__type>& rect) const
{
    if (    (origin.x + size.x) < rect.origin.x
        ||  (rect.origin.x + rect.size.x) < origin.x
        ||  (origin.y + size.y) < rect.origin.y
        ||  (rect.origin.y + rect.size.y) < origin.y )
    {
        return trect<__type>((__type)0);
    }
    else
    {
        float x = GET_MAX(origin.x, rect.origin.x);
        float y = GET_MAX(origin.y, rect.origin.y);
        float w = GET_MIN(origin.x + size.x, rect.origin.x + rect.size.x);
        float h = GET_MIN(origin.y + size.y, rect.origin.y + rect.size.y);
        return trect<__type>(x, y, w - x, h - y);
    }
}
//------------------------------------------------------------------------------
template<typename __type>
bool trect<__type>::empty() const
{
    return size.x == 0.f && size.y == 0;
}
//------------------------------------------------------------------------------
}//math
