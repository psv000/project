//
//  vec2.h
//  engine
//
//  Created by s.popov on 13/09/2017.
//
//

#pragma once

#include <glm/glm.hpp>
#include <common/inttypes.h>

template<typename T, glm::precision P = glm::precision::defaultp>
using vec2 = glm::tvec2<T, P>;

using vec2f = glm::tvec2<float>;
using vec2us = glm::tvec2<uint16>;
using vec2u = glm::tvec2<uint32>;
using vec2i = glm::tvec2<int>;
