//
//  math.h
//  engine
//
//  Created by s.popov on 25/09/2017.
//
//

#pragma once

#include <common/inttypes.h>
#include <cstdlib>
#include <cmath>

namespace math
{
//------------------------------------------------------------------------------
inline uint32 round2pow(uint32 v)
{
    v--;
    v |= v >> 1;
    v |= v >> 2;
    v |= v >> 4;
    v |= v >> 8;
    v |= v >> 16;
    v++;
    return v;
}
//------------------------------------------------------------------------------
inline float frandom(float s, float i)
{
    return s + static_cast <float> ( rand() ) /( static_cast <float> ( RAND_MAX / ( i - s ) ) );
}
//------------------------------------------------------------------------------
inline int random(int s, int i)
{
    return s + rand() % i;
}
//------------------------------------------------------------------------------
inline double round(double val)
{
    return (int64)(val + 0.5);
}
//------------------------------------------------------------------------------
inline double ceil(double val)
{
    return (int64)(val + 1. * (val > 0.));
}
//------------------------------------------------------------------------------
inline double trunc(double val)
{
    return (int64)val - (val < 0.);
}
//------------------------------------------------------------------------------
inline float round(float val)
{
    return (int)(val + 0.5f);
}
//------------------------------------------------------------------------------
inline float ceil(float val)
{
    return (int64)val - (val < 0.f);
}
//------------------------------------------------------------------------------
inline float trunc(float val)
{
    return (int)val;
}
//------------------------------------------------------------------------------
inline float roundull(float val)
{
    return (uint64)(val + 0.5f);
}
//------------------------------------------------------------------------------
inline float ceilull(float val)
{
    return (uint64)(val + 1.f);
}
//------------------------------------------------------------------------------
inline float fclamp(float current, float min, float max)
{
    return GET_MAX(GET_MIN(current, max), min);
}
//------------------------------------------------------------------------------
template<typename T>
inline T sign(T val)
{
    return ( T(0) < val ) - ( val < T(0) );
}
//------------------------------------------------------------------------------
}//namespace math
