//
//  rect.h
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#pragma once

#include "details/type_rect.h"

using rectf = math::trect<float>;
using rectui = math::trect<unsigned int>;
using recti = math::trect<int>;
