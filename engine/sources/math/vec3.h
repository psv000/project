//
//  vec3.h
//  engine
//
//  Created by s.popov on 13/09/2017.
//
//

#pragma once

#include <glm/glm.hpp>

template<typename T, glm::precision P = glm::precision::defaultp>
using vec3 = glm::tvec3<T, P>;

using vec3f = glm::tvec3<float>;
using vec3u = glm::tvec3<unsigned int>;
using vec3i = glm::tvec3<int>;
