//
//  GestureNotifier.cpp
//  engine
//
//  Created by s.popov on 28/09/2017.
//
//

#include "GestureNotifier.h"
#include "IGestureObserver.h"
#include <core/Activity.h>

#include <math/vec-helpers.h>

#include <asserts.h>
#include <logs.h>

namespace input
{
//------------------------------------------------------------------------------
GestureNotifier::GestureNotifier()
{

}
//------------------------------------------------------------------------------
GestureNotifier::~GestureNotifier()
{

}
//------------------------------------------------------------------------------
void GestureNotifier::handleGesture(const Gesture& gesture)
{
    switch (gesture.type) {
        case GestureType::Tap:
            GestureNotifier::notifyAboutTapGesture(gesture);
            break;
        case GestureType::DoubleTap:
            GestureNotifier::notifyAboutTapGesture(gesture);
            break;
        case GestureType::LongPress:
            GestureNotifier::notifyAboutTapGesture(gesture);
            break;
        case GestureType::SingleTouchPinch:
        case GestureType::Pinch:
            GestureNotifier::notifyAboutPinchGesture( static_cast<const PinchGesture&>(gesture) );
            break;
        case GestureType::Pan:
            GestureNotifier::notifyAboutPanGesture( static_cast<const PanGesture&>(gesture) );
            break;
        default:
            msg_assert(false, "it\'s unknown gesture type");
            break;
    }
}
//------------------------------------------------------------------------------
void GestureNotifier::registerGestureObserver(IGestureObserver* observer)
{
    if (hasGestureObserver(observer))
    {
        msg_assert(false, "it\'s already observe gestures");
        return;
    }
    mObservers.push_back(observer);
}
//------------------------------------------------------------------------------
void GestureNotifier::unregisterGestureObserver(IGestureObserver* observer)
{
    auto it = std::find(mObservers.begin(), mObservers.end(), observer);
    if (it != mObservers.end())
    {
        mObservers.erase(it);
    }
}
//------------------------------------------------------------------------------
bool GestureNotifier::hasGestureObserver(IGestureObserver* observer)
{
    for (const auto* ptr : mObservers)
    {
        if (ptr == observer)
        {
            return true;
        }
    }
    return false;
}
//------------------------------------------------------------------------------
void GestureNotifier::notifyAboutTapGesture(const Gesture& gesture)
{
    for (auto observer : mObservers)
    {
        observer->onTapEvent(gesture);
    }
}
//------------------------------------------------------------------------------
void GestureNotifier::notifyAboutPanGesture(const PanGesture& gesture)
{
    for (auto observer : mObservers)
    {
        observer->onPanEvent(gesture);
    }
}
//------------------------------------------------------------------------------
void GestureNotifier::notifyAboutPinchGesture(const PinchGesture& gesture)
{
    for (auto observer : mObservers)
    {
        observer->onPinchEvent(gesture);
    }
}
//------------------------------------------------------------------------------
}//namespace input
