//
//  IGestureObserver.h
//  engine
//
//  Created by s.popov on 28/09/2017.
//
//

#pragma once

#include "TouchEventTypes.h"

namespace input
{
    
class IGestureObserver
{
public:
    ~IGestureObserver() {}
    
    virtual void onTapEvent(const Gesture&) = 0;
    virtual void onPinchEvent(const PinchGesture&) = 0;
    virtual void onPanEvent(const PanGesture&) = 0;
};
    
}//namespace input
