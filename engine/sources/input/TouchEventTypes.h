//
//  InputEvent.h
//  engine
//
//  Created by s.popov on 28/09/2017.
//
//

#pragma once

#include <common/inttypes.h>
#include <math/vec2.h>

namespace input
{

enum class EventType : uint8
{
    Began = 0,
    Progress,
    Ended,
    Unknown
};
    
enum class GestureType : uint8
{
    Touch = 0,
    Untouch,
    Tap,
    DoubleTap,
    LongPress,
    Pinch,
    SingleTouchPinch,
    Pan,
    Unknown
};
    
struct Gesture
{
    GestureType type;
    EventType event;
    vec2f position;
    double timestamp;
};
    
struct PinchGesture : Gesture
{
    float pinch;
};
    
struct PanGesture : Gesture
{
    vec2f translation;
    vec2f velocity;
    vec2f lastPosition;
};
    
}//namespace input
