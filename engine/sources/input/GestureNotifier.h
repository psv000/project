//
//  GestureNotifier.h
//  engine
//
//  Created by s.popov on 28/09/2017.
//
//

#pragma once

#include "TouchEventTypes.h"
#include <vector>

namespace input
{
    
class IGestureObserver;
class GestureNotifier final
{
public:
    GestureNotifier();
    ~GestureNotifier();
    
    GestureNotifier(const GestureNotifier&) = delete;
    GestureNotifier& operator=(const GestureNotifier&) = delete;
    
    void handleGesture(const Gesture&);
    
    void registerGestureObserver(IGestureObserver*);
    void unregisterGestureObserver(IGestureObserver*);
    
private:
    bool hasGestureObserver(IGestureObserver*);
    void notifyAboutPinchGesture(const PinchGesture&);
    void notifyAboutPanGesture(const PanGesture&);
    void notifyAboutTapGesture(const Gesture&);

private:
    using GestureObservers = std::vector<IGestureObserver*>;
    GestureObservers mObservers;
};
    
}//namespace input
