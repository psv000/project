//
// Created by s.popov on 18/10/2017.
//

#include <core/Activity.h>
#include "AndroidFile.h"
#include <platform/android/IAndroidPlatformContext.h>
#include <android/asset_manager.h>

#define AASSET_SEEK_FROM_BEGIN  (0)
#define AASSET_SEEK_FROM_CUR    (1)
#define AASSET_SEEK_FROM_END    (2)

namespace FileSystem
{
//------------------------------------------------------------------------------
File::File(const char* filename, AccessMod mod):
mAsset(nullptr),
mName(filename),
mExtension(parseExtension(filename))
{
    AAssetManager* manager = ((IAndroidPlatformContext*)Activity::context())->assetManager();
    mAsset = AAssetManager_open(manager, filename, AASSET_MODE_RANDOM);
}
//------------------------------------------------------------------------------
File::~File()
{
    release();
}
//------------------------------------------------------------------------------
File::File(File &&file)
{
    if (&file == this)
    {
        return;
    }

    mAsset = file.mAsset;
    mName = std::move(file.mName);
    mExtension = file.mExtension;

    file.mAsset = nullptr;
    file.mExtension = Extension::Unknown;
}
//------------------------------------------------------------------------------
File& File::operator=(File &&file)
{
    if (&file == this)
    {
        return *this;
    }

    mAsset = file.mAsset;
    mName = std::move(file.mName);
    mExtension = file.mExtension;

    file.mAsset = nullptr;
    file.mExtension = Extension::Unknown;

    return *this;
}
//------------------------------------------------------------------------------
const char* File::name() const
{
    return mName.c_str();
}
//------------------------------------------------------------------------------
Extension File::extension() const
{
    return mExtension;
}
//------------------------------------------------------------------------------
void File::release()
{
    if (!mAsset) { return; }
    AAsset_close(mAsset);
}
//------------------------------------------------------------------------------
uint64 File::read(char *buffer, uint64 size)
{
    return AAsset_read(mAsset, buffer, size);
}
//------------------------------------------------------------------------------
uint64 File::seek(uint64 offset)
{
    return AAsset_seek(mAsset, offset, AASSET_SEEK_FROM_BEGIN);
}
//------------------------------------------------------------------------------
uint64 File::rseek(uint64 offset)
{
    return AAsset_seek(mAsset, offset, AASSET_SEEK_FROM_END);
}
//------------------------------------------------------------------------------
uint64 File::seekd(uint64 delta)
{
    return AAsset_seek(mAsset, delta, AASSET_SEEK_FROM_CUR);
}
//------------------------------------------------------------------------------
uint64 File::rseekd(uint64 delta)
{
    return AAsset_seek(mAsset, -delta, AASSET_SEEK_FROM_CUR);
}
//------------------------------------------------------------------------------
uint64 File::offset() const
{
    return length() - AAsset_getRemainingLength64(mAsset);
}
//------------------------------------------------------------------------------
uint64 File::length() const
{
    return AAsset_getLength64(mAsset);
}
//------------------------------------------------------------------------------
bool File::empty() const
{
    return !mAsset;
}
//------------------------------------------------------------------------------
bool File::end() const
{
    return offset() >= length();
}
//------------------------------------------------------------------------------
}//namespace FileSystem
