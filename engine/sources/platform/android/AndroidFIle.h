//
// Created by s.popov on 18/10/2017.
//

#pragma once

#include <core/file_system/FileTypes.h>
#include <common/stringtypes.h>

class AAsset;
namespace FileSystem
{

class File
{
public:
    File(const char *, AccessMod);
    ~File();

    File(const File &) = delete;
    File &operator=(const File &) = delete;

    File(File &&);
    File& operator=(File &&);

    const char* name() const;
    Extension extension() const;

    void release();

    uint64 read(char *buffer, uint64 size);

    uint64 seek(uint64 offset);
    uint64 rseek(uint64 offset);
    uint64 seekd(uint64 offset);
    uint64 rseekd(uint64 offset);

    uint64 offset() const;
    uint64 length() const;

    bool empty() const;
    bool end() const;

private:
    AAsset* mAsset;
    string mName;
    Extension mExtension;
};


}//namespace FileSystem
