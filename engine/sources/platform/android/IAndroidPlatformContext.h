//
// Created by Никита Головин on 13.02.18.
//

#pragma once

#include <android/asset_manager.h>
#include <core/IPlatformContext.h>

class IAndroidPlatformContext : public IPlatformContext
{
public:
    virtual AAssetManager* assetManager() = 0;
};
