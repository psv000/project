//
//  View.mm
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#include "DefaultView.h"
#include <core/Activity.h>
#include "IOSRenderSurface.h"

#include <asserts.h>
#include <logs.h>

@implementation DefaultView

static DefaultView* glInstance = nil;
EAGLContext* glContext = nil;
graphics::IOSRenderSurface* glRenderSurface = nil;

static double gLastTime = CFAbsoluteTimeGetCurrent();

+(Class)layerClass
{
    return [CAEAGLLayer class];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self)
    {
        CAEAGLLayer* eaglLayer = (CAEAGLLayer *)self.layer;
        eaglLayer.opaque = TRUE;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:TRUE], kEAGLDrawablePropertyRetainedBacking,
                                        kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
    }
    
    glInstance = self;
    return self;
}

+(DefaultView*)get
{
    return glInstance;
}

-(void) layoutSubviews {
    [super layoutSubviews];
    
    CGSize size;
    size.width = self.frame.size.width;
    size.height = self.frame.size.height;
    [self resize: size];
}

-(CGSize) size {
    return self.frame.size;
}

-(BOOL) initContext:(float) scaleFactor {
    glContext = [[EAGLContext alloc] initWithAPI:(kEAGLRenderingAPIOpenGLES3)];
    if (!glContext)
    {
        LOGE("Failed to create ES context");
        return NO;
    }
    else if (![EAGLContext setCurrentContext:glContext])
    {
        LOGE("Failed to set ES context current");
        return NO;
    }

    mDisplayLink = nil;
    
    glRenderSurface = new graphics::IOSRenderSurface();
    auto renderBufferStorage = [self]()
    {
        [glContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    };
    
    int width = self.frame.size.width;
    int height = self.frame.size.height;
    glRenderSurface->init(renderBufferStorage, width, height);
    
    self.contentScaleFactor = scaleFactor;
    
    return YES;
}

-(void) dealloc {
    [self invalidate];
}

-(void) invalidate {
    if (glInstance == nil)
        return;
    
    glContext = nil;
    glInstance = nil;
    
    glRenderSurface->deinit();
    delete glRenderSurface;
}

-(graphics::IRenderSurface*) surface {
    return glRenderSurface;
}

#pragma mark - GLFrameBuffer -

-(void) resize:(CGSize) size
{
    [glContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)self.layer];
    glRenderSurface->onResize(size.width, size.height);
}

-(void)applyFrameBuffer
{
    if (glContext && [EAGLContext setCurrentContext:glContext])
    {
        glRenderSurface->apply();
    }
    else
    {
        LOGE("Can\'t set Frame Buffer");
    }
}

-(void) swapBuffers
{
    [EAGLContext setCurrentContext:glContext];
    glRenderSurface->present();
    [glContext presentRenderbuffer:GL_RENDERBUFFER];
}

#pragma mark - GLView Life Cycle -

-(void) startUpdate {
    mDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(update)];
    [mDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

-(void) stopUpdate {
    [mDisplayLink invalidate];
    mDisplayLink = nil;
}

-(void) update {
    double time = CFAbsoluteTimeGetCurrent();
    const float dt = (float)(time - gLastTime);
    
    Activity::update(dt);
    
    gLastTime = time;
    [self draw];
}

- (void)draw
{
    [self applyFrameBuffer];
    Activity::draw();
    [self swapBuffers];
}

@end
