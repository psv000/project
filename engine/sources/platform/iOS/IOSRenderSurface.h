//
//  IOSRenderSurface.h
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#pragma once

#include <graphics/IRenderSurface.h>
#include <graphics/RenderPrimitives.h>
#include <graphics/GLES/GLESPrerequisites.h>
#include <math/vec2.h>

#include <functional>

namespace graphics
{

using RenderBufferStorageGET = std::function<void()>;
    
class IOSRenderSurface final : public IRenderSurface
{
public:
    IOSRenderSurface();
    
    void init(const RenderBufferStorageGET&, uint16, uint16);
    void deinit();
    
    vec2u size() const override { return mSurfaceSize; }
    
    void onResize(uint16, uint16) override;
    void apply() override;
    void present() override;
    
    void MSAAon(uint8 samplesCount) override;
    void MSAAoff() override;
    
private:
    vec2i mSurfaceSize;
    int mSamplesCount;
    
    bool mMSAA;
    
    FramebufferID mFrameBuffer;
    FramebufferID mColorBuffer;
    
    FramebufferID mMSAAColorBuffer;
    FramebufferID mMSAAFrameBuffer;
    
    RenderBufferStorageGET mRenderBufferStorage;
};

}//namespace graphics
