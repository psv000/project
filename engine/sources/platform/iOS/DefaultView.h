//
//  DefaultView.h
//  engine
//
//  Created by s.popov on 15/09/2017.
//
//

#pragma once

#import <UIKit/UIKit.h>

#include <graphics/IRenderSurface.h>

@interface DefaultView : UIView
{
@private
    CADisplayLink* mDisplayLink;
}
-(BOOL) initContext:(float) scaleFactor;
-(void) startUpdate;
-(void) stopUpdate;
-(void) invalidate;
+(DefaultView*) get;

-(void) resize:(CGSize) size;
-(CGSize) size;

-(graphics::IRenderSurface*) surface;

@end
