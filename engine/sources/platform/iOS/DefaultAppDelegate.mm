//
//  DefaultAppDelegate.mm
//  engine
//
//  Created by s.popov on 13/09/2017.
//
//

#import "DefaultAppDelegate.h"
#import "DefaultViewController.h"
#import "DefaultView.h"

@implementation DefaultAppDelegate

@synthesize window;
@synthesize viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UIApplication sharedApplication].statusBarHidden = YES;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window makeKeyAndVisible];
    
    self.window.multipleTouchEnabled = YES;
    self.window.exclusiveTouch = YES;
    
    viewController = [self createViewController];
    viewController.view = [viewController createView];
    
    [self.window setRootViewController:viewController];
    [self.window addSubview:viewController.view];
    [viewController viewDidCreate];
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [viewController viewDidDestroy];
    viewController = nil;
}

#pragma mark - app interface -

-(DefaultViewController*) createViewController
{
    return [[DefaultViewController alloc] init];
}

@end
