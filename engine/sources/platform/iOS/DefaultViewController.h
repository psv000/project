//
//  ViewController.h
//  engine
//
//  Created by s.popov on 13/09/2017.
//
//

#pragma once

#import <UIKit/UIKit.h>

@class DefaultView;

@interface DefaultViewController : UIViewController

-(DefaultView*) createView;

-(void)viewDidCreate;
-(void)viewDidDestroy;

-(void)onEngineInit;
-(void)onEngineShutdown;

@end
