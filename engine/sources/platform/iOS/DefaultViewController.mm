//
//  ViewController.mm
//  engine
//
//  Created by s.popov on 13/09/2017.
//
//

#import "DefaultViewController.h"
#import "DefaultView.h"

#include "IOSContext.h"
#include <core/Activity.h>
#include <core/file_system/FileSystem.h>
#include <core/timesource.h>
#include <input/GestureNotifier.h>
#include <input/TouchEventTypes.h>
#include <asserts.h>

static DefaultViewController* sViewController = nullptr;
static DefaultViewController* getViewController() { return sViewController; }
static float scale();
static float ppi();

@interface DefaultViewController ()

@property (strong, nonatomic) EAGLContext *context;
@property IOSContext* iOSPlatformContext;

@property (strong, nonatomic) UITapGestureRecognizer*       gTapGestureRecognizer;
@property (strong, nonatomic) UITapGestureRecognizer*       gDoubleTapGestureRecognizer;
@property (strong, nonatomic) UILongPressGestureRecognizer* gLongPressGestureRecognizer;
@property (strong, nonatomic) UIPinchGestureRecognizer*     gPinchGestureRecognizer;
@property (strong, nonatomic) UIPanGestureRecognizer*       gPanGestureRecognizer;

@property (nonatomic) CGPoint gLastPanPoint;
@property (nonatomic) CGFloat gLastPinchValue;
@property (nonatomic) NSInteger gLastPanTouchesCount;

@end

@implementation DefaultViewController

//------------------------------------------------------------------------------
- (void)viewDidCreate {
    sViewController = self;
    
    _iOSPlatformContext = new IOSContext();
    _iOSPlatformContext->setScaleGetter(scale);
    _iOSPlatformContext->setPPIGetter(ppi);
    
    BOOL res = [(DefaultView*)self.view initContext: scale()];
    (void) res;
    assert(res);
    self.view.multipleTouchEnabled = YES;
    self.view.exclusiveTouch = YES;
    
    [self initializeTapRecognizer];
    [self initializeDoubleTapRecognizer];
    [self initializeLongTapRecognizer];
    [self initializePinchRecognizer];
    [self initializePanRecognizer];
    
    [self onEngineInit];
}
//------------------------------------------------------------------------------
-(void) initializeTapRecognizer {
    _gTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [_gTapGestureRecognizer setNumberOfTapsRequired: 1];
    [_gTapGestureRecognizer setNumberOfTouchesRequired: 1];
    [self.view addGestureRecognizer: _gTapGestureRecognizer];
}
//------------------------------------------------------------------------------
-(void) initializeDoubleTapRecognizer {
    _gDoubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    [_gTapGestureRecognizer setNumberOfTapsRequired: 1];
    [_gTapGestureRecognizer setNumberOfTouchesRequired: 1];
    [self.view addGestureRecognizer: _gDoubleTapGestureRecognizer];
}
//------------------------------------------------------------------------------
-(void) initializeLongTapRecognizer {
    _gLongPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [self.view addGestureRecognizer: _gLongPressGestureRecognizer];
}
//------------------------------------------------------------------------------
-(void) initializePinchRecognizer {
    _gPinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [_gPinchGestureRecognizer setScale: 1.f];
    [self.view addGestureRecognizer: _gPinchGestureRecognizer];
}
//------------------------------------------------------------------------------
-(void) initializePanRecognizer {
    _gPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [_gPanGestureRecognizer setMinimumNumberOfTouches: 2];
    [_gPanGestureRecognizer setMaximumNumberOfTouches: 3];
    [self.view addGestureRecognizer: _gPanGestureRecognizer];
}
//------------------------------------------------------------------------------
- (void)viewDidDestroy {
    
    [self.view removeGestureRecognizer: _gTapGestureRecognizer];
    [self.view removeGestureRecognizer: _gDoubleTapGestureRecognizer];
    [self.view removeGestureRecognizer: _gLongPressGestureRecognizer];
    [self.view removeGestureRecognizer: _gPinchGestureRecognizer];
    [self.view removeGestureRecognizer: _gPanGestureRecognizer];
    
    _gTapGestureRecognizer       = nil;
    _gDoubleTapGestureRecognizer = nil;
    _gLongPressGestureRecognizer = nil;
    _gPinchGestureRecognizer     = nil;
    _gPanGestureRecognizer       = nil;
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
    _context = nil;
    delete _iOSPlatformContext;
    [self onEngineShutdown];
    sViewController = nullptr;
}
//------------------------------------------------------------------------------
- (BOOL)shouldAutorotate {
    return YES;
}
//------------------------------------------------------------------------------
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}
//------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    Activity::didRecieveMemoryWarning();
}
//------------------------------------------------------------------------------
- (BOOL)prefersStatusBarHidden {
    return YES;
}
//------------------------------------------------------------------------------
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    // Code here will execute before the rotation begins.
    // Equivalent to placing it in the deprecated method -[willRotateToInterfaceOrientation:duration:]
    
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Place code here to perform animations during the rotation.
        // You can pass nil or leave this block empty if not necessary.
        
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        // Code here will execute after the rotation has finished.
        // Equivalent to placing it in the deprecated method -[didRotateFromInterfaceOrientation:]

        [(DefaultView*)self.view resize: size];
    }];
}

#pragma mark - app interface -
//------------------------------------------------------------------------------
- (DefaultView*) createView {
    return [[DefaultView alloc] initWithFrame:[UIScreen mainScreen].bounds];
}
//------------------------------------------------------------------------------
-(void)onEngineInit {
    
    FileSystem::Locations locations;
    NSString* appDirPath = [NSString stringWithFormat: @"%@/", [[NSBundle mainBundle] resourcePath]];
    NSString* dataDirPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"data"];
    NSString* docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* cacheDirPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString* localDirPath = [[NSBundle mainBundle] resourcePath];
    NSString* storageDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    const uint maxLen {1024};
    BOOL result = NO;
    
    char stringBuffer [maxLen];
    result = [appDirPath getCString:stringBuffer maxLength:maxLen encoding:NSASCIIStringEncoding];
    locations.appRootDir = stringBuffer;
    result = [dataDirPath getCString:stringBuffer maxLength:maxLen encoding:NSASCIIStringEncoding] && result;
    locations.dataDir = stringBuffer;
    result = [docDirPath getCString:stringBuffer maxLength:maxLen encoding:NSASCIIStringEncoding] && result;
    locations.docsDir = stringBuffer;
    result = [cacheDirPath getCString:stringBuffer maxLength:maxLen encoding:NSASCIIStringEncoding] && result;
    locations.cacheDir = stringBuffer;
    result = [localDirPath getCString:stringBuffer maxLength:maxLen encoding:NSASCIIStringEncoding] && result;
    locations.localDir = stringBuffer;
    result = [storageDirPath getCString:stringBuffer maxLength:maxLen encoding:NSASCIIStringEncoding] && result;
    locations.storageDir = stringBuffer;
    
    locations.resourcesDir = locations.appRootDir;
    
    msg_assert(result, "invalid location");
    
    FileSystem::setLocations(std::move(locations));
    Activity::setupManagers(self.iOSPlatformContext, [(DefaultView*)self.view surface]);
    
    CGSize size;
    size.width = 0;
    
    [(DefaultView*)self.view resize: size];
    Activity::start();
}
//------------------------------------------------------------------------------
-(void)onEngineShutdown {
    Activity::stop();
    Activity::deinit();
}
//------------------------------------------------------------------------------
-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(DefaultView*)self.view stopUpdate];
    Activity::appWillEnterBackground();
}
//------------------------------------------------------------------------------
-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    Activity::appDidEnterBackground();
}
//------------------------------------------------------------------------------
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    Activity::appWillEnterForeground();
}
//------------------------------------------------------------------------------
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [(DefaultView*)self.view startUpdate];
    Activity::appDidEnterForeground();
}
//------------------------------------------------------------------------------
#pragma mark - Input Handling -
//------------------------------------------------------------------------------
-(CGPoint) convertScreenPoint:(CGPoint) point {
    CGSize size = self.view.bounds.size;
    point.y = size.height - point.y;
    return point;
}
//------------------------------------------------------------------------------
+(input::EventType) inputEvent:(UIGestureRecognizerState) state {
    switch (state) {
        case UIGestureRecognizerStateBegan:
            return input::EventType::Began;
        case UIGestureRecognizerStateChanged:
            return input::EventType::Progress;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed:
        default:
            return input::EventType::Ended;
    }
    return input::EventType::Ended;
}
//------------------------------------------------------------------------------
-(void) handleTap:(UITapGestureRecognizer*)sender {
    CGPoint point = [self convertScreenPoint: [sender locationInView: sender.view]];
    
    input::Gesture gesture;
    gesture.type = input::GestureType::Tap;
    gesture.event = [DefaultViewController inputEvent:sender.state];
    gesture.position = vec2f(point.x, point.y);
    gesture.timestamp = posix_time();
    
    Activity::gestureNotifier()->handleGesture(gesture);
}
//------------------------------------------------------------------------------
-(void) handleDoubleTap:(UITapGestureRecognizer*)sender {
    
    CGPoint point = [self convertScreenPoint: [sender locationInView: sender.view]];
    
    input::Gesture gesture;
    gesture.type = input::GestureType::DoubleTap;
    gesture.event = [DefaultViewController inputEvent:sender.state];
    gesture.position = vec2f(point.x, point.y);
    gesture.timestamp = posix_time();
    
    Activity::gestureNotifier()->handleGesture(gesture);
}
//------------------------------------------------------------------------------
-(void) handleLongPress:(UILongPressGestureRecognizer*)sender {
    
    CGPoint point = [self convertScreenPoint: [sender locationInView: sender.view]];
    
    input::Gesture gesture;
    gesture.type = input::GestureType::LongPress;
    gesture.event = [DefaultViewController inputEvent:sender.state];
    gesture.position = vec2f(point.x, point.y);
    gesture.timestamp = posix_time();
    
    Activity::gestureNotifier()->handleGesture(gesture);
}
//------------------------------------------------------------------------------
-(void) handlePinch:(UIPinchGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        _gLastPinchValue = sender.scale;
    }
    
    CGPoint point = [self convertScreenPoint: [sender locationInView: sender.view]];
    CGFloat scale = sender.scale - _gLastPinchValue;
    
    input::PinchGesture gesture;
    gesture.type = input::GestureType::Pinch;
    gesture.event = [DefaultViewController inputEvent:sender.state];
    gesture.position = vec2f(point.x, point.y);
    gesture.timestamp = posix_time();
    gesture.pinch = scale;
    
    _gLastPinchValue = sender.scale;
    
    Activity::gestureNotifier()->handleGesture(gesture);
}
//------------------------------------------------------------------------------
-(void) handlePan:(UIPanGestureRecognizer*)sender {
    if (sender.state == UIGestureRecognizerStateBegan
        || _gLastPanTouchesCount != sender.numberOfTouches)
    {
        _gLastPanTouchesCount = sender.numberOfTouches;
        _gLastPanPoint = [sender locationInView: sender.view];
    }
    
    CGPoint point = [self convertScreenPoint: [sender locationInView: sender.view]];
    CGPoint velocity = [sender velocityInView:sender.view];
    
    input::PanGesture gesture;
    gesture.type = input::GestureType::Pan;
    gesture.event = [DefaultViewController inputEvent:sender.state];
    gesture.position = vec2f(point.x, point.y);
    gesture.timestamp = posix_time();
    
    gesture.translation = vec2f(point.x - _gLastPanPoint.x, point.y - _gLastPanPoint.y);
    gesture.velocity = vec2f(velocity.x, velocity.y);
    gesture.lastPosition = gesture.position - gesture.translation;
    
    _gLastPanPoint = point;
    _gLastPanTouchesCount = sender.numberOfTouches;
    
    Activity::gestureNotifier()->handleGesture(gesture);
}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
-(float) scale
{
    return [[UIScreen mainScreen] scale];
}
//------------------------------------------------------------------------------
-(float) ppi
{
    float scale = [self scale];
    return scale * ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) ? 132 : 163);
}
//------------------------------------------------------------------------------
-(float) screenWidth
{
    float scale = [self scale];
    return ([[UIScreen mainScreen] bounds].size.width * scale);
}
//------------------------------------------------------------------------------
-(float) screenHeight
{
    float scale = [self scale];
    return ([[UIScreen mainScreen] bounds].size.height * scale);
}
//------------------------------------------------------------------------------
-(float) screenDiagonal
{
    float ppi = [self ppi];
    float width = [self screenWidth];
    float height = [self screenHeight];
    float horizontal = width / ppi, vertical = height / ppi;
    return sqrt(pow(horizontal, 2) + pow(vertical, 2));
}
//------------------------------------------------------------------------------
static float scale()
{
    return [getViewController() scale];
}
//------------------------------------------------------------------------------
static float ppi()
{
    return [getViewController() ppi];
}
//------------------------------------------------------------------------------
@end
