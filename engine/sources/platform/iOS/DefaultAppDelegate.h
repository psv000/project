//
//  DefaultAppDelegate.h
//  engine
//
//  Created by s.popov on 13/09/2017.
//
//

#import <UIKit/UIKit.h>

@class DefaultViewController;

@interface DefaultAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DefaultViewController *viewController;

@end
