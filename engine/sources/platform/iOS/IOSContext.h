//
//  IOSContext.h
//  engine
//
//  Created by s.popov on 20/10/2017.
//
//

#pragma once

#include <core/IPlatformContext.h>

typedef float(*floatGetter)();

class IOSContext final : public IPlatformContext
{
public:
    float ppi() const override;
    float density() const override;
    
    void setPPIGetter(floatGetter f) { mPPIGetter = f; }
    void setScaleGetter(floatGetter f) { mScaleGetter = f; }
private:

    floatGetter mPPIGetter;
    floatGetter mScaleGetter;
};
