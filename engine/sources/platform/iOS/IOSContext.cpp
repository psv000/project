//
//  IOSContext.cpp
//  engine
//
//  Created by s.popov on 20/10/2017.
//
//

#include "IOSContext.h"
#include <asserts.h>

//------------------------------------------------------------------------------
float IOSContext::ppi() const
{
    void_assert(mPPIGetter);
    return mPPIGetter();
}
//------------------------------------------------------------------------------
float IOSContext::density() const
{
    void_assert(mScaleGetter);
    static float density = mScaleGetter();
    return density;
}
//------------------------------------------------------------------------------
