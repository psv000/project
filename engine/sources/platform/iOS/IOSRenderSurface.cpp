//
//  IOSRenderSurface.cpp
//  engine
//
//  Created by s.popov on 22/09/2017.
//
//

#include "IOSRenderSurface.h"
#include <OpenGLES/ES3/gl.h>
#include <graphics/GLES/GLESPrerequisites.h>
#include <core/IPlatformContext.h>
#include <core/Activity.h>
#include <common/formatstring.h>
#include <logs.h>

namespace graphics
{
//------------------------------------------------------------------------------
IOSRenderSurface::IOSRenderSurface():
mSurfaceSize(0u),
mSamplesCount(0),
mMSAA(mSamplesCount > 0),
mFrameBuffer(INVALID_FBO),
mColorBuffer(INVALID_FBO),
mMSAAColorBuffer(INVALID_FBO),
mMSAAFrameBuffer(INVALID_FBO),
mRenderBufferStorage(nullptr)
{
}
//------------------------------------------------------------------------------
void IOSRenderSurface::init(const RenderBufferStorageGET& renderBufferStorage, uint16 width, uint16 height)
{
    mRenderBufferStorage = renderBufferStorage;
    
    glGenFramebuffers(1, &mFrameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, mFrameBuffer);
    {
        glGenRenderbuffers(1, &mColorBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, mColorBuffer);
        
        mSurfaceSize.x = width;
        mSurfaceSize.y = height;
        
        mRenderBufferStorage();
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mColorBuffer);
    }
    void_assert(glGetError() == GL_NO_ERROR);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        msg_assert(false, format_string("Failed to make complete framebuffer object %x", (int)glCheckFramebufferStatus(GL_FRAMEBUFFER)).c_str());
    }
}
//------------------------------------------------------------------------------
void IOSRenderSurface::deinit()
{
    glDeleteRenderbuffers(1, &mFrameBuffer);
    glDeleteRenderbuffers(1, &mColorBuffer);
    
    if (mMSAA)
    {
        glDeleteRenderbuffers(1, &mMSAAColorBuffer);
        glDeleteRenderbuffers(1, &mMSAAFrameBuffer);
    }
    
    mFrameBuffer = INVALID_FBO;
    mColorBuffer = INVALID_FBO;
    
    mMSAAColorBuffer = INVALID_FBO;
    mMSAAFrameBuffer = INVALID_FBO;
}
//------------------------------------------------------------------------------
void IOSRenderSurface::onResize(uint16 width, uint16 height)
{
    {
        glBindRenderbuffer(GL_RENDERBUFFER, mColorBuffer);
        mRenderBufferStorage();
        
        float density = Activity::deviceScaleFactor();
        mSurfaceSize.x = width * density;
        mSurfaceSize.y = height * density;

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mColorBuffer);
    }
    if (mMSAA)
    {
        glDeleteRenderbuffers(1, &mMSAAColorBuffer);
        mMSAAColorBuffer = 0;
        
        glBindFramebuffer(GL_FRAMEBUFFER, mMSAAFrameBuffer);
        glGenRenderbuffers(1, &mMSAAColorBuffer);
        void_assert(mMSAAFrameBuffer || !"Can't create MSAA color buffer");
        
        glBindRenderbuffer(GL_RENDERBUFFER, mMSAAColorBuffer);
        glRenderbufferStorageMultisample(GL_RENDERBUFFER, (GLsizei)mSamplesCount, GL_RGBA8, (GLsizei)mSurfaceSize.x, (GLsizei)mSurfaceSize.y);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mMSAAColorBuffer);
    }
    void_assert(glGetError() == GL_NO_ERROR);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        LOGE(format_string("Failed to make complete framebuffer object %x", (int)glCheckFramebufferStatus(GL_FRAMEBUFFER)).c_str());
    }
    
    Activity::setContextSize(mSurfaceSize.x, mSurfaceSize.y);
}
//------------------------------------------------------------------------------
void IOSRenderSurface::apply()
{
    if (mMSAA)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, mMSAAFrameBuffer);
        
        void_assert(glGetError() == GL_NO_ERROR);
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            LOGE(format_string("Failed to make complete framebuffer object %x", (int)glCheckFramebufferStatus(GL_FRAMEBUFFER)).c_str());
        }
    }
}
//------------------------------------------------------------------------------
void IOSRenderSurface::present()
{
    glBindRenderbuffer(GL_RENDERBUFFER, mColorBuffer);
    
    auto err = glGetError();
    void_assert(err == GL_NO_ERROR);
    
    if (mMSAA)
    {
        glBindFramebuffer(GL_READ_FRAMEBUFFER, mMSAAColorBuffer);
        
        err = glGetError();
        void_assert(err == GL_NO_ERROR);
        
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, mFrameBuffer);
        glClear(GL_COLOR_BUFFER_BIT);
        
        err = glGetError();
        void_assert(err == GL_NO_ERROR);
        
        glBlitFramebuffer(0,0,mSurfaceSize.x,mSurfaceSize.y,
                          0,0,mSurfaceSize.x,mSurfaceSize.y,
                          GL_COLOR_BUFFER_BIT, GL_NEAREST);
        
        err = glGetError();
        void_assert(err == GL_NO_ERROR);
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            LOGE("Failed to make complete framebuffer object %x",
                 (int)glCheckFramebufferStatus(GL_FRAMEBUFFER));
        }
    }
}
//------------------------------------------------------------------------------
void IOSRenderSurface::MSAAon(uint8 samplesCount)
{
    if ( mMSAA && samplesCount > 0 ) return;
    
    mMSAA = samplesCount > 0;
    
    int maxSamplesAllowed;
    glGetIntegerv(GL_MAX_SAMPLES, &maxSamplesAllowed);
    mSamplesCount = GET_MIN(maxSamplesAllowed, (int)samplesCount);
    
    glGenFramebuffers(1, &mMSAAFrameBuffer);
    void_assert( mMSAAFrameBuffer || !"Can't create default MSAA frame buffer");
    glBindFramebuffer(GL_FRAMEBUFFER, mMSAAFrameBuffer);
    
    glGenRenderbuffers(1, &mMSAAColorBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, mMSAAColorBuffer);
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, maxSamplesAllowed, GL_RGBA8, mSurfaceSize.x, mSurfaceSize.y);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, mMSAAColorBuffer);
    
    void_assert(glGetError() == GL_NO_ERROR);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        LOGE("Failed to make complete framebuffer object %x",
             (int)glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
}
//------------------------------------------------------------------------------
void IOSRenderSurface::MSAAoff()
{
    if (mSamplesCount > 0)
    {
        glDeleteRenderbuffers(1, &mMSAAColorBuffer);
        glDeleteRenderbuffers(1, &mMSAAFrameBuffer);
        
        mSamplesCount = 0;
        mMSAA = false;
    }
}
//------------------------------------------------------------------------------
}//namespace graphics
