//
//  UnixFile.cpp
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#include "UnixFile.h"
#include <core/memory/memory.h>
#include <asserts.h>

namespace FileSystem
{
//------------------------------------------------------------------------------
File::File(const char *filename, AccessMod mod):
mName(filename),
mFile(nullptr),
mSize(0),
mExtension(parseExtension(filename))
{
    if (filename) {
        mFile = fopen(filename, accessModToString(mod).c_str());
        if (mFile) {
            fseek(mFile, 0L, SEEK_END);
            mSize = ftell(mFile);
            fseek(mFile, 0L, SEEK_SET);
        }
    }
}
//------------------------------------------------------------------------------
File::~File()
{
    release();
}
//------------------------------------------------------------------------------
File::File(File&& file)
{
    if (&file != this)
    {
        return;
    }

    this->mFile = file.mFile;
    this->mName = std::move(file.mName);
    this->mExtension = file.mExtension;
    this->mSize = file.mSize;

    file.mFile = nullptr;
    file.mExtension = Extension::Unknown;
    file.mSize = 0;
}
//------------------------------------------------------------------------------
File& File::operator=(File&& file)
{
    if (&file != this)
    {
        return *this;
    }

    this->mFile = file.mFile;
    this->mName = std::move(file.mName);
    this->mExtension = file.mExtension;
    this->mSize = file.mSize;

    file.mFile = nullptr;
    file.mExtension = Extension::Unknown;
    file.mSize = 0;

    return *this;
}
//------------------------------------------------------------------------------
void File::release()
{
    if (!mFile) { return; }
    
    fclose(mFile);
    mFile = nullptr;
    
    mSize = 0;
}
//------------------------------------------------------------------------------
uint64 File::read(char *buffer, uint64 size)
{
    return fread(buffer, 1, size, mFile);
}
//------------------------------------------------------------------------------
uint64 File::seek(uint64 offset)
{
    return fseek(mFile, offset, SEEK_SET);
}
//------------------------------------------------------------------------------
uint64 File::rseek(uint64 offset)
{
    return fseek(mFile, offset, SEEK_END);
}
//------------------------------------------------------------------------------
uint64 File::seekd(uint64 delta)
{
    return fseek(mFile, offset() + delta, SEEK_SET);
}
//------------------------------------------------------------------------------
uint64 File::rseekd(uint64 delta)
{
    return fseek(mFile, delta - mSize + offset(), SEEK_END);
}
//------------------------------------------------------------------------------
uint64 File::offset() const
{
    return ftell(mFile);
}
//------------------------------------------------------------------------------
uint64 File::length() const
{
    return mSize;
}
//------------------------------------------------------------------------------
bool File::empty() const
{
    return !mFile || mSize == 0;
}
//------------------------------------------------------------------------------
bool File::end() const
{
    return offset() >= mSize;
}
//------------------------------------------------------------------------------
const void* File::data() const
{
    char* fileData = nullptr;
    if (!empty())
    {
        fileData = (char*)memory::alloc(mSize + 1);
        void_assert(fileData);
        memset(fileData, 0, mSize);
        
        if (fileData)
        {
            uint64 res = fread(fileData, 1, mSize, mFile);
            if (res != mSize)
            {
                memory::dealloc(fileData);
                fileData = nullptr;
            }
            else
            {
                fileData[mSize] = 0;
            }
        }
    }
    return fileData;
}
//------------------------------------------------------------------------------  
}//namespace FileSystem
