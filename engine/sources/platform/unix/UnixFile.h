//
//  UnixFile.h
//  engine
//
//  Created by s.popov on 19/09/2017.
//
//

#pragma once

#include <core/file_system/FileTypes.h>
#include <common/stringtypes.h>
#include <stdio.h>

namespace FileSystem
{
    
class File final
{
public:
    File(const char *, AccessMod);
    File(const File &) = delete;
    File &operator=(const File &) = delete;
    
    File(File &&);
    File& operator=(File &&);
    
    ~File();
    
    const char* name() const { return mName.c_str(); }
    Extension extension() const { return mExtension; }
    const void* data() const;
    
    void release();

    uint64 read(char *buffer, uint64 size);
    
    uint64 seek(uint64 offset);
    uint64 rseek(uint64 offset);
    uint64 seekd(uint64 offset);
    uint64 rseekd(uint64 offset);
    
    uint64 offset() const;
    uint64 length() const;
    
    bool empty() const;
    bool end() const;
    
private:
    string mName;
    FILE *mFile;
    uint64 mSize;
    Extension mExtension;
};
    
}//namespace FileSystem
