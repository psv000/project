//
//  asserts.h
//  engine
//
//  Created by s.popov on 14/09/2017.
//
//

#pragma once

#include "core/assertion.h"

#ifdef ASSERTIONS_ENABLED
    #define void_assert(condition) __void_assert( condition, {__LINE__, __FUNCTION__, __FILE__} )
    #define msg_assert(condition, message) __msg_assert( condition, {__LINE__, __FUNCTION__, __FILE__}, message )
#else
    #define void_assert(condition)
    #define msg_assert(condition, message)
#endif //ASSERTIONS_ENABLED

#define abort_with_msg(condition, message) __abort_with_msg(condition, {__LINE__, __FUNCTION__, __FILE__}, message )
