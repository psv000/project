/* Freetype GL - A C OpenGL Freetype engine
 *
 * Distributed under the OSI-approved BSD 2-Clause License.  See accompanying
 * file `LICENSE` for more details.
 */

#include "vertex-buffer.h"

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "vec234.h"
#include "platform.h"

/**
 * Buffer status
 */
#define CLEAN  (0)
#define DIRTY  (1)
#define FROZEN (2)

// ----------------------------------------------------------------------------
vertex_buffer_t *
vertex_buffer_new( size_t vertexSize )
{   
    vertex_buffer_t *self = (vertex_buffer_t *) malloc (sizeof(vertex_buffer_t));
    if( !self )
    {
        return NULL;
    }
    
    self->vertices = vector_new( vertexSize );
    self->indices = vector_new( sizeof(size_t) );
    self->items = vector_new( sizeof(ivec4) );
    
    return self;
}

// ----------------------------------------------------------------------------
void
vertex_buffer_delete( vertex_buffer_t *self )
{
    assert( self );

    vector_delete( self->vertices );
    self->vertices = 0;

    vector_delete( self->indices );
    self->indices = 0;
    
    vector_delete( self->items );
    
    free( self );
}

// ----------------------------------------------------------------------------
void
vertex_buffer_push_back_indices ( vertex_buffer_t * self,
                                 const size_t * indices,
                                 const size_t icount )
{
    assert( self );

    vector_push_back_data( self->indices, indices, icount );
}

// ----------------------------------------------------------------------------
void
vertex_buffer_push_back_vertices ( vertex_buffer_t * self,
                                  const void * vertices,
                                  const size_t vcount )
{
    assert( self );
    
    vector_push_back_data( self->vertices, vertices, vcount );
}

// ----------------------------------------------------------------------------
void
vertex_buffer_insert_indices ( vertex_buffer_t *self,
                              const size_t index,
                              const size_t *indices,
                              const size_t count )
{
    assert( self );
    assert( self->indices );
    assert( index < self->indices->size+1 );
    
    vector_insert_data( self->indices, index, indices, count );
}

// ----------------------------------------------------------------------------
void
vertex_buffer_insert_vertices( vertex_buffer_t *self,
                              const size_t index,
                              const void *vertices,
                              const size_t vcount )
{
    size_t i;
    assert( self );
    assert( self->vertices );
    assert( index < self->vertices->size+1 );
    
    
    for( i=0; i<self->indices->size; ++i )
    {
        if( *(size_t *)(vector_get( self->indices, i )) > index )
        {
            *(size_t *)(vector_get( self->indices, i )) += index;
        }
    }
    
    vector_insert_data( self->vertices, index, vertices, vcount );
}

// ----------------------------------------------------------------------------
void
vertex_buffer_erase_indices( vertex_buffer_t *self,
                            const size_t first,
                            const size_t last )
{
    assert( self );
    assert( self->indices );
    assert( first < self->indices->size );
    assert( (last) <= self->indices->size );
    
    vector_erase_range( self->indices, first, last );
}

// ----------------------------------------------------------------------------
void
vertex_buffer_erase_vertices( vertex_buffer_t *self,
                             const size_t first,
                             const size_t last )
{
    size_t i;
    assert( self );
    assert( self->vertices );
    assert( first < self->vertices->size );
    assert( last <= self->vertices->size );
    assert( last > first );
    
    for( i=0; i<self->indices->size; ++i )
    {
        if( *(size_t *)(vector_get( self->indices, i )) > first )
        {
            *(size_t *)(vector_get( self->indices, i )) -= (last-first);
        }
    }
    vector_erase_range( self->vertices, first, last );
}
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
size_t
vertex_buffer_push_back( vertex_buffer_t * self,
                        const void * vertices, const size_t vcount,
                        const size_t * indices, const size_t icount )
{
    return vertex_buffer_insert( self, vector_size( self->items ),
                                vertices, vcount, indices, icount );
}

// ----------------------------------------------------------------------------
size_t
vertex_buffer_insert( vertex_buffer_t * self, const size_t index,
                     const void * vertices, const size_t vcount,
                     const size_t * indices, const size_t icount )
{
    size_t vstart, istart, i;
    size_t* p;
    ivec4 item;
    assert( self );
    assert( vertices );
    assert( indices );
    
    // Push back vertices
    vstart = vector_size( self->vertices );
    vertex_buffer_push_back_vertices( self, vertices, vcount );
    
    // Push back indices
    istart = vector_size( self->indices );
    vertex_buffer_push_back_indices( self, indices, icount );
    
    // Update indices within the vertex buffer
    for( i=0; i<icount; ++i )
    {
        p = (size_t *)(vector_get( self->indices, istart+i ));
        *p += vstart;
        *p = *p;
    }
    
    // Insert item
    item.x = vstart;
    item.y = vcount;
    item.z = istart;
    item.w = icount;
    vector_insert( self->items, index, &item );
    
    return index;
}

// ----------------------------------------------------------------------------
void
vertex_buffer_erase( vertex_buffer_t * self,
                    const size_t index )
{
    ivec4 * item;
    int vstart;
    size_t vcount, istart, icount, i;
    
    assert( self );
    assert( index < vector_size( self->items ) );
    
    item = (ivec4 *) vector_get( self->items, index );
    vstart = item->vstart;
    vcount = item->vcount;
    istart = item->istart;
    icount = item->icount;
    
    // Update items
    for( i=0; i<vector_size(self->items); ++i )
    {
        ivec4 * item = (ivec4 *) vector_get( self->items, i );
        if( item->vstart > vstart)
        {
            item->vstart -= vcount;
            item->istart -= icount;
        }
    }

    vertex_buffer_erase_indices( self, istart, istart+icount );
    vertex_buffer_erase_vertices( self, vstart, vstart+vcount );
    vector_erase( self->items, index );
}

// ----------------------------------------------------------------------------
size_t
vertex_buffer_size( const vertex_buffer_t *self )
{
    assert( self );
    
    return vector_size( self->items );
}

// ----------------------------------------------------------------------------
void
vertex_buffer_clear( vertex_buffer_t *self )
{
    assert( self );
    
    vector_clear( self->indices );
    vector_clear( self->vertices );
    vector_clear( self->items );
}

// ----------------------------------------------------------------------------
