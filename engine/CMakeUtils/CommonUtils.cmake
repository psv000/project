message ( "include Common Utils" )
#--------------------------------------------------------------------
function ( remove_target TARGET )
    set_target_properties ( ${TARGET} PROPERTIES EXCLUDE_FROM_ALL 1 EXCLUDE_FROM_DEFAULT_BUILD 1 )
endfunction ( remove_target )
#--------------------------------------------------------------------
function ( collect_sources
    DIRECTORIES
    NAME_PATTERNS
    RESULT )

    set ( FUNC_RESULT )

    list ( LENGTH NAME_PATTERNS PATTERS_COUNT )
	math ( EXPR PATTERS_COUNT "${PATTERS_COUNT} - 1" )

    foreach( DIR ${${DIRECTORIES}} )

        foreach ( PATTERN ${${NAME_PATTERNS}} )

            file ( GLOB 
                GLOB_RESULT
                ${DIR}/${PATTERN} )

            string(REGEX REPLACE "/" "\\\\" GROUP_NAME ${DIR})
            source_group("${GROUP_NAME}" FILES ${GLOB_RESULT})
            list ( APPEND FUNC_RESULT ${GLOB_RESULT} )

        endforeach ( )

    endforeach ( )

    set(${RESULT} ${${RESULT}} ${FUNC_RESULT} PARENT_SCOPE)

endfunction ( collect_sources )
#--------------------------------------------------------------------
function ( assets_make_copy_target TARGET_NAME SRC_DIR DST_DIR )
    file( GLOB_RECURSE RESOURCE_FILES
        ${SRC_DIR}/*.png
        ${SRC_DIR}/*.json
        ${SRC_DIR}/*.frag
        ${SRC_DIR}/*.vert )

    foreach ( RES_FILE ${RESOURCE_FILES} )
        file ( RELATIVE_PATH RES_FILE_RELPATH ${SRC_DIR} ${RES_FILE} )
        set ( OUT_RES_FILE ${DST_DIR}/${RES_FILE_RELPATH} )
        add_custom_command ( OUTPUT ${OUT_RES_FILE}
                            COMMAND ${CMAKE_COMMAND} -E copy ${RES_FILE} ${OUT_RES_FILE}
                            DEPENDS ${RES_FILE} )
        list ( APPEND OUT_RESOURCE_FILES ${OUT_RES_FILE} )

        message ("*** ${OUT_RES_FILE}")
    endforeach ( )

    message ( "TARGET_NAME: ${TARGET_NAME}" )
    message ( "RESOURCE_FILES: ${RESOURCE_FILES}" )

    add_custom_target ( ${TARGET_NAME} ALL DEPENDS ${OUT_RESOURCE_FILES} )
endfunction ( assets_make_copy_target )
#--------------------------------------------------------------------
function ( collect_resources
        DIRECTORIES
        NAME_PATTERNS
        RESULT )

    set ( FUNC_RESULT )

    foreach ( DIR ${${DIRECTORIES}} )

        foreach ( PATTERN ${${NAME_PATTERNS}} )
        
            file ( GLOB GLOB_RESULT
                    ${DIR}/${PATTERN}
                    ${DIR}/${PATTERN}
            )
        
            list ( APPEND FUNC_RESULT ${GLOB_RESULT} )
        
        endforeach ( )
    
    endforeach ( )

    set ( ${RESULT} ${${RESULT}} ${FUNC_RESULT} PARENT_SCOPE )

endfunction ( collect_resources )
#--------------------------------------------------------------------
function ( collect_crossplatform_sources
    DIRECTORIES
    HEADERS_RESULT
    SOURCES_RESULT )

    set ( HEADERS_FUNC_RESULT )
    set ( SOURCES_FUNC_RESULT )

    set ( COMMON_NAME_PATTERNS *.h *.hpp *.pch )
    collect_sources ( ${DIRECTORIES} COMMON_NAME_PATTERNS HEADERS_FUNC_RESULT )
    set ( COMMON_NAME_PATTERNS *.c *.cpp )
    collect_sources ( ${DIRECTORIES} COMMON_NAME_PATTERNS SOURCES_FUNC_RESULT )

    set ( ${HEADERS_RESULT} ${${HEADERS_RESULT}} ${HEADERS_FUNC_RESULT} PARENT_SCOPE )
    set ( ${SOURCES_RESULT} ${${SOURCES_RESULT}} ${SOURCES_FUNC_RESULT} PARENT_SCOPE )

endfunction ( collect_crossplatform_sources )
#--------------------------------------------------------------------