message ( "include Output Utils" )

##############################
##                          ##
##        COLORIZING        ##
##                          ##
##############################
string( ASCII 27 ESCAPE )

set( ColourReset "${ESCAPE}[m" )
set( ColourBold  "${ESCAPE}[1m" )
set( Red         "${ESCAPE}[31m" )
set( Green       "${ESCAPE}[32m" )
set( Yellow      "${ESCAPE}[33m" )
set( Blue        "${ESCAPE}[34m" )
set( Magenta     "${ESCAPE}[35m" )
set( Cyan        "${ESCAPE}[36m" )
set( White       "${ESCAPE}[37m" )
set( BoldRed     "${ESCAPE}[1;31m" )
set( BoldGreen   "${ESCAPE}[1;32m" )
set( BoldYellow  "${ESCAPE}[1;33m" )
set( BoldBlue    "${ESCAPE}[1;34m" )
set( BoldMagenta "${ESCAPE}[1;35m" )
set( BoldCyan    "${ESCAPE}[1;36m" )
set( BoldWhite   "${ESCAPE}[1;37m" )
#--------------------------------------------------------------------
function( message )

  list( GET ARGV 0 MessageType )
  if( MessageType STREQUAL FATAL_ERROR OR MessageType STREQUAL SEND_ERROR )

    list    ( REMOVE_AT ARGV 0 )
    _message( ${MessageType} "${BoldRed}${ARGV}${ColourReset}" )

  elseif( MessageType STREQUAL WARNING )

    list    ( REMOVE_AT ARGV 0 )
    _message( ${MessageType} "${BoldMagenta}${ARGV}${ColourReset}" )

  elseif( MessageType STREQUAL AUTHOR_WARNING )

    list    ( REMOVE_AT ARGV 0 )
    _message(${MessageType} "${Yellow}${ARGV}${ColourReset}" )

  elseif( MessageType STREQUAL STATUS )

    list    ( REMOVE_AT ARGV 0 )
    _message( ${MessageType} "${Cyan}${ARGV}${ColourReset}" )

  else()

    _message( "${Green}${ARGV}${ColourReset}" )

  endif()

endfunction()
#--------------------------------------------------------------------
##############################
##                          ##
##        COPY_FILES        ##
##                          ##
##############################
#--------------------------------------------------------------------
macro( COPY_DIRECTORY in_dir out_dir )

	FILE   ( GLOB_RECURSE in_file_list ${in_dir}/* )
	message( STATUS ${out_dir} )	
	message( STATUS ${in_dir} )

	FOREACH( in_file ${in_file_list} )

		message       ( STATUS ${in_file} )
		STRING        ( REGEX REPLACE ${in_dir} ${out_dir} out_file ${in_file} )
		configure_file( ${in_file} ${out_file} COPYONLY )

	ENDFOREACH( in_file )
  	
endmacro( COPY_DIRECTORY )
#--------------------------------------------------------------------