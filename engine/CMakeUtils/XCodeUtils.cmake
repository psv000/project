message ( "include XCode Utils" )
#--------------------------------------------------------------------
function ( set_xcode_property
    targ
    xc_prop_name
    xc_prop_val )
    
    set_property ( TARGET ${targ} PROPERTY XCODE_ATTRIBUTE_${xc_prop_name} ${xc_prop_val} )
    
endfunction ( set_xcode_property )
#--------------------------------------------------------------------
function ( xcode_sync_resources
    TARGET
    DIRS )
    
    foreach (DIR ${${DIRS}})
    add_custom_command (
        TARGET ${TARGET}
        POST_BUILD
        COMMAND rsync -av ${DIR} \${TARGET_BUILD_DIR}/\${TARGET_NAME}.app
    )

    message("copy resource dir: ${DIR}")
    endforeach (DIR)
endfunction ( xcode_sync_resources )
#--------------------------------------------------------------------