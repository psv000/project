#version 100

precision lowp float;
precision lowp int;

varying vec2 texcoord;

uniform sampler2D utexture0;
uniform vec4 ucolor;

void main(void)
{
  gl_FragColor = ucolor * texture2D(utexture0, texcoord);
}