#version 100

attribute vec3 position;
attribute vec4 color;
attribute mediump vec2 uvcoord;

varying vec2 texcoord;

uniform mat4 umvp;

void main(void)
{
  gl_Position = umvp * vec4(position, 1);
  texcoord = uvcoord;
}