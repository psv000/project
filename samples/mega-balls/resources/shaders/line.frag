#version 100

precision mediump float;
precision mediump int;

varying vec4 vcolor;
varying vec2 vtexcoord;

uniform float uperiod;
uniform float udash;
uniform vec4 ucolor;

void main(void)
{
    float frag_pos = mod( length(vtexcoord), uperiod );
    int vis = int((frag_pos / uperiod) < udash);
    
    gl_FragColor = vcolor * ucolor;
    gl_FragColor.a = float( vis );
}