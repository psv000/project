#version 100

precision lowp float;
precision lowp int;

varying vec2 vtexcoord;
varying vec4 vcolor;

uniform sampler2D utexture0;
uniform vec4 ucolor;

void main(void)
{
    vec4 color = vcolor;
    color.a *= ucolor.a * texture2D(utexture0, vtexcoord).a;
    gl_FragColor = color;
}