#version 100

precision lowp float;
precision lowp int;

varying vec4 vcolor;
uniform vec4 ucolor;

void main(void)
{
    gl_FragColor = vcolor * ucolor;
}