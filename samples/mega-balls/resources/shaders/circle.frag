#version 100

precision lowp float;
precision lowp int;

varying vec4 vcolor;
varying vec2 vtexcoord;

uniform vec4 ucolor;
uniform float uradius;
uniform float uborder;

void main( void )
{
    vec2 uv = vtexcoord.xy;
    uv -= 0.5;
    float l = length(uv) / 0.5;
    float t = smoothstep(1. - uborder, 1., l);

    gl_FragColor.rgb = vcolor.rgb * ucolor.rgb;
	gl_FragColor.a = mix(vcolor.a * ucolor.a, 0., t);
}