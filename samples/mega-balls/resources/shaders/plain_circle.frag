#version 100

precision lowp float;
precision lowp int;

varying vec2 vtexcoord;

uniform vec4 ucolor;
uniform float uborder;

void main( void )
{
    vec2 uv = vtexcoord.xy;
    uv -= 0.5;
    float l = length(uv) / 0.5;
    float t = smoothstep(1. - uborder, 1., l);

    gl_FragColor.rgb = ucolor.rgb;
	gl_FragColor.a = mix(ucolor.a, 0., t);
}