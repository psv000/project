#version 100

precision lowp float;
precision lowp int;

uniform vec4 ucolor;

void main(void)
{
    gl_FragColor = ucolor;
}