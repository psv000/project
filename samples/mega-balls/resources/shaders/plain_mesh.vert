#version 100

precision lowp float;
precision lowp int;

attribute vec3 position;

uniform mat4 umvp;

void main(void)
{
    gl_Position = umvp * vec4(position, 1.);
}