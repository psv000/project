#version 100

precision lowp float;
precision lowp int;

attribute vec3 position;
attribute vec4 color;
attribute vec2 uvcoord;

varying vec4 vcolor;

uniform mat4 umvp;

void main(void)
{
    gl_Position = umvp * vec4(position, 1.);
    vcolor = color;
}