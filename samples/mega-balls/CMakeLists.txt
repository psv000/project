cmake_minimum_required(VERSION 3.6)

#----------------- variables ------------------
set ( PROJECT_NAME "mega-balls" )
set ( CMAKE_CXX_STANDARD 14 )

set ( ROOT_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/sources" )
set ( SOURCES )
set ( HEADERS )
set ( PLATFORM_SPECIFIC )
set ( PLATFORM_SOURCE_DIRECTORIES )
set ( PLATFORM_SOURCE_DIRECTORIES_LIB )

set ( PROJECT_TARGET ${PROJECT_NAME} )

#----------------------------------------------
project( ${PROJECT_NAME} VERSION 0.9.0.0 )
#---------- Platform Sources -----------------
if ( ${TARGET_PLATFORM} STREQUAL iOS )
    set ( PLATFORM_SOURCE_DIRECTORIES 
        "${ROOT_SOURCE_DIR}/platform/iOS" )
    set ( PLATFORM_NAME_PATTERNS *.swift *.mm *.m )
elseif ( ${TARGET_PLATFORM} STREQUAL Android )
    set ( PLATFORM_SOURCE_DIRECTORIES 
        "${ROOT_SOURCE_DIR}/platform/android" )
    set ( PLATFORM_NAME_PATTERNS *.java )
    set ( PLATFORM_LIBS ${PLATFORM_LIBS} android EGL GLESv3 log )
elseif ( ${TARGET_PLATFORM} STREQUAL web )
    set ( PLATFORM_SOURCE_DIRECTORIES 
        "${ROOT_SOURCE_DIR}/platform/emscripten" )
elseif ( ${TARGET_PLATFORM} STREQUAL Unix )
    set ( PLATFORM_SOURCE_DIRECTORIES 
        "${ROOT_SOURCE_DIR}/platform/unix" )
    set ( PLATFORM_LIBS ${PLATFORM_LIBS} glfw )
endif ( )
collect_sources ( PLATFORM_SOURCE_DIRECTORIES PLATFORM_NAME_PATTERNS SOURCES )
#------------------ sources -------------------
set ( SOURCE_DIRECTORIES
    ${ROOT_SOURCE_DIR}
)
include_directories ( ${ROOT_SOURCE_DIR} )
list ( APPEND SOURCE_DIRECTORIES ${PLATFORM_SOURCE_DIRECTORIES} )
collect_crossplatform_sources ( SOURCE_DIRECTORIES HEADERS SOURCES )
#---------- Collect Common Resources ----------
set ( RESOURCES_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/resources" )
set ( RESOURCES_DIRS
    ${RESOURCES_ROOT}/atlases
    ${RESOURCES_ROOT}/fonts
    ${RESOURCES_ROOT}/shaders )
set ( RESOURCES_NAME_PATTERNS *.frag *.vert )
collect_resources ( RESOURCES_DIRS RESOURCES_NAME_PATTERNS PROJECT_RESOURCES )
message ( " >> Resources from ${RESOURCES_ROOT} -- \n${PROJECT_RESOURCES} " )
#----------------------------------------------

#---------- iOS Resources ---------------------
if ( ${TARGET_PLATFORM} STREQUAL iOS )
    # set ( BUNDLE_IDENTIFIER "com.space307.${PROJECT_NAME}" )
    set ( BUNDLE_IDENTIFIER "com.olymptrade.ios" )
    set( IMAGES
        # resources/ios/iphone/launchscreen/Default@2x.png # Retina, 640x960
        target_resources/ios/launch_screen/Default-568h@2x.png # Retina 4-inch, 640x1136
        # See plist.in:
        # images/ipad/Default-Portrait~ipad.png # Portrait Non-Retina, 768x1024
        # images/ipad/Default-Portrait@2x~ipad.png # Portrait Retina, 1536x2048
        # images/ipad/Default-Landscape~ipad.png # Landscape Non-Retina, 1024x768
        # images/ipad/Default-Landscape@2x~ipad.png # Landscape Retina, 2048x1536
    )
    set( ICONS
        # resources/ios/iphone/icons/Icon~iphone.png # iPhone Retina, 120x120
        # icons/app/Icon~ipad.png # iPad Non-Retina, 76x76
        # icons/app/Icon@2x~ipad.png # iPad Retina, 152x152
        # icons/spotlight/Icon.png # iPhone/iPad Retina, 80x80
        # icons/spotlight/Icon~ipad.png # iPad Non-Retina, 40x40
        # icons/settings/Icon@2x.png # iPhone Retina, 58x58
        # icons/settings/Icon-Small.png # iPad Non-Retina 29x29
    )
    set ( PLATFORM_SPECIFIC ${IMAGES} ${ICONS} )
    set ( TARGET_PLATFORM_PROPERTY MACOSX_BUNDLE )
endif ()

if ( ${TARGET_PLATFORM} STREQUAL iOS )

    add_executable (
        ${PROJECT_TARGET}
        ${TARGET_PLATFORM_PROPERTY}
        ${PLATFORM_SPECIFIC}
        ${SOURCES} ${HEADERS} )

#---------- Common Resources For iOS ----------
    xcode_sync_resources ( ${PROJECT_TARGET} RESOURCES_DIRS )
#---------- XCode Proj Properties -------------

    set_xcode_property ( ${PROJECT_TARGET} DEBUG_INFORMATION_FORMAT[variant=Debug] "dwarf-with-dsym")
    set_xcode_property ( ${PROJECT_TARGET} ONLY_ACTIVE_ARCH "YES")
    set_xcode_property ( ${PROJECT_TARGET} IPHONEOS_DEPLOYMENT_TARGET "9.3" )
    set_xcode_property ( ${LIB_ENGINE} IPHONEOS_DEPLOYMENT_TARGET "9.3" )
    set_xcode_property ( ${PROJECT_TARGET} ENABLE_BITCODE "NO")
    set_xcode_property ( ${PROJECT_TARGET} COMPILE_FLAGS "-fobjc-arc")
    set_xcode_property ( ${PROJECT_TARGET} CODE_SIGN_IDENTITY "iPhone Developer")
    set_xcode_property ( ${PROJECT_TARGET} DEVELOPMENT_TEAM "3VLSSJ2SLK")
#---------- Info.plist Properties -------------
    set ( MACOSX_BUNDLE_GUI_IDENTIFIER ${BUNDLE_IDENTIFIER} )
#---------- iOS Libraries ---------------------
    set ( PLATFORM_LIBS
        ${PLATFORM_LIBS}
        "-framework OpenGLES"
        "-framework GLKit"
        "-framework QuartzCore"
        "-weak_framework UIKit"
        "-framework Foundation"
        "-framework CoreGraphics"
        "-framework CoreData"
        "-framework AVFoundation"
        "-ObjC"
    )
    set_target_properties ( ${PROJECT_TARGET} PROPERTIES
        COMPILE_FLAGS "${PROJECT_COMPILE_FLAGS}"
        LINK_FLAGS "${PROJECT_LINK_FLAGS}"
        XCODE_ATTRIBUTE_CLANG_ENABLE_OBJC_ARC YES
        RESOURCE "${IMAGES};${ICONS}"
    )

elseif ( ${TARGET_PLATFORM} STREQUAL Android )

    add_library ( ${PROJECT_TARGET} SHARED
                ${PLATFORM_SPECIFIC}
                ${SOURCES} ${HEADERS}
    )

elseif ( ${TARGET_PLATFORM} STREQUAL web )

    add_executable (
        ${PROJECT_TARGET}
        ${SOURCES} ${HEADERS}
    )

    set ( EMCC_OPTIONS "${EMCC_OPTIONS} --preload-file resources"  )
    set ( EMCC_OPTIONS "${EMCC_OPTIONS} --memory-init-file 1" )
    set ( EMCC_OPTIONS "${EMCC_OPTIONS} -s EXTRA_EXPORTED_RUNTIME_METHODS=\"['ccall', 'cwrap']\"" )
    set ( EMCC_OPTIONS "${EMCC_OPTIONS} -s EXPORTED_FUNCTIONS=\"['_main', '_emscripten_resize', '_emscripten_initCandleArray', '_emscripten_addCandleToArray', '_emscripten_flushCandleArray', '_emscripten_addCandle', '_emscripten_updateCandle', '_emscripten_changeChartType']\"" )
    set ( EMCC_OPTIONS "${EMCC_OPTIONS} -s DISABLE_EXCEPTION_CATCHING=1" )
    set ( EMCC_OPTIONS "${EMCC_OPTIONS} -s WASM=0" )
    # set ( EMCC_OPTIONS "${EMCC_OPTIONS} -s MODULARIZE=1" )
    set ( EMCC_OPTIONS "${EMCC_OPTIONS} -s ASSERTIONS=1" )
    set ( EMCC_OPTIONS "${EMCC_OPTIONS} -s GL_ASSERTIONS=1" )

    set ( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${EMCC_OPTIONS}" )
    set ( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${EMCC_OPTIONS}" )

elseif ( ${TARGET_PLATFORM} STREQUAL Unix )

    add_executable (
        ${PROJECT_TARGET}
        ${SOURCES} ${HEADERS}
    )

endif ( )

message ( " >> Platform libs: \n${PLATFORM_LIBS}" )

target_link_libraries ( ${PROJECT_TARGET} ${LIB_ENGINE} ${PLATFORM_LIBS} )
add_dependencies ( ${PROJECT_TARGET} ${LIB_ENGINE} )

#------------ project definitions -------------
if ( ${CMAKE_BUILD_TYPE} STREQUAL Debug )
    target_compile_definitions ( ${PROJECT_TARGET} PUBLIC DEBUG_ENABLED=1 )
    target_compile_definitions ( ${LIB_ENGINE} PUBLIC DEBUG_ENABLED=1 )
    target_compile_definitions ( ${LIB_ENGINE} PUBLIC FT_DEBUG_LEVEL_TRACE=1 )
    target_compile_definitions ( ${LIB_ENGINE} PUBLIC SHOW_DEBUG_STATS_SHORT=${CMAKE_DEF_SHOW_DEBUG_STATS} )
endif()

target_compile_definitions ( ${LIB_ENGINE} PUBLIC ASSERTIONS_ENABLED=1 )
target_compile_definitions ( ${LIB_ENGINE} PUBLIC DEBUG_LOG_SINCE_START=1 )
target_compile_definitions ( ${PROJECT_TARGET} PUBLIC PROJECT_NAME_TAG="${PROJECT_NAME}" )
target_compile_definitions ( ${PROJECT_TARGET} PUBLIC PROJECT_VERSION_TAG="${PROJECT_VERSION}" )
